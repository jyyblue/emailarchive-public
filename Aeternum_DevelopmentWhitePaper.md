<h1>Javascript Developer White Paper</h1>
<h1>Hornetsceurity Archive app</h1>

Stand: B02.01 2015.04024

#TODOs Übergabe
- prepare build (uglify etc) (---dev parameter & function as value in Gruntfile.js)
- prepare console output
- remove api-data log

#Grundlegendes
##wichtige benutzte Libraries/Frameworks

javascript

- jquery https://jquery.com/
- backbone http://backbonejs.org/
- underscore http://underscorejs.org/
- requirejs http://requirejs.org/
- handlebars http://handlebarsjs.com/
- cryptojslib https://code.google.com/p/crypto-js/
- momentjs http://momentjs.com/
- cordova.js https://cordova.apache.org/

css

- bootstrap http://getbootstrap.com/

##Entwickeln mit Chrome (o.ä.)
###Mobilevariante laufen lassen, Vaiablen setzen in main.js

    window.asemobile_isArchive = false;
    window.asemobile_isArchiveMobileDevice = true;
    window.asemobile_isArchiveMobileDeviceDev = true;

###Webvariante laufen lassen, Vaiablen setzen in main.js

    window.asemobile_isArchive = true;
    window.asemobile_isArchiveMobileDevice = false;
    window.asemobile_isArchiveMobileDeviceDev = false;

An den Stellen wo auf NativeCordovaFunktionen zurückgegriffen wird erschein ein Dev-Hinweis. Diese Funktionen müssen auf einem Device ausprobiert werden.

Im Buildprozess werden diese Einstellungen automatisch entsprechend dem Target angepasst.

Die Ausgabe auf der Console wird in main.js konfiguriert/umgeleitet, die Umleitung in Grunfile.js für die verschiedenen Builds angepaßt.

##Verzeichnisse
###public_html/js/tools

einzelne Dateien oder Libraries, die NICHT über bower installiert werden können

###public_html/js/tools/hbshelpers
Handlebars (Template Engine, die in allen *.html Snippets verwendet wird) plugin, insbesondere zum Übersetzen in die verschiedenen Sprachen (l10n.js)

###public_html/js/app/models
Daten und Klassen zum Aufrufen der Rest-API

###public_html/js/app/tpl
HTML Templates

###public_html/js/app/views
Klassen zur Darstellung der Daten 

##Startverhalten
###Mobile

Variante Credentials ok UND Server/REST-API verfügbar

    - requirejs configuration einlesen 
    - falls im localstorage credentials liegen mit diesen Credentials ein Token anfordern und danach die vorhandenen Jahre, für die ein Archiv existiert
    - Wenn cordova geladen ist (deviceready) Starten des Routers, der die verschiedenen Views verwaltet
    - laden der aktuellsten 30 Einträge aus dem Archiv
    - wechseln zum View Mailbox

Variante Credentials NICHT ok ODER Server/REST-API NICHT verfügbar

    - requirejs configuration einlesen 
    - falls im localstorage credentials liegen mit diesen Credentials ein Token ... Keine Antwort
    - Wenn cordova geladen ist (deviceready) Starten des Routers, der die verschiedenen Views verwaltet
    - wechseln zum Preferences View zum Eingeben von Email/Passwort

###Web
    - requirejs configuration einlesen 
    - Starten des Routers und prüfen auf eine Session-Variable, der die verschiedenen Views verwaltet
    - wechseln zum Preferences View zum Eingeben von Email/Passwort
    - Nach Eingabe von Name/Passwort speichern dieser Daten verschlüsselt in eienr Session-Variable 
    - reload der Page, damit die Werte im lokalen Browser gespeichert werden
    - Starten des Routers, Session-Variable auslesen (globales Session-Object siehe main.js) und mit diesen Daten Token und Archiv-Jahre anfordern
    - laden der aktuellsten 30 Einträge aus dem Archiv
    - wechseln zum View Mailbox

##lokale Daten
###Konfiguration
Die Konfiguration wird im LocalStorage des Browsers gespeichert.
Es werden 3 JSON-Strings abgelegt

    ase_mobile_credentials_1_0 (nur für mobile devices) verwaltet von ApiLogin.js
    ase_mobile_lastsearch_1_0 (nur für mobile devices) verwaltet von LastSearch.js
    ase_mobile_preferences_1_0 (mobile, web wenn Privater Computer eingeschaltet ist) verwaltet von PreferencesStore.js


###Attachments
MOBILE: Alle Attachments werden im temporären Dateisystem des Devices gespeichert.
Löschen der heruntergeladenen Daten erfolgt

    Beim erneuten anmelden mit Touch/Click auf Login  
    Beim Touch/Click auf: lokale Datan löschen 
    Im (Android-)Device: Settings -->Applications -->Manage Applications --> Choose Applications --> Clear Cache Files

###Listen
Auf mobilen Clients (Stand 2014) sind 500 Einträge in der durch Blättern erstellten Ergebnis-Liste
der empfohlene Maximalwert, danach sollte die Suche weiter eingeschränkt werden.
Für Webclients sind auch 1000 Einträge problemlos (Chome, März 2015, Windows 8.1, 8GB). 
Diese Werte sind Empfehlungen und werden technisch nicht geprüft.

###Session-Emulation
Eine "Session" wird mithilfe der Token-Gültigkeit emuliert. WEB: Bei einem abgelaufenen Token wird die App neu geladen und der Nutzer  muß sich neu anmelden. MOBILE: Mit den hinterlegten Credentials wird ein neues Token geholt. Im mobilen Client bleibt die letzte Suche immer erhalten, für den Webclient nur für die Zeit, solange das Token gilt (emulierte Session). 

##Gruntfile.js
Taskmanager wird ist für folgende Aufgaben eingerichtet.

    Kompilieren von less zu css - Section less:
    Optimierung mit r.js - Section requirejs:
    Löschen von Dateien - Section clean:
    Kopieren von Dateien - Section copy:
    Tasks für Cordova - Section cordovacli:
    Ersetzten von Text in Dateien - Section replace:

Plugins für Grunt können in package.json hinzugefügt werden (siehe grunt-* Einträge) und mit npm installiert werden.

##Übersetzung
Die Übersetzung des Interfaces wird mit dem Module l10n.js (in tools/hbshelpers) und der Klasse Localization.js (in models) umgesetzt. Dabei wird zuerst die dezeit eingestellte Sprache des Clients genutzt, falls dafür keine Übersetzung existiert wird auf Englisch zurückgegriffen, falls es für den Key keine Übersetzuung gibt wird der Key angezeigt. 
In einem solchen Fall ist dann z.B. __base.success zu sehen und der Key muss mit den Übersetzungen in Localization.js nachgetragen werden. 

Ein Key MUSS IMMER in der englischen Sprache existieren.

##Fehlercodes von der Rest API
TODO

##Cordova Plugins
TODO

##Unterstützung neuer MobileOS-Versionen
Überprüfen in Router.js->checkOperatingSystem()

##Gesten
Hammer.js wird (testweise) für das Zurückblättern von Details zur Liste im TwoPageMode genutzt (siehe ArchiveMailDetails.js)

##index.html
Die Datei index.html ist kurz gefasst und lädt zwei Dateien: styles.css und *.js. Eine Änderung der Meta-Daten (insbesondere Viewport) kann weitreichende Auswirkungen haben. Für die Cordova-Build wird eine angepaßte index.html genutzt.

An das div-element asearchive_app werden die Views angehängt.

#Einzelne Funktionen
##In der Historie zurückblättern
### Nutzungserlebnis der Anwender

Für das Zurückgehen in der Historie stehen dem Anwender auf den Ziel-Plattformen
unterschiedliche Möglichkeiten zur Verfügung.

Im Browser kann er mit dem Zurückgehen-Button des Browsers sich auf zuvor gesehene Seiten bewegen. 
Auf Android-Devices steht der in der Hardware verbaute Back-Button zur Verfügung, iOS benutzer 
kennen eine solche Funktion nicht, hier muss die Anwendung einen solchen Button in der Software 
umsetzen.

Wenn ein in der History liegende oder im Browser in den Bookmarks abgespeicherte Mail aufgerufen  
werden soll, die sich NICHT in der aktuellen Ergebnisliste befindet kann diese nicht angezeigt werden.

Desweiteren ist folgendes zu beachten:

There is no way to clear the session history or to disable the back/forward navigation from 
unprivileged code. The closest available solution is the location.replace() method, which 
replaces the current item of the session history with the provided URL.

Quelle: https://developer.mozilla.org/en/DOM/window.history

###Umsetzung 

Für die Funktion der App ist ein Zurückblättern auf schmalen Devices zwingend notwendig. Hier werden
Mailliste und Maildetails auf 2 getrennten Bildschirmen dargestellt. Beim wechseln zur Mailliste 
wird immer auch ein Schritt in der Historie zurückgegangen und somit stellt sich die 
History-Funktion anders dar.

Für diese Funktion MUSS der Back-Button auf iOS angezeigt/genutzt werden, für Android KANN der
Button angezeigt werden, es kann aber auch ausschliwßlich der Hardware-Back-Button 
genutzt werden, beim Web-Client gibt es diese Art der Darstellung in der App nicht.

Nach dem Betrachten verschiedener Möglichkeiten (z.B. verlinken von einzelnen Mail als Bookmark des 
Browsers, Möglichkeiten der verschiedenen History.back Funktionen und der Beschränkungen) wurde
die Route /archiv/item_id entfernt und die Funktion in den MailBox-View integriert. Damit gibt es folgende Bildschirme:
    
    preferences
    help
    mailList (Mailbox) 
    mailDetails (Mailbox) 
    searchForm

wobei mailDetails bei der zweispaltigen MailBox ein Teil der mailList ist. Es werden folgende
Bildschirmfolgen beim BackButton genutzt:

    searchForm -> mailList -> preferences
    help -> preferences
    mailDetails -> mailList -> preferences (bei Devices un ter 768px Breite)


##Verhalten des Smart Search Eingabemodus
* Eingabe eines Textes ohne Doppelpunkt -> der Text wird in das Submenü komplett übertragen, 
bei Enter oder Click auf Lupe wird nach diesem Suchmuster mit dem Key body beginnend ab dem aktuellen 
Datum (rückwärts) gesucht
* Eingabe eines Textes mit Doppelpunkt -> alles vor dem Doppelpunkt bis zum ersten Leerzeichen 
wird als Key für die Suche interpretiert. Sollte dieses Feld nicht existieren, wird das Feld body 
als Standard gesetzt, alle Zeichen hinter dem Doppelpunkt bis maximal zum nächsten Doppelpunkt 
(kein Key) oder bis zum ersten Leerzeichen vor einem Key werden als Suchmuster interpretiert 
und in die entsprechenden Felder des Submenüs übertragen.
* Mit der Bearbeitung des Eingabefeldes (Eingabe eines Zeichens) wird die Markierung der 
Menü-Einträge, die zur aktuell angezeigten Ergebnis-Liste passen gelöscht.
angezeigten Suche
* vor jedem Key muss mindestens ein Leerzeichen stehen. Dieses Leerzeichen gehört NICHT zum Suchmuster,
das sich nach links in der Zeichenkette anschließt und dem alle weiteren Leerzeichen zugerechnet werden. 
Leerzeichen zwischen einem Key und dem Trennzeichen (:) werden ignoriert.
* beim Click auf einen Eintrag aus dem SubMenu wird der das Smartsearch Eingabefeld mit einem Eintrag 
__feld:wert__ aktualisiert. 
* Die Daten aus dem SmartSearch-Feld werden beim Aufruf der Erweiterten/Verbesserten Suche in die Maske übernommen.
Sollte eine abgespeicherte Erweiterte Suche verfügbar sein werden aus diesen Start- und End-Datum 
übernommen. Bei einem WebClient wird eine solche abgespeicherte Suche im Speicher des Computers
abgelegt und ist somit beim ersten Aufruf innerhalb einer Session leer, für die mobilen Geräte wird 
diese Suche aus dem lokalen Storage geladen. Ist das SmartSearch-Feld leer wird die letzte abgespeicherte Suche
verwendet.


###Beispiele und ihre Übersetzung:

führende Leerzeichen bei Subject

    Benutzereingabe ist "subject: Das ka to:marketing from :com attachment:pdf"
    Object {subject: " Das ka", to: "marketing", from: "com", attachment: "pdf"}

führende Leerzeichen UND nachgeordnete Leerzeichen bei Subject


    Benutzereingabe ist "subject: Das ka  to:marketing from :com attachment:pdf"
    Object {subject: " Das ka ", to: "marketing", from: "com", attachment: "pdf"}

xsubject ist kein Key -> Default-Key

    Benutzereingabe ist "xsubject:1 to:marketing from:com attachment:pdf"
    Object {body: "1", to: "marketing", from: "com", attachment: "pdf"}

xsubject ist kein Key (und hat auch kein Suchmuster) und wird weggelassen

    Benutzereingabe ist "xsubject: to:marketing "
    Object {to: "marketing "}

Der Key subject hat keinen Wert und wird weggelassen, für marketing fehlt der Key -> Default-Key
    
    Benutzereingabe ist "subject: :marketing from:com attachment:pdf"
    Object {body: "marketing", from: "com", attachment: "pdf"}

Der Key subject hat keinen Wert und wird weggelassen, für marketing fehlt der Key -> Default-Key

    Benutzereingabe ist "subject::marketing from:com attachment:pdf"
    Object {body: "marketing", from: "com", attachment: "pdf"}

tox ist kein Key und gehört zum Suchmuster, "marketing xfrom" bekommt den den Default-Key body, wird aber 
danach von com überschrieben, da auch hier kein Key existiert

    Benutzereingabe ist "subject: Das ka tox:marketing xfrom:com attachment:pdf"
    Object {subject: " Das ka tox", body: "com", attachment: "pdf"}

Führende Leerzeichen für subject, Leerzeichen zwischen einem Key und dem Trennzeichen (:) werden ignoriert

    Benutzereingabe ist "subject: Das ka to :marketing xfrom:comattachment: pdf "
    Object {subject: " Das ka", to: "marketing xfrom", body: " pdf "}

mehrmals der Default-Key body benutzt, letzter Wert (pdf) gilt

    Benutzereingabe ist "Das ka to :marketing xfrom:comattachment: pdf "
    Object {body: " pdf "}

Kein Leerzeichen vor from -> kein Key stattdessen ist xfrom teil des Suchmusters, mehrmals der Default-Key 
body benutzt, letzter Wert (pdf) gilt

    Benutzereingabe ist ":Das ka to :marketing xfrom:comattachment: pdf "
    Object {body: " pdf ", to: "marketing xfrom"}

mehrere gleiche Keys -> letzter Wert gilt

    Benutzereingabe ist "to:marketing to:sonoxo"
    Object {to: "sonoxo"}

Groß-/Kleinschreibung wird ignoriert

    Benutzereingabe ist "SUBJECT: Das ka  TO:marketing FROM :com attachment:pdf"
    Object {subject: " Das ka ", to: "marketing", from: "com", attachment: "pdf"}

ein weiter rechts stehendes leereres Suchmuster (hinter dem letzten Doppelpunkt) 
überschreibt ein bereits zuvor eingetragenes Suchmuster für den gleichen  Key. In 
diesem Fall body, dessen Suchmuster zuvor comattachment war.

    Benutzereingabe ist "subject: Das ka to :marketing xfrom:comattachment:   "
    Object {subject: " Das ka", to: "marketing xfrom"}


##Timing

Einträge in StaticData.js

    timeoutRetries: 5,
    delayForNext: 1000, //100
    loginTimeout: 3000, // 1000
    defaultTimeout: 5000, // 2000
    attachmentTimeout: 60000,

Wenn eine Anfrage an die API einen Fehler-Code als Antwort erhält, wird unabhängig vom Fehler-Code
5 mal versucht, ein neues Token zu erhalten um im Erfolgsfall dann mit diesem Token nach einer Verzögerung
die Abfrage nochmals durchzuführen. Wird kein Token geliefert, wird die Abfrage sofort abgebrochen.
Die Default-Abfrage im dauert somit maximal: defaultTimeout + (5*(loginTimeout + delayForNext + defaultTimeout)),
für Attachments kann dieser Zeitraum verlängert werden.

#Anhang
##CSS - Xpaths im MailBox-View für mobile Geräte
###Zweispaltige Darstellung

Ein Email in der Liste:
    
    /html/body/div[@id='asearchive_app']/div[@id='content']/div[@id='mailbox']/div[@id='mailbox-content']

    /div[@id='tableview-item-list']/ul[@class='mail-list']/li[@id='2015_128482']/div[@class='email-item-from']

Details:

    /html/body/div[@id='asearchive_app']/div[@id='content']/div[@id='mailbox']/div[@id='mailbox-content']

    /div[@id='tableview-item-details']/div[@id='email-datails']/ul[@class='list-group']/li[@class='list-group-item'][1]/div[@class='row']/div[@class='col-md-2']


###Einspaltige Darstellung auf 2 getrennten Bildschirmen (kleine Geräte bis 768 Pixel Breite):

Ein Email in der Liste:

    /html/body/div[@id='asearchive_app']/div[@id='content']/div[@id='mailbox']/div[@id='mailbox-content']

    /ul[@class='mail-list']/li[@id='2015_128752']


Details:

    /html/body/div[@id='asearchive_app']/div[@id='content']/div[@id='mailbox']/div[@id='mailbox-content']

    /div[@id='email-datails']/ul[@class='list-group']/li[@class='list-group-item'][1]


###Suchformular

    /html/body/div[@id='asearchive_app']/div[@id='content']/div[@id='mailbox']/div[@id='mailbox-content']

    /div[@class='row']/div[@class='col-xs-12 col-md-8']/form/div[@class='panel panel-info']/ul[@class='list-group']/li[@class='list-group-item'][1]


##Konfigurationsobjekt während einer Session

window.hs_archive

### Mobile Device
    {
      "build_id": "edge",
      "isQuarantineWeb": false,
      "isArchiveWeb": false,
      "isArchiveMobileDevice": true,
      "isArchiveMobileDeviceDev": false,
      "isEnhanchedLogging": false,
      "isDeveloperLog": false,
      "asemobile_footprint": {
        "Build": "edge",
        "Resolution_X": 1920,
        "Resolution_Y": 1046,
        "ColorDepth": 24,
        "UserAgent": "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36",
        "Timezone": -120,
        "Language": "de",
        "Os": "web",
        "OsVersion": ""
      },
      "sessiondata": {
        "preferences": {},
        "search": {},
        "token": "2FB204A1416584FFDCAB298E2FE2D734",
        "years": [
          {
            "year": 2015
          },
          {
            "year": 2014
          },
          {
            "year": 2013
          },
          {
            "year": 2012
          },
          {
            "year": 2011
          },
          {
            "year": 2010
          },
          {
            "year": 2009
          }
        ]
      },
      "credentials": {
        "username": "steffen.krueger@sonoxo.com"
      },
      "logtext": "Log started at: May 26th 2015, 14:25:12\nwindow.hs_archive.isArchiveMobileDevice true"
    }

###Web
    {
        "build_id": "edge",
        "isQuarantineWeb": false,
        "isArchiveWeb": true,
        "isArchiveMobileDevice": false,
        "isArchiveMobileDeviceDev": false,
        "isEnhanchedLogging": false,
        "isDeveloperLog": false,
        "asemobile_footprint": {
            "Build": "edge",
            "Resolution_X": 1920,
            "Resolution_Y": 1046,
            "ColorDepth": 24,
            "UserAgent": "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36",
            "Timezone": -120,
            "Language": "de",
            "Os": "web",
            "OsVersion": ""
        },
        "sessiondata": {
            "search": {
                "page": 1,
                "direction": "both",
                "year": 2015,
                "limit": 30,
                "token": "F1BEAA7367A5516304723B6A17DAF714",
                "response": "json"
            },
            "preferences": {
                "isDemoMode": false
            },
            "token": "F1BEAA7367A5516304723B6A17DAF714",
            "years": [
                {
                    "year": 2015
                },
                {
                    "year": 2014
                },
                {
                    "year": 2013
                },
                {
                    "year": 2012
                },
                {
                    "year": 2011
                },
                {
                    "year": 2010
                },
                {
                    "year": 2009
                }
            ]
        },
        "credentials": {
            "username": "steffen.krueger@sonoxo.com",
            "data": "w8soS/LyKhDTEzVpEJBnh61eX7C4RZBbWIjDB7RKTwQAjO3ihFnoN6XXYOo/FD6Cu8BPRAoKneJz6dUkcQoUjk8EDQP6UTGPh0gogHEUHkhv7YbCJYY1zqOlvQzeZ2KpJ4R1UQ0UABhis1ga+9lA0Q=="
        },
        "logtext": "Log started at: May 26th 2015, 14:28:31\nwindow.hs_archive.isArchiveMobileDevice false"
    }

### Web, Demo mode
    {
        "build_id": "edge",
        "isQuarantineWeb": false,
        "isArchiveWeb": true,
        "isArchiveMobileDevice": false,
        "isArchiveMobileDeviceDev": false,
        "isEnhanchedLogging": false,
        "isDeveloperLog": false,
        "asemobile_footprint": {
            "Build": "edge",
            "Resolution_X": 1920,
            "Resolution_Y": 1046,
            "ColorDepth": 24,
            "UserAgent": "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36",
            "Timezone": -120,
            "Language": "de",
            "Os": "web",
            "OsVersion": ""
        },
        "sessiondata": {
            "search": {
                "page": 1
            },
            "preferences": {
                "isDemoMode": true
            },
            "token": null,
            "years": null
        },
        "credentials": {
            "username": null,
            "data": null
        },
        "logtext": "Log started at: May 26th 2015, 14:28:31\nwindow.hs_archive.isArchiveMobileDevice false"
    }


##Data
Die App wurde auf Basis der früheren API von Antispameurope entwickelt. Dadurch beinhaltet sie einige
Workarounds die eigentlich durch Anpassungen im Backend besser umgesetzt werden können. Auch die Keys in den json-string unterscheiden sich. Zu beachten ist hier insbesondere die Groß-/Kleinschreibung. 


###Archive from REST
    
    {
      "id": 109662,
      "Subject": "Re: API Doc für neue App funktionalität",
      "Date": "2015-03-17 15:36:55.0",
      "direction": "OUT",
      "To": "rieke@hornetsecurity.com",
      "attachments": "",
      "attachment": false,
      "From": "marketing.krueger@sonoxo.com",
      "Size": 11,
      "private": false,
      "_id": "2015_109662",
      "year": 2015
    }

###Quarantine from REST
    
    {
      "id": "dHJhY2tpbmdfc29ub3hvX2NvbTowOjE3NTY3",
      "mail_to": "journaling@archiv2.simple-cloud.com",
      "mail_direction": 2,
      "host": "mailout02.exchange-anywhere.de[93.186.203.3].1434",
      "mail_date": "2014-12-01 11:15:03.0",
      "mail_from": "marketing.krueger@sonoxo.com",
      "mail_size": 3969,
      "msg_id": "Yjk0MzkzMzQwOWEzOWU5NWZkMWFkMTM0NTU0MjgzZGYtYTc5Zjg2N2FmNjc1N2MyMTBlMGQ3MzE5NjFhNTllMDU=",
      "mail_subject": "Akzeptiert: Brainstormingmeeting",
      "message_id": "OTE1YWUyYjYtNGJkNy00NjY2LTgxN2YtNjdmZmFhZDZmM2M1QGpvdXJuYWwucmVwb3J0LmdlbmVyYXRvcg==",
      "crypttype": 1,
      "server": "mx-gate24-haj2"
    }

###Data for Maillist / Details (siehe z.B. Templates MailListItemClick.html und ArchiveMailDetails.html)

wie archive

    {
      "id": 109662,
      "Subject": "Re: API Doc für neue App funktionalität",
      "Date": "2015-03-17 15:36:55.0",
      "direction": "OUT",
      "To": "rieke@hornetsecurity.com",
      "attachments": "",
      "attachment": false,
      "From": "marketing.krueger@sonoxo.com",
      "Size": 11,
      "private": false,
      "_id": "2015_109662",
      "year": 2015
    }

steffen.krueger@sonoxo.com