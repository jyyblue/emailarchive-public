
module.exports = function (grunt) {

    'use strict';
    // Force use of Unix newlines
    grunt.util.linefeed = '\n';
    RegExp.quote = function (string) {
        return string.replace(/[-\\^$*+?.()|[\]{}]/g, '\\$&');
    };
    // Project configuration.
    grunt.initConfig({
        // bootstrap
        pkg: grunt.file.readJSON('public_html/libs/bootstrap/package.json'),
        // different build targets - set in task 
        buildtraget: '/dist/<%= build_target%>',
        project: {
            "dist": "public_html/libs/bootstrap/dist/css/"
        },
        less: {
            compileCore: {
                options: {
                    strictMath: true,
                    sourceMap: true,
                    outputSourceFiles: true,
                    sourceMapURL: '<%= pkg.name %>.css.map',
                    sourceMapFilename: '<%= project.dist %><%= pkg.name %>.css.map'
                },
                files: {
                    '<%= project.dist %><%= pkg.name %>.css': 'public_html/less/styles.less'
                }
            },
            minify: {
                options: {
                    cleancss: true,
                    report: 'min'
                },
                files: {
                    '<%= project.dist %><%= pkg.name %>.min.css': '<%= project.dist %><%= pkg.name %>.css'
                }
            }
        },
        requirejs: {
            archive_mobile: {
                options: {
                    onBuildWrite: function (name, path, contents) {
                        if (name === '../js/main') {
						
                            contents = contents.replace('var useCustomizedLogOrFilter = false;', 'var useCustomizedLogOrFilter = true;'); // comment this line for debug output
                            contents = contents.replace('window.hs_archive.build_id = "edge";', 'window.hs_archive.build_id = "' + getBuildId() + '";');
                            contents = contents.replace('window.hs_archive.isQuarantineWeb = true;', 'window.hs_archive.isQuarantineWeb = false;');
                            contents = contents.replace('window.hs_archive.isArchiveWeb = true;', 'window.hs_archive.isArchiveWeb = false;');
                            contents = contents.replace('window.hs_archive.isArchiveMobileDevice = false;', 'window.hs_archive.isArchiveMobileDevice = true;');
                            contents = contents.replace('window.hs_archive.isArchiveMobileDeviceDev = true;', 'window.hs_archive.isArchiveMobileDeviceDev = false;');
                            contents = contents.replace('window.hs_archive.isDeveloperLog = true;', 'window.hs_archive.isDeveloperLog = false;'); // comment this line for debug output
                        }
                        return contents;
                    },
                    "baseUrl": "public_html/libs",
                    "mainConfigFile": "public_html/js/main.js",
                    "include": ["almond/almond.js"],
                    "insertRequire": ["../js/main"],
                    "name": "../js/main",
                    "out": "public_html/<%= buildtraget %>/dist.js",
                    "optimize": "none",
					// "optimize": "uglify2",
                    preserveLicenseComments: false,
                    uglify2: {
                        warnings: true,
                        mangle: true
                    }
                }
            },
            archive_web: {
                options: {
                    onBuildWrite: function (name, path, contents) {
                        if (name === '../js/main') {
                            contents = contents.replace('var hideAllLogging = false;', 'var hideAllLogging = true;'); // comment this line for debug output
                            contents = contents.replace('window.hs_archive.build_id = "edge";', 'window.hs_archive.build_id = "' + getBuildId() + '";');
                            contents = contents.replace('window.hs_archive.isArchiveWeb = false;', 'window.hs_archive.isArchiveWeb = true;');
                            contents = contents.replace('window.hs_archive.isQuarantineWeb = true;', 'window.hs_archive.isQuarantineWeb = false;');
                            contents = contents.replace('window.hs_archive.isArchiveMobileDeviceDev = true;', 'window.hs_archive.isArchiveMobileDeviceDev = false;');
                            contents = contents.replace('window.hs_archive.isArchiveMobileDevice = true;', 'window.hs_archive.isArchiveMobileDevice = false;');
                            contents = contents.replace('window.hs_archive.isDeveloperLog = true;', 'window.hs_archive.isDeveloperLog = false;'); // comment this line for debug output
                        }
                        return contents;
                    },
                    "baseUrl": "public_html/libs",
                    "mainConfigFile": "public_html/js/main.js",
                    "include": ["almond/almond.js"],
                    "insertRequire": ["../js/main"],
                    "name": "../js/main",
                    "out": "public_html/<%= buildtraget %>/dist.js",
                    "optimize": "none",
                    // "optimize": "uglify2",
                    preserveLicenseComments: false,
                    uglify2: {
                        warnings: true,
                        mangle: true
                    }
                }
            },
            quarantine_web: {
                options: {
                    onBuildWrite: function (name, path, contents) {
                        if (name === '../js/main') {
                            contents = contents.replace('var hideAllLogging = false;', 'var hideAllLogging = true;'); // comment this line for debug output
                            contents = contents.replace('window.hs_archive.build_id = "edge";', 'window.hs_archive.build_id = "' + getBuildId() + '";');
                            contents = contents.replace('window.hs_archive.isQuarantineWeb = false;', 'window.hs_archive.isQuarantineWeb = true;');
                            contents = contents.replace('window.hs_archive.isArchiveWeb = true;', 'window.hs_archive.isArchiveWeb = false;');
                            contents = contents.replace('window.hs_archive.isArchiveMobileDeviceDev = true;', 'window.hs_archive.isArchiveMobileDeviceDev = false;');
                            contents = contents.replace('window.hs_archive.isArchiveMobileDevice = true;', 'window.hs_archive.isArchiveMobileDevice = false;');
                            contents = contents.replace('window.hs_archive.isDeveloperLog = true;', 'window.hs_archive.isDeveloperLog = false;'); // comment this line for debug output
                        }
                        return contents;
                    },
                    "baseUrl": "public_html/libs",
                    "mainConfigFile": "public_html/js/main.js",
                    "include": ["almond/almond.js"],
                    "insertRequire": ["../js/main"],
                    "name": "../js/main",
                    "out": "public_html/<%= buildtraget %>/dist.js",
                    "optimize": "none",
                    // "optimize": "uglify2",
                    preserveLicenseComments: false,
                    uglify2: {
                        warnings: true,
                        mangle: true
                    }
                }
            },
            spa: {
                options: {
                    "baseUrl": "public_html/libs",
                    "mainConfigFile": "public_html/js/main.js",
                    "include": ["almond/almond.js"],
                    "insertRequire": ["../js/main"],
                    "name": "../js/main",
                    "out": "public_html/<%= buildtraget %>/dist.js",
                    "optimize": "none"
                }
            },
            css: {
                options: {
                    "cssIn": "public_html/css/styles.css",
                    "out": "public_html/<%= buildtraget %>/css/styles.css"
                }
            }
        },
        clean: {
            mobile: {
                src: [
                    "www/**/*.*",
                    "public_html/dist/spa/**/*.*"
                ]
            },
            aweb: {
                src: [
                    "public_html/dist/archiveweb/**/*.*"
                ]
            },
            qweb: {
                src: [
                    "public_html/dist/quarantineweb/**/*.*"
                ]
            }
        },
        copy: {
            qd: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        cwd: 'public_html/files/glyph/',
                        src: ['**'],
                        dest: 'public_html/libs/bootstrap/dist/fonts/'
                    }
                ]
            },
            libs: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        cwd: 'public_html/libs/bootstrap/dist/fonts/',
                        src: ['**'],
                        dest: 'public_html/<%= buildtraget %>/libs/bootstrap/dist/fonts/'
                    },
                    {
                        src: 'public_html/libs/modernizr/modernizr.js',
                        dest: 'public_html/<%= buildtraget %>/libs/modernizr/modernizr.js'
                    },
                    {
                        expand: true,
                        cwd: 'public_html/resources/',
                        src: ['**'],
                        dest: 'public_html/<%= buildtraget %>/resources/'
                    },
                    {
                        src: 'public_html/index_spa.html',
                        dest: 'public_html/<%= buildtraget %>/index.html'
                    },
                    {
                        expand: true,
                        cwd: 'public_html/data/',
                        src: ['**'],
                        dest: 'public_html/<%= buildtraget %>/data/'
                    }
                ]
            },
            cordova: {
                files: [
                    {
                        // TODO exclude modernizr
                        expand: true,
                        cwd: 'public_html/<%= buildtraget %>/',
                        src: ['**'],
                        dest: 'www/'
                    },
                    {
                        src: 'public_html/index_cordova.html',
                        dest: 'www/index.html'
                    },
                    {
                        // src: 'public_html/files/config.xml',
                        src: 'config.xml',
                        dest: 'www/config.xml'
                    }

                ]
            }
        },
        cordovacli: {
            options: {
                path: './'
            },
            cordova: {
                options: {
                    command: ['plugin', 'build'],
                    platforms: ['ios', 'android'],
                    plugins: ['device', 'dialogs']
                }
            },
            add_plugins: {
                options: {
                    command: 'plugin',
                    action: 'add',
                    plugins: [
                        'org.apache.cordova.file',
                        'org.apache.cordova.file-transfer',
                        'de.appplant.cordova.plugin.email-composer',
                        'org.apache.cordova.device',
                        'org.apache.cordova.globalization',
                        'org.apache.cordova.inappbrowser',
                        'org.apache.cordova.network-information',
                        'https://github.com/pwlin/cordova-plugin-file-opener2',
                        'org.apache.cordova.console',
                        'org.apache.cordova.statusbar',
                        'https://github.com/apache/cordova-plugins.git#master:keyboard',
						'cordova-plugin-whitelist'
                    ]
//                    plugins: [
//                        'org.apache.cordova.file@1.1.0',
//                        'org.apache.cordova.file-transfer@0.4.1',
//                        'de.appplant.cordova.plugin.email-composer@0.8.1',
//                        'org.apache.cordova.device@0.2.8',
//                        'org.apache.cordova.globalization@0.2.6',
//                        'org.apache.cordova.inappbrowser@0.3.1',
//                        'org.apache.cordova.network-information@0.2.7',
////                        'https://github.com/pwlin/cordova-plugin-file-opener2',
//                        'https://github.com/break24/cordova-plugin-file-opener2',
//                        'org.apache.cordova.console@0.2.7',
//                        'org.apache.cordova.statusbar',
//                        'https://github.com/apache/cordova-plugins.git#master:keyboard'
//                    ]
                }
            },
            plugins_removeall: {
                options: {
                    command: 'plugin',
                    action: 'rm',
                    plugins: [
                        'org.apache.cordova.file-transfer',
                        'org.apache.cordova.file',
                        'de.appplant.cordova.plugin.email-composer',
                        'org.apache.cordova.device',
                        'org.apache.cordova.globalization',
                        'org.apache.cordova.inappbrowser',
                        'org.apache.cordova.network-information',
                        'io.github.pwlin.cordova.plugins.fileopener2a',
                        'org.apache.cordova.console',
                        'org.apache.cordova.statusbar',
                        'org.apache.cordova.labs.keyboard'

                    ]
                }
            },
            build_ios: {
                options: {
                    command: 'build',
                    platforms: ['ios']
                }
            },
            build_android: {
                options: {
                    command: 'build',
                    platforms: ['android']
                }
            },
            run_android: {
                options: {
                    command: 'run',
                    platforms: ['android']
                }
            },
            emulate_android: {
                options: {
                    command: 'emulate',
                    platforms: ['android']
                }
            },
            emulate_ios: {
                options: {
                    command: 'emulate',
                    platforms: ['ios']
                }
            }
        },
        replace: {
            jsaddversion: {
                src: ['public_html/<%= buildtraget %>/index.html'], // source files array (supports minimatch)
                dest: 'public_html/<%= buildtraget %>/index.html', // destination directory or file
                replacements: [{
                        from: 'src="dist.js"', // string replacement
                        to: 'src="dist.js?version=' + getBuildIdTime() + '"'
                    }]
            }
        }
    });
    require('load-grunt-tasks')(
            grunt,
            {scope: 'devDependencies'}
    );

    grunt.registerTask('alldone', 'Finish Message', function () {
        grunt.log.writeln('####################################################');
        grunt.log.writeln('###                                              ###');
        grunt.log.writeln('###             SPA was build                    ###');
        grunt.log.writeln('###                                              ###');
        grunt.log.writeln('####################################################');
    });
    grunt.registerTask('default', 'Main Tasks:', function () {
        grunt.log.writeln('####################################################\n\n');
        grunt.log.writeln('Main tasks:\n');
        grunt.log.writeln('  aweb: Archive HTML 5\n');
        grunt.log.writeln('  qweb: Quratine HTML 5\n');
        grunt.log.writeln('  arun: Mobile Android build and run on local device\n');
        grunt.log.writeln('  ios: Mobile ios (on mac) build for XCode\n');
        grunt.log.writeln('\n\n####################################################');
    });
    // CSS distribution task.
    grunt.registerTask('test', 'Test a task', function (n) {
        // >grunt test:001
        console.log(n);
    });

    // cordova
    grunt.registerTask('cprm', 'Remove all cordova plugins ', ['cordovacli:plugins_removeall']);
    grunt.registerTask('cpadd', 'Add plugins for cordova - !!EDIT GRUNTFILE BEFORE!!', ['cordovacli:add_plugins']);

    grunt.registerTask('css', 'Create bootstrap.<min>.css from less', ['less']);
    grunt.registerTask('spa_sub', 'Create SPA files css and js using r.js and Almond.js', ['requirejs:spa', 'requirejs:css']);
    grunt.registerTask('mobile_sub', 'Create SPA files css and js using r.js and Almond.js',
            ['requirejs:archive_mobile', 'requirejs:css']
            );
    grunt.registerTask('web_sub', 'Create SPA files css and js using r.js and Almond.js', ['requirejs:archive_web', 'requirejs:css']);
    grunt.registerTask('qd', 'Quick and dirty fixes for web-designer', ['copy:qd']);
    grunt.registerTask('libs', 'Copy parts from /libs directory for later use in SPA', ['copy:libs']);
    grunt.registerTask('quarantine_sub', 'Create SPA files css and js using r.js and Almond.js', ['requirejs:quarantine_web', 'requirejs:css']);

    grunt.task.registerTask('initBuildId', '', setBuildId);

    grunt.registerTask('aemu', 'Copy current SPA state and emulate', function () {
        grunt.config.set('build_target', 'spa');
        grunt.task.run('mobile_sub', 'copy:cordova', 'cordovacli:emulate_android');
    });
    grunt.registerTask('iemu', 'Copy current SPA state and emulate', function () {
        grunt.config.set('build_target', 'spa');
        grunt.task.run('mobile_sub', 'copy:cordova', 'cordovacli:emulate_ios');
    });
    grunt.registerTask('android', 'Copy current SPA state and emulate', function () {
//        grunt.task.run('mobile_sub', 'copy:cordova', 'cordovacli:build_android');
        grunt.config.set('build_target', 'spa');
        grunt.task.run('mobile_sub', 'copy:cordova', 'cordovacli:build_android');
    });
    grunt.registerTask('spa', 'Complete build for HTML 5 including less, copy libs & demo data, r.js', function () {
//        grunt.config.set('build_target', 'spa');
        grunt.task.run('default');
    });

    // **** MAIN ****
    grunt.registerTask('arun', '### Mobile Android - complete Build AND run on an attached device', function () {
        grunt.config.set('build_target', 'spa');
        grunt.task.run('less', 'clean:mobile', 'mobile_sub', 'copy:qd', 'copy:libs', 'copy:cordova', 'cordovacli:run_android');
    });
    grunt.registerTask('ios', '### Mobile iOS -complete build ', function () {
        grunt.config.set('build_target', 'spa');
        grunt.task.run('less', 'clean:mobile', 'mobile_sub', 'copy:qd', 'copy:libs', 'copy:cordova', 'cordovacli:build_ios');
    });
    grunt.registerTask('aweb', '### Archie - Complete build for HTML 5 including less, copy libs & demo data, r.js', function () {
        grunt.config.set('build_target', 'archiveweb');
        grunt.task.run('less', 'clean:aweb', 'web_sub', 'copy:qd', 'copy:libs', 'replace:jsaddversion', 'alldone');
    });
    grunt.registerTask('qweb', '###     Quarantaine - Complete build for HTML 5 including less, copy libs & demo data, r.js', function () {
        grunt.config.set('build_target', 'quarantineweb');
        grunt.task.run('less', 'clean:qweb', 'quarantine_sub', 'copy:qd', 'copy:libs', 'replace:jsaddversion', 'alldone');
    });

    // ##############################################################################################################

    function getBuildId() {
        if (!global["build_id_time"]) {
            setBuildId();
        }
        console.log("Build: " + global["build_id"]);
        console.log('Time: ' + global["build_id_time"]);
        return global["build_id"];
    }
    function getBuildIdTime() {
        if (!global["build_id_time"]) {
            setBuildId();
        }
        console.log("Build: " + global["build_id"]);
        console.log('Time: ' + global["build_id_time"]);
        return global["build_id_time"];
    }

    function setBuildId() {
        var buildParameter = grunt.option('build');
        if (buildParameter) {
            console.log(buildParameter);
        } else {
            console.log('Generic build');
        }
        if (buildParameter) {
            var n = new Date();
            var m = n.getMonth() > 8 ? "." + (n.getMonth() + 1) : ".0" + (n.getMonth() + 1);
            var d = n.getDate() > 9 ? n.getDate() : "0" + n.getDate();
            // q+d
            var idt = n.getTime().toString();
            global["build_id_time"] = idt[0] + "." + idt[1] + idt[2] + idt[3] + "." + idt[4] + idt[5] + idt[6] + "." + idt[7] + idt[8] + idt[9] + "." + idt[10] + idt[11] + idt[12];
            global["build_id"] = n.getFullYear() + m + d + "-" + buildParameter;
        } else {
            var n = new Date();
            var m = n.getMonth() > 8 ? "." + (n.getMonth() + 1) : ".0" + (n.getMonth() + 1);
            var d = n.getDate() > 9 ? n.getDate() : "0" + n.getDate();
            // q+d
            var idt = n.getTime().toString();
            // global["build_id_time"] = idt[0] + "." + idt[1] + idt[2] + idt[3] + "." + idt[4] + idt[5] + idt[6] + "." + idt[7] + idt[8] + idt[9] + "." + idt[10] + idt[11] + idt[12];
            global["build_id_time"] = idt[10] + idt[11] + idt[12];
            global["build_id"] = n.getFullYear() + m + d + "-" + global["build_id_time"];
        }
    }

};
