# Skeleton 2014.0424

##Node.js and npm

Download and install [NodeJS](http://nodejs.org/download/) to have node.js and npm available.

##Build tools and libraries

### repeat this on all os/coumpters (windows, mac, linux) part of the development
Checkout the repository and open directory in terminal. All javascript related files can be found below public_html directory. node-moduls are only used for build. Install following modules global
    
    
    $sudo npm install -g grunt-cli
    $sudo npm install -g bower
    // ?? on macos
    // ?? sudo npm -g install cca

    // list what you've done
    npm -g list -depth=0

For later use - if you need to update node and npm  (on MacOS, Linux ONLY)

    // clear you npm cache
    sudo npm cache clean -f 
    
    sudo npm install -g n 
    
    // upgrade to lastest version
    sudo n stable 
    
    // list what you've done
    npm list -depth=0    

###Install local Build-Tools for project
Installation are configured in public_html/package.json. Run

	$ npm install [--dedupe]
	
to install all tools. (Parameter to move dependencies further up the tree)

###Dependency management
Libraries will be installed using bower. Add/remove libraries in project_root/bower.json. Bower's default directory is changed to public_html/libs in .bowerrc

	$ bower install

will install and update all javaascript components/packages/libraries.	
	
###Grunt - the taskrunner
Grunt was installed by npm. All task are configured in Gruntfile.js 
Run 

	grunt --help 

to figure out current available tasks. 

- compile less to css (Bootstrap) 
- copy resources 
- Merge and uglify Javascript and CSS
- Build complete HTML5 application
- run cordova tasks

Main tasks
    
    grunt arun
    grunt ios
    grunt aweb

###AMD - Require.js and r.js
RequireJS is used for file and module loading (http://requirejs.org/), see http://requirejs.org/docs/whyamd.html for more deatils about AMD. 

require.config is located in ASEPrototype\public_html\js\main.js where the app will be initialized.

r.js is used for optimization. It combines related scripts together into build layers and minifies them via UglifyJS and it optimizes CSS by inlining CSS files referenced by @import and removing comments.

##Cordova
### Pre-requisites

Install the [Android SDK](http://developer.android.com/sdk/index.html) or iOS (XCode) or ...

Install [Cordova](https://cordova.apache.org/):

	$ npm install -g cordova

### ONLY if no code exists! will overwite config.xml Create project

	$ cordova create hello com.break24.cordova.hello "HelloWorld"

###Add a platform
	
	$ cordova platform add ios|android|..

Remove / add platform after plugin was deleted/reinstalled or id in config.xml was changed

###icon&splash

needs imagemagick, see https://github.com/AlexDisler/cordova-splash

Install npm module
    
    npm -g install cordova-icon
    npm -g install cordova-splash
    
This modules will convert icon.png and splash.png from root directory. Andoid has a transparent backbround, ios not.

##Loginng Android
adb logcat -c
adb logcat CordovaLog:V *:S

##IDE (Netbeans)

New HTML5 Project with existing sources. Point Site-route to public_html

###Good to know
Object-folding
    
    Close: STR+SHIFT+LEFT-CLICK
    Open: STR+LEFT-CLICK

## Gitignore

###Adding local project to GitHub

    git remote add origin remote_repository_URL
    git push -u origin master

###Add following lines to .gitignore

	# Folders to ignore
	node_modules
	libs
	nbproject
	config
	hooks
	merges
	platforms
	plugins
	test
	www

##Directories below pulic html

###/css
styles.css import all css files, including a new compield bootstrap.css
ap.css - add all css canges here

###/data
contains all json files for local ajax calls.

###/dist
output directory for build

###/files
files will be copy to target

###/js
The CODE for the app

###/less
configure bootstrap mixins and compile them using grunt
styles.less import all less files, including bootstrap.less
app.less overwrite default bootstrap values

###/libs 
bower's directory - dont care
Delete directory and run
    
    bower install

and everything is working.

###/resources 
images, fonts and other non-css assets


##Directories and files in project directory

###/.git
version control

###/hooks
use this to hook into cordovas build process

###/nbproject
Net beans configuration files

###/node_modules 
npm's directory - dont care

###/platforms
cordova - target platforms available on this computer

###/plugins
cordova - plugins, installable by cordovacli & grunt (see Gruntfile.js)

###/www
copy from dist for cordova build 

###important files
.bowerrc
.gitattributes
.gitignore
bower.json
config.xml
Gruntfile.js
package.json
*.md



	
