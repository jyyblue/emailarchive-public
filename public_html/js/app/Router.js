/*
 * set gloal listener
 * manage routes to mailbox, preferences and faq
 * manage modal views
 * helpers 
 * - history/back button
 * - orientation change
 * - clear collections or prefernces
 * - check OS
 * - screen size
 */

define(function (require) {


    "use strict";
    var Backbone = require('backbone');
    var $ = require('jquery');
    var _ = require('Underscore');

    // views
    var StaticPageView = require('app/views/generic/StaticPage');
    var PreferencesView = require('app/views/Preferences');
    var MailBoxView = require('app/views/MailBox');
    var ShellView = require('app/views/Shell');
    var WaitForUserAction = require('app/views/generic/WaitForUserAction');

    // templates
    var helpTemplate = require('hbs!app/tpl/Help');

    // models
    var sd = require('app/models/StaticData');
    var ApiLogin = require('app/models/ApiLogin');

    // Archive
    var ApiArchiveSearch = require('app/models/ApiArchiveSearch');
    var ArchiveYear = require('app/models/ApiArchiveGetYears');
    var ArchiveDetailsView = require('app/views/ArchiveMailDetails');

    // Quarantine
    var ApiQuarantineSearch = require('app/models/ApiQuarantineSearch');
    var QuarantineDetailsView = require('app/views/QuarantineMailDetails');


    // TODO move to class
    var shellView = null;

    return Backbone.Router.extend({
        selectedId: null,
        searchResultCollection: null,
        mailboxView: null,
        preferencesView: null,
        helpView: null,
        waitForUserAction: null,
        currentView: '',
        lastViewWasTwoPages: false,
        isStart: true,
        mailListType: '',
        routes: {
            '': 'start',
            'archive': 'mailbox',
            'quarantine': 'mailbox',
            'enhancedsearch': 'searchForm',
            'archive(/:id)': 'mailDetails',
            'quarantine(/:id)': 'mailDetails',
            'loadnextpage/:apipage': 'loadnextpage',
            'help': 'help',
            'preferences': 'preferences',
            'noPageInfo': 'noPageInfo',
            '*path': 'noPageInfo'
        },
        initialize: function () {
            // init shell view
            shellView = new ShellView({
                el: $('#asearchive_app'), 
                authenticateOnStartup: window.hs_archive.isArchiveMobileDevice
            }).render();
            var self = this;
            // this event is triggered from odels if ANY (!) error occurs during ajax request - primarily used for security reasons after timeouts
            this.on('resetsession', function () {
                location.reload();
            });

            // only print os for logging
            this.checkOperatingSystem();

            // INFO check routes - mainly for development
            this.on('route', function (route, params) {
//                console.log(this._events.route);
                console.log("Router: eventlistener on route: " + route + ", CurrentView is: " + self.currentView);
//                                this.currentView = 'mailDetails';

//                console.log("Router: route parameter: " + JSON.stringify(params));
                // console.log(Backbone.history.history);
            });
            function doOnOrientationChange() {
                console.log("Router >> event orientation change");

                switch (window.orientation) {
                    case -90:
                    case 90:
                        console.log('Router: changed to landscape');
                        self.updateScreenAfterOrientationChange();
                        break;
                    default:
                        console.log('Router: changed to portrait');
                        self.updateScreenAfterOrientationChange();
                        break;
                }
            }
            window.addEventListener('orientationchange', doOnOrientationChange);


            // Android hardware backbutton
            document.addEventListener("backbutton", this.goBackInHistory, false);

            // add cordova specific functions
            if (window.hs_archive.isArchiveMobileDevice) {
                // WORKAROUND 2014 for ios:
                if (typeof Keyboard !== 'undefined') {
                    /* Virtual keyboards
                     * Also, note that if you're using inputs in your modal or navbar, iOS has a 
                     * rendering bug that doesn't update the position of fixed elements when the 
                     * virtual keyboard is triggered. A few workarounds for this include transforming 
                     * your elements to <code>position: absolute</code> or invoking a timer on focus 
                     * to try to correct the positioning manually. This is not handled by Bootstrap, 
                     * so it is up to you to decide which solution is best for your application.
                     */

                    console.log("Router: load keyboard for iOS");
                    // Using an ios-only plugin for keyboard-event in iphones ...
                    Keyboard.onshowing = function () {
                        console.log("Router: keyboard onshowing");
                        window.hs_archive_modules.router.mailboxView.scrollListToTop();
                        $("#footer").css('visibility', 'hidden');
                    };
                    Keyboard.onshow = function () {
                        console.log("Router: Keyboard onshow");
                        // TODO check if needed in future - see shell.js
                        // self.ioskeyboard = true;
                        // console.log($("#header").css('position'));
                        $("#header").css('position', 'absolute');
                    };
                    Keyboard.onhide = function () {
                        console.log("Router: Keyboard onhide");
//                        $("#footer").css('visibility', 'hidden');
                        $("#footer").css('visibility', 'visible');
                        $("#header").css('position', 'fixed');
                        $("#footer").css('position', 'fixed');
                        // TODO check if needed in future - see shell.js
                        // self.ioskeyboard = false;
                    };
                }
                // WORKAROUND 2014 for ios: default behavior - check if needed
                document.addEventListener("showkeyboard", function () {
                    console.log("Router: Keyboard is ON");
                }, false);
                document.addEventListener("hidekeyboard", function () {
                    console.log("Router: Keyboard is OFF");
                }, false);
                if (typeof StatusBar !== 'undefined') {
                    StatusBar.backgroundColorByHexString(sd.iosStatusbarBackground);
                }
            }

            _.bindAll(this, 'goBackInHistory');

            if (window.hs_archive.isArchiveWeb) {
                // for web-client after submit
                var transferObjectBlob = Session.get('transferObject');
                if (transferObjectBlob) {
                    console.log("Router: WebClient reloaded");
                    window.hs_archive.sessiondata.token = this.getPersistedSessionData(transferObjectBlob);
                    Session.clear();
                } else {
                    console.warn("Router: No Session data.");
                }
            }

            // is archive or quaratine
            if (window.hs_archive.isQuarantineWeb) {
                // init quarantine
                this.mailListType = 'quarantine';
                this.detailsViewClass = QuarantineDetailsView;
                this.searchResultCollection = new ApiQuarantineSearch();
            } else if (window.hs_archive.isArchiveWeb || window.hs_archive.isArchiveMobileDevice) {
                // init archive
                this.mailListType = 'archive';
                this.detailsViewClass = ArchiveDetailsView;
                this.searchResultCollection = new ApiArchiveSearch();
            }


            // after initialize start is called (see routes)

        },
        goBackInHistory: function (e) {

            console.log('Router: goBackInHistory - currentView is: ' + window.hs_archive_modules.router.currentView);
            console.log('Router: window.hs_archive_modules.router.isMobileTwoPages(): ' + window.hs_archive_modules.router.isMobileTwoPages());
            if (window.hs_archive_modules.router.currentView === 'preferences') {
                if (navigator.app) {
                    navigator.app.exitApp();
                } else {
                    location.reload();
                }
            } else if (window.hs_archive_modules.router.isMobileTwoPages()) {
                if (window.hs_archive_modules.router.currentView === 'mailDetails') {

                    window.hs_archive_modules.router.mailboxView.mailList();

                } else if (window.hs_archive_modules.router.currentView === 'mailList'
                        || window.hs_archive_modules.router.currentView === 'help') {

                    window.hs_archive_modules.router.navigate('preferences', {trigger: true});

                } else if (window.hs_archive_modules.router.currentView === 'searchForm') {

                    window.hs_archive_modules.router.navigate('archive');
                    window.hs_archive_modules.router.mailboxView.mailList();
                } else {
                    console.warn('Should not happen ...');
                    window.history.go(-1);
                }
            } else {
                if (
                        window.hs_archive_modules.router.currentView === 'mailList'
                        || window.hs_archive_modules.router.currentView === 'mailDetails'
                        || window.hs_archive_modules.router.currentView === 'help') {
                    window.hs_archive_modules.router.navigate('preferences', {trigger: true});
                } else if (window.hs_archive_modules.router.currentView === 'searchForm') {

                    window.hs_archive_modules.router.navigate('archive');
                    window.hs_archive_modules.router.mailboxView.mailList();
                } else {
                    console.warn('Should not happen ...');
                    window.history.go(-1);
                }
            }
        },
        getPersistedSessionData: function (transferObjectBlob) {
            var apiLogin = new ApiLogin();
            // console.log(transferObjectBlob);
            var transferObject = apiLogin.decode(transferObjectBlob);
            console.log('Router: Transferred data: HIDDEN');
            // console.log(JSON.stringify(transferObject));

            if (_.isNumber(transferObject.errorcode)) {
                this.addModalView({
                    title: '__base.error',
                    description: '__login.errorcode_' + transferObject.errorcode,
                    action: '__base.close'
                });
            } else {
                // persist in memory
                apiLogin.writeCredentials({
                    username: transferObject.username,
                    password: transferObject.password,
                    token: transferObject.token
                });
                return transferObject.token;
            }

        },
        start: function () {
            var token = window.hs_archive.sessiondata.token;
            var years = window.hs_archive.sessiondata.years;
            var isToken = !_.isNull(token) && !_.isUndefined(token);
            var isYears = !_.isNull(years) && !_.isUndefined(years);
            console.log('Router: token: '
                    + isToken + ' years: '
                    + isYears + ' isStart: ' + this.isStart);
            // set years for achive to collection if needed and available

            if (window.hs_archive.isQuarantineWeb) {
                console.log('Router: set years as dummy');
                isYears = [];
            } else if (isYears && typeof this.searchResultCollection.setYears === 'function') {
                this.searchResultCollection.setYears(years);
            }
            var self = this;
            if ((window.hs_archive.isArchiveWeb || window.hs_archive.isArchiveMobileDevice)
                    && window.hs_archive.sessiondata.preferences.isDemoMode) {
                // user is authorized for demo mode (only available in archive)
                this.isStart = false;
                this.addModalView({
                    title: '__base.info',
                    description: '__preferences.demomode_msg',
                    action: '__base.close'
                });
                self.mailbox({showHomeScreen: true});
            } else if (isToken) {
                // user was authorized during init or after login, only for mobile version
                this.isStart = false;
                if (isToken && !isYears) {

                    if (!window.hs_archive.isQuarantineWeb) {
                        // not used for quarantine
//                        console.log("Router: update years");
                        var years = new ArchiveYear();
                        years.fetch({token: token,
                            success: function () {
                                window.hs_archive.sessiondata.years = years;
                                self.searchResultCollection.setYears(years);
                                // configure interface now
                                console.log("Router: authorized and years");

                                self.mailbox({showHomeScreen: true});
                            }, error: function () {
                                // TODO ? check how to handle error
                                self.configInterface();
                            }
                        });
                    }
                } else {
//                    console.log("Router: authorized");
                    self.mailbox({showHomeScreen: true});
                }
            } else {
                // Web-Client all versions, Wrong credetials for initial login, credentials was changed or NO Connection
                console.log("Router: No login during init.");
                // TODO ? next line could be removed
                shellView.enableFooterMenuButtonState(false);
                this.navigate('preferences', {trigger: true});
            }

        },
        // ####################### routes ####################################
        preferences: function (state) {
            console.log('Router: > preferences');
//            console.log('this.isStart is ' + this.isStart);
            if (this.isStart) {
                if (window.hs_archive.sessiondata.preferences.isDemoMode) {
                    shellView.enableFooterMenuButtonState(true);
                } else {
                    shellView.enableFooterMenuButtonState(false);
                }
                this.isStart = false;
            }
            shellView.setCurrentFooterButton('preferences');
            // ios fix
            $("#header").css('position', 'fixed');
            $("#footer").css('position', 'fixed');
            this.currentView = 'preferences';


            // 2015.0602 added static
            if (this.preferencesView === null) {
                var self = this;
                this.preferencesView = new PreferencesView({
                    el: shellView.getContentElement('preferences'),
                    resetCollection: function () {
                        if (self.mailboxView) {
                            self.mailboxView.clearMailCollection();
                        }
                        shellView.enableFooterMenuButtonState(window.hs_archive.sessiondata.preferences.isDemoMode);
                    },
                    navigateToApphome: function () {
                        self.start();
                    }
                });
            }
        },
        help: function () {
            console.log('Router: > help');
            if (this.isStart || (window.hs_archive.isArchiveWeb
                    && _.isEmpty(window.hs_archive.credentials)
                    && !window.hs_archive.sessiondata.preferences.isDemoMode)) {
                this.start();
            } else {
                this.currentView = 'help';
                shellView.setCurrentFooterButton('help');
                $("#header").css('position', 'fixed');
                $("#footer").css('position', 'fixed');
//            console.info("Router: function help");
                var language = (navigator.language) ? navigator.language : navigator.userLanguage;
                var separator = language.indexOf('-');
                if (separator !== -1) {
                    language = language.substr(0, separator);
                }
                if (this.helpView === null) {
                    this.helpView = new StaticPageView({
                        el: shellView.getContentElement('help'),
                        template: helpTemplate,
                        msg: {
                            // no navbar ... + 25px
                            height: this.getScreenHeightInPixel() + 25,
                            language: language,
                            appTitleLong: this.mailListType === 'archive' ? 'faq-archive-service-aeternum' : 'qurantine'
                        }
                    });
                }
            }
        },
        mailbox: function (showHomeScreen) {
            console.log('Router > mailbox');
            console.log(typeof this.mailboxView);
            if (this.mailboxView === null) {
                this.mailboxView = new MailBoxView({
                    el: shellView.getContentElement('mailbox'),
                    mailListType: this.mailListType,
                    apiCollection: this.searchResultCollection,
                    detailsViewClass: this.detailsViewClass,
                    isMobileTwoPages: this.isMobileTwoPages()
                });
            }

            $("#header").css('position', 'fixed');
            $("#footer").css('position', 'fixed');


            if (this.isStart) {
                // page reloaded
                console.log('Router: quarantine start');
                shellView.enableFooterMenuButtonState(false);
                this.navigate('preferences', {trigger: true});
            } else {
                shellView.enableFooterMenuButtonState(true);
                shellView.setCurrentFooterButton('mailbox');

                console.log('Router: navigate to ' + this.mailListType);
                this.navigate(this.mailListType);

                this.mailboxView.showListCalledFromRouter(
                        showHomeScreen
                        || window.hs_archive.sessiondata.preferences.isDemoMode);
            }
        },
        // ####################### helpers ####################################
        addModalView: function (options) {
            if (this.waitForUserAction) {
                this.waitForUserAction.hide();
            }
            options.static = true;
            this.waitForUserAction = new WaitForUserAction(options);
            this.waitForUserAction.show();
        },
        noPageInfo: function () {
            this.addModalView({
                title: '__base.error',
                description: '__app.page_does_not_exists',
                action: '__base.close'
            });
        },
        clearCredentialsAfterTimeout: function () {
            window.hs_archive.credentials = {};
            this.clearMailCollection();
            this.preferences();
        },
        clearMailCollection: function () {
            shellView.enableFooterMenuButtonState(false);
            if (this.mailboxView) {
                this.mailboxView.clearMailCollection();
            }
        },
        updateScreenAfterOrientationChange: function () {
            console.log('Router: >> UpdadteScreen after orientation change');
            console.log('Router: >> currentView is ' + window.hs_archive_modules.router.currentView);

            if (window.hs_archive_modules.router.currentView === 'help') {
                // TODO current version: repaint help complete - could be: set new height only
                window.hs_archive_modules.router.help();
            } else if ((window.hs_archive_modules.router.currentView === 'mailList')
                    || (window.hs_archive_modules.router.currentView === 'mailDetails')) {
                window.hs_archive_modules.router.mailboxView.setMailboxView({
                    newModeIsTwoPages: window.hs_archive_modules.router.isMobileTwoPages(),
                    currentView: window.hs_archive_modules.router.currentView,
                    screenHeightInPixel: window.hs_archive_modules.router.getScreenHeightInPixel()
                });
            } else if (window.hs_archive_modules.router.mailboxView !== null) {
                window.hs_archive_modules.router.mailboxView.setViewMode({
                    newModeIsTwoPages: window.hs_archive_modules.router.isMobileTwoPages()

                });
            }
        },
        getScreenHeightInPixel: function () {
            // console.info("Router: function getScreenHeightInPixel");
            return $(window).height() - sd.sizeInPixelForHeaderAndFooter;
        },
        isMobileTwoPages: function () {
            // console.info("Router: function isMobileTwoPages");
            return ((window.innerWidth
                    || document.documentElement.clientWidth
                    || document.body.clientWidth) < 768) && window.hs_archive.isArchiveMobileDevice;
        },
        isWebOnIOS: function () {
            if (window.hs_archive.asemobile_footprint.Os.toLowerCase() === 'web') {
                var iOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false);
                return iOS;
            } else {
                return false;
            }
        },
        checkOperatingSystem: function () {
            // TODO Needs to be updated for new OS-Versions
            // console.log("Router: OSString is "+window.hs_archive.asemobile_footprint.Os.toLowerCase());
            var osString = window.hs_archive.asemobile_footprint.Os.toLowerCase();
            var osAndVersion = 'web';
            if (/iphone|ipad|ipod|ios/i.test(osString)) {
                if (/7./i.test(osString)) {
                    osAndVersion = 'ios_gte_7';
                } else {
                    osAndVersion = 'ios_lt_7';
                }
            } else if (/android/i.test(osString)) {

                var version = window.hs_archive.asemobile_footprint.OsVersion;
                if (!version) {
                    // try get it from OS-String
                    var match = osString.match(/android\s([0-9\.]*)/);
                    if (match) {
                        version = match[1];
                    }
                }
                if (_.isEmpty(version)) {
                    // number conversion not successfull
                    console.warn('Router: Android version not feasible. Update sourcecode.');
                    if (/5./i.test(osString)) {
                        osAndVersion = 'android_gte_5';
                    } else if (/4.4/i.test(osString)) {
                        osAndVersion = 'android_gte_44';
                    } else {
                        osAndVersion = 'android_lt_44';
                    }
                } else {
                    if (parseFloat(version) < 4.4) {
                        osAndVersion = 'android_lt_44';
                    } else if (parseFloat(version) < 5) {
                        osAndVersion = 'android_gte_44';
                    } else {
                        osAndVersion = 'android_gte_5';
                    }
                }
            }
            // console.log(window.hs_archive.asemobile_footprint.Os.toLowerCase());
            console.log("Router: plattform is " + osAndVersion);
            return osAndVersion;
        }
    });
});