/*
 * Get attachment 
 * 
 */
define(function (require) {
    "use strict";
    var Backbone = require('backbone');
    var $ = require('jquery');
    var _ = require('Underscore');
    var sd = require('app/models/StaticData');
    var ApiLogin = require('app/models/ApiLogin');

    return Backbone.Model.extend({
        counter: sd.timeoutRetries,
        urlRoot: "",
        urlPost: sd.apiBaseUrl + '/archive/GetAttachment',
        requestid: null,
        fetch: function (options) {
            console.log("api-uri: " + this.urlPost);
            this.requestid = options.requestid;
            var searchParameter = {};
            searchParameter.id = options.mailid;
            searchParameter.year = options.year;
            searchParameter.name = options.filename.trim();
            searchParameter.token = window.hs_archive.sessiondata.token;
            searchParameter.response = "json";

            console.log('api-data: ' + JSON.stringify(searchParameter, null, 4));

            return this.postRequest(searchParameter, options);

        },
        postRequest: function (searchParameter, options) {

            var self = this;
            var request = $.ajax({
                type: "POST",
                url: self.urlPost,
                data: searchParameter,
                timeout: sd.attachmentTimeout
            });
            request.done(function (data, textStatus) {

                console.log("ApiArchiveGetAttachment: Request done, POST status is " + textStatus);
                console.log("ApiArchiveGetAttachment: Keys in GetAttachmentResponse.result: " + _.keys(data.GetAttachmentResponse.result));
                console.log("ApiArchiveGetAttachment: data: " + JSON.stringify(data.GetAttachmentResponse, null, 4));

                if (data.GetAttachmentResponse.result.data) {
                    console.log('data.GetAttachmentResponse.result.mimetype: ' + data.GetAttachmentResponse.result.mimetype);
                    var inputData;
                    try {
                        inputData = atob(data.GetAttachmentResponse.result.data);
                    } catch (e) {
                        console.warn('ApiArchiveGetAttachment: GetAttachmentResponse.result.data is NOT base64 encoded.');
                        inputData = data.GetAttachmentResponse.result.data;
                    }
                    console.log("ApiArchiveGetAttachment: Create binary for " + inputData.length + " Bytes");
                    var mimetype = data.GetAttachmentResponse.result.mimetype;
                    var filename = data.GetAttachmentResponse.result.filename;

                    var arrayBuffer = new ArrayBuffer(inputData.length);
                    var binArray = new Uint8Array(arrayBuffer);
                    for (var i = 0; i < inputData.length; i++) {
                        binArray[i] = inputData.charCodeAt(i);
                    }

                    // deprecated, use lower version in future - Android ? DONE now?
                    var osAndVersion = window.hs_archive_modules.router.checkOperatingSystem();
                    switch (osAndVersion) {
                        case 'ios_gte_7':
                        case 'ios_lt_7':
                        case 'android_gte_44':
                        case 'android_gte_5':
                        case 'web':
                            console.log("ApiArchiveGetAttachment: Use DataView");

                            var dataView = new DataView(arrayBuffer);
                            var blob = new Blob([dataView], {mimetype: mimetype});

                            console.log("ApiArchiveGetAttachment: Done.");
                            options.success(blob, {
                                filename: filename,
                                mimetype: mimetype,
                                requestid: self.requestid
                            });
                            break;
                        default:
                            console.warn("ApiArchiveGetAttachment: Use DEPRECATED WebKitBlobBuilder");
                            var bb = new window.WebKitBlobBuilder();
                            bb.append(arrayBuffer);
                            var blob = bb.getBlob(mimetype);

                            console.log("ApiArchiveGetAttachment: Done.");
                            options.success(blob, {
                                filename: filename,
                                mimetype: mimetype,
                                requestid: self.requestid
                            });
                            break;
                    }
                } else {
                    console.warn('ApiArchiveGetAttachment: no data for attachment from api for ' + JSON.stringify(searchParameter));
                    options.error({filename: searchParameter.name, errorcode: 3, requestid: self.requestid});
                }
            });
            request.fail(function (jqXHR, textStatus, errorThrown) {

                console.warn("ApiArchiveGetAttachment: POST fail textStatus is: " + textStatus + " jqXHR.status: " + jqXHR.status);
                if (textStatus === "abort") {
                    options.error({filename: searchParameter.name, errorcode: 4, requestid: self.requestid});
                } else if (window.hs_archive.isArchiveWeb && !window.hs_archive.sessiondata.preferences.isPrivateComputer) {
                    // reset session
                    window.hs_archive_modules.router.trigger('resetsession');
                } else if (self.counter > 0) {
                    console.warn('ApiArchiveGetAttachment: Time out error. RETRY ' + (6 - self.counter));
                    self.counter--;

                    var apiLoginService = new ApiLogin();
                    apiLoginService.checkStatusAndGetToken({
                        success: function (loginOptions) {
                            searchParameter.token = loginOptions.token;
                            console.log("ApiArchiveGetAttachment: renew token to " + searchParameter.token);
                            setTimeout(function () {
                                self.postRequest(searchParameter, options);
                            }, sd.delayForNext);
                        },
                        error: function () {
                            console.error("ApiArchiveGetAttachment: No token from server");
                            options.error(1);
                        }
                    });
                } else {
                    console.info('ApiArchiveGetAttachment: Error after 5 attempts for ' + JSON.stringify(searchParameter));
                    options.error({filename: searchParameter.name, errorcode: 3, requestid: self.requestid});
                }
            });
            return request;
        }
    });
});