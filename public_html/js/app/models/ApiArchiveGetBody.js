/*
 * Get Body
 * 
 */
define(function (require) {
    "use strict";
    var Backbone = require('backbone');
    var $ = require('jquery');
    var sd = require('app/models/StaticData');
    var ApiLogin = require('app/models/ApiLogin');

    var CryptoJS = require("CryptoJS");
    require("cryptojs_base64");

    return Backbone.Model.extend({
        counter: sd.timeoutRetries,
        urlRoot: "",
        urlPost: sd.apiBaseUrl + '/archive/GetBody',
        fetch: function (options) {
            console.log("api-uri: " + this.urlPost);

            var searchParameter = {};
            searchParameter.id = options.mailid;
            searchParameter.year = options.year;
            // TODO ?
            searchParameter.token = window.hs_archive.sessiondata.token;
            searchParameter.response = "json";
            console.log('api-data: ' + JSON.stringify(searchParameter, null, 4));

            return this.postRequest(searchParameter, options);

        },
        postRequest: function (searchParameter, options) {
            var self = this;
            var request = $.ajax({
                type: "POST",
                url: self.urlPost,
                data: searchParameter,
                timeout: sd.defaultTimeout,
                error: function (jqXHR, textStatus, errorThrown) {
                    console.warn('ApiArchvieGetBody: canceled? error is ' + textStatus);
                }
            });
            request.done(function (data, textStatus) {
                console.log("ApiArchiveGetBody: Request done, POST status is " + textStatus);
                console.log('data.GetBodyResponse.result.data is:');

//                console.log(data.GetBodyResponse.result.data);
                if (data.GetBodyResponse.result.data) {
//                    var mailbody = CryptoJS.enc.Utf8.stringify(CryptoJS.enc.Base64.parse(data.GetBodyResponse.result.data));
                    var mailbody = decodeURIComponent(escape(atob(data.GetBodyResponse.result.data)));
                    // console.log("ApiArchiveGetBody: mailbody: " + mailbody);
                    options.success(mailbody);
                } else {
                    options.success("");
                }
            });
            request.fail(function (jqXHR, textStatus, errorThrown) {
                console.warn("ApiArchiveGetBody: POST fail textStatus is: " + textStatus + " jqXHR.status: " + jqXHR.status);
                if (textStatus === "abort") {
                    options.error(4);
                } else if (window.hs_archive.isArchiveWeb && !window.hs_archive.sessiondata.preferences.isPrivateComputer) {
                    // reset session
                    window.hs_archive_modules.router.trigger('resetsession');
                } else if (self.counter > 0) {
                    console.warn('ApiArchiveGetBody: Time out error. RETRY ' + (6 - self.counter));
                    self.counter--;
                    var apiLoginService = new ApiLogin();
                    apiLoginService.checkStatusAndGetToken({
                        success: function (loginOptions) {
                            searchParameter.token = loginOptions.token;
                            console.log("ApiArchiveGetBody: renew token to " + searchParameter.token);
                            setTimeout(function () {
                                self.postRequest(searchParameter, options);
                            }, sd.delayForNext);
                        },
                        error: function (errorCode) {
                            console.error("ApiArchiveGetBody: No token from server");

                            /* because of timing - if no token received from Server app will NOT recall 
                             * API again, instead it will view mail without body. 
                             * This could be changed in the future.
                             */
                            options.error(errorCode);
                        }
                    });
                } else {
                    console.info('ApiArchiveGetBody: Error after 5 attempts for ' + JSON.stringify(searchParameter));
                    options.error(1);
                }
            });
            return request;
        }
    });
});