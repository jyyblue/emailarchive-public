/*
 * GetYear
 * 
 * Model for options in year-select
 * 
 */
define(function (require) {

    "use strict";
    var Backbone = require('backbone');
    var $ = require('jquery');
    var _ = require('Underscore');
    var sd = require('app/models/StaticData');
    var ApiLogin = require('app/models/ApiLogin');

    var Model = Backbone.Model.extend({
        defaults: {
        }
    });
    return Backbone.Collection.extend({
        model: Model,
        counter: sd.timeoutRetries,
        urlPost: sd.apiBaseUrl + '/archive/GetYears',
        comparator: function (item) {
            var a = item.get("year");
            // sort numbers 
            return -a;
        },
        initialize: function () {
        },
        fetch: function (options) {
            console.log("api-uri: " + this.urlPost);

            var searchParameter = {};
            searchParameter.token = options.token;
            searchParameter.response = "json";

            console.log('api-data: ' + JSON.stringify(searchParameter, null, 4));

            this.postRequest(searchParameter, options);
        },
        postRequest: function (searchParameter, options) {
            var self = this;

            var request = $.ajax({
                url: self.urlPost,
                type: "POST",
                data: searchParameter,
                timeout: sd.defaultTimeout
            });
            request.done(function (data, textStatus) {
                console.log("ApiArchiveGetYears: Request done, POST status is " + textStatus);

                _.each(data.GetYearsResponse.result.Year, function (year) {
                    var model = new Model({year: year});
                    self.add(model);
                });
                options.success();
            });
            request.fail(function (jqXHR, textStatus, errorThrown) {
                console.warn("ApiArchiveGetYears: POST fail textStatus is: " + textStatus + " jqXHR.status: " + jqXHR.status);
                if (window.hs_archive.isArchiveWeb && !window.hs_archive.sessiondata.preferences.isPrivateComputer) {
                    // reset session
                    window.hs_archive_modules.router.trigger('resetsession');
                } else if (self.counter > 0) {
                    console.warn('ApiArchiveGetYears: Time out error. RETRY ' + (6 - self.counter));
                    self.counter--;
                    var apiLoginService = new ApiLogin();
                    apiLoginService.checkStatusAndGetToken({
                        success: function (loginOptions) {
                            searchParameter.token = loginOptions.token;
                            console.log("ApiArchiveGetYears: renew token to " + searchParameter.token);
                            setTimeout(function () {
                                self.postRequest(searchParameter, options);
                            }, sd.delayForNext);
                        },
                        error: function () {
                            console.error("ApiArchiveGetYears: No token from server");
                            options.error(1);

                        }
                    });
                } else {
                    console.info('ApiArchiveGetYears: Error after 5 attempts for ' + JSON.stringify(searchParameter));
                    options.error(1);
                }
            });
        }
    });
});