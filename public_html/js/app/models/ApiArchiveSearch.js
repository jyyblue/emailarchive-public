/*
 * ApiArchvieSearch 
 * 
 */
define(function (require) {
    "use strict";
    var Backbone = require('backbone');
    var $ = require('jquery');
    var _ = require('Underscore');
    var ApiLogin = require('app/models/ApiLogin');
    var sd = require('app/models/StaticData');
    var Model = Backbone.Model.extend({
        idAttribute: "_id"
    });
    return Backbone.Collection.extend({
        model: Model,
        itemsOnPage: sd.itemsOnSearchPage,
        moreResultsInCurrentYear: false,
        isAnotherYear: true,
        availableYears: null,
        availableYearsIndex: 0,
        availableYearsCount: 0,
        currentFromYear: null,
        currentToYear: null,
        apiPageParameter: 1,
        currentUsedYear: null,
        counter: sd.timeoutRetries,
        urlPost: sd.apiBaseUrl + '/archive/Search',
        urlDemo: "data/archive/Search.json",
        originalSearchParameter: null, // ???
        initialize: function () {
        },
        setYears: function (years) {
            if (years) {
                this.availableYears = years;
                this.availableYearsCount = this.availableYears.length;
            } else {
                this.availableYears = null;
                this.availableYearsCount = 0;
            }
        },
        readFirstYear: function (options) {
            if (this.availableYears) {
                var firstYear = this.availableYears.at(this.availableYearsCount - 1).get('year');
                options.success(firstYear);
            } else {
                options.error();
            }
        },
        fetch: function (searchParameter, options) {
            console.log("api-uri: " + this.urlPost);

            if (searchParameter.page === 1) {
                this.currentFromYear = null;
                this.currentToYear = null;
                this.reset();
            }
            if (options.isDemoMode) {
                console.log("ApiArchvieSearch: Load demo mode");
                this.fetchDemoMode(searchParameter, options);
            } else {
                this.fetchArchive(searchParameter, options);
            }
        },
        fetchDemoMode: function (searchParameter, options) {
            this.counter = sd.timeoutRetries;
            var self = this;
            var count = 0;
            var request = $.ajax({
                url: self.urlDemo,
                type: "GET",
                dataType: "json",
                cache: false
            });
            request.done(function (data, textStatus) {
                console.log("ApiArchvieSearch: Request done, POST status is " + textStatus);
                _.each(data, function (mail) {
                    mail._id = "DEMO_" + mail['id'];
                    if (count < sd.demoModeItemsOnSearchPage && !self.get(mail._id)) {
                        self.add(mail);
                        count++;
                    }
                });
                options.success({
                    year: searchParameter.year,
                    moreresults: count === sd.demoModeItemsOnSearchPage
                });
            });
            request.fail(function (jqXHR, textStatus, errorThrown) {
                console.warn("ApiArchvieSearch: POST error status is: " + textStatus);
            });
        },
        fetchArchive: function (searchParameter, options) {
            this.counter = sd.timeoutRetries;
            console.log('ApiArchvieSearch: fromDate: ' + searchParameter.starttime);
            console.log('ApiArchvieSearch: toDate: ' + searchParameter.endtime);

            // this hack is compatibile with backend dateformat
            if (searchParameter.starttime) {
                this.currentFromYear = parseInt(searchParameter.starttime);
            }
            if (searchParameter.endtime) {
                this.currentToYear = parseInt(searchParameter.endtime);
            }

            console.log('ApiArchvieSearch: Year: ' + this.currentFromYear + " - " + this.currentToYear);
            console.log('ApiArchvieSearch: starttime: ' + searchParameter.starttime);
            console.log('ApiArchvieSearch: endtime: ' + searchParameter.endtime);

            // fix wrong timeframe
            if ((this.currentToYear && this.currentFromYear) && this.currentFromYear > this.currentToYear) {
                var tmp = this.currentToYear;
                this.currentToYear = this.currentFromYear;
                this.currentFromYear = tmp;
            }

            // check years
            if (searchParameter.page === 1) {
//                console.log('searchParameter.page === 1');
                this.isAnotherYear = true;

                // first page -> init year
                if (this.currentToYear || this.currentFromYear) {
                    if (!this.currentToYear) {
                        this.currentToYear = this.availableYears.at(0).get('year');
                    }
                    var self = this;
                    var found = false;
                    this.availableYears.each(function (model, index) {
                        //console.log(model.get("year"));
                        if (!found && model.get("year") <= self.currentToYear) {
                            found = true;
                            self.currentUsedYear = self.currentToYear;
                        }
                    }); // there are more years ?
                    this.isAnotherYear = (this.currentUsedYear !== this.currentFromYear);
                } else {
                    this.availableYearsIndex = 0;
                    this.apiPageParameter = 1;
                    this.currentUsedYear = this.availableYears.at(this.availableYearsIndex).get('year');
                }

            } else if (this.isAnotherYear && !this.moreResultsInCurrentYear) {
//                console.log('this.isAnotherYear && !this.moreResultsInCurrentYear');

                // change year in searchParameter
                if (this.currentFromYear) {
//                    console.log('this.currentFromYear');
                    // match next year is old year is complete from list
                    var self = this;
                    var found = false;
//
//                    console.log(this.availableYears);
                    var i = 0;
                    do {
//                        console.log(this.availableYears.length - i);
                        var year = this.availableYears.at(i).get("year");
                        if (year < this.currentUsedYear) {
                            found = true;
                            this.currentUsedYear = year;
                            this.apiPageParameter = 1;
                        }
                        i++;
                    } while (!found && i < this.availableYears.length)
                    this.isAnotherYear = (this.currentUsedYear > this.currentFromYear);
                } else {
                    this.availableYearsIndex++;
                    if (this.availableYearsIndex < this.availableYearsCount) {
                        this.currentUsedYear = this.availableYears.at(this.availableYearsIndex).get('year');
                    }
                    this.isAnotherYear = ((this.availableYearsIndex + 1) < this.availableYearsCount);
                }
                this.apiPageParameter = 1;
            } else {
                this.apiPageParameter++;
            }

            console.log(
                    'ApiArchvieSearch: from: ' + this.currentFromYear + ' current: ' + this.currentUsedYear + ' to: ' + this.currentToYear +
                    ' is anotherYear? ' + this.isAnotherYear);

            searchParameter.direction = sd.searchDirection;

            searchParameter.page = this.apiPageParameter;

            searchParameter.year = this.currentUsedYear;
            searchParameter.limit = this.itemsOnPage;
            searchParameter.token = options.token;
            searchParameter.response = "json";

            // to create a copy 
            // http://stackoverflow.com/questions/18359093/how-to-copy-javascript-object-to-new-variable-not-by-reference
            this.originalSearchParameter = JSON.parse(JSON.stringify(searchParameter));
//            console.log(JSON.stringify(this.originalSearchParameter, null, 2));
            // clear parameter not used for Api
            delete searchParameter.fromDate;
            delete searchParameter.fromDateMoment;
            delete searchParameter.toDate;
            delete searchParameter.toDateMoment;
            delete searchParameter.defaultValue;

            console.log('api-data: ' + JSON.stringify(searchParameter, null, 4));

            this.postRequest(searchParameter, options);
        },
        postRequest: function (searchParameter, options) {

            var count = 0;
            var self = this;
            var request = $.ajax({
                type: "POST",
                url: self.urlPost,
                data: searchParameter,
                timeout: sd.defaultTimeout
            });
            request.done(function (data, textStatus) {
                console.log("ApiArchvieSearch: POST status is " + textStatus);
//                console.log(data.SearchResponse.result);
                // BUG FIX backend
                if (data.SearchResponse.result.MailHeader instanceof Array) {
                    _.each(data.SearchResponse.result.MailHeader, function (mail) {
                        // TODO neue REST API
//                        mail['id'] = mail['id'].replace('=', '');

                        mail._id = self.currentUsedYear + "_" + mail['id'];
                        mail.year = self.currentUsedYear;
                        var model = new Model(mail);
                        self.add(model);
                        count++;
                    });
                } else {
                    // BUG FIX backend
                    var mail = data.SearchResponse.result.MailHeader;
                    if (mail) {
                        mail._id = self.currentUsedYear + "_" + mail['id'];
                        mail.year = self.currentUsedYear;
                        var model = new Model(mail);
                        self.add(model);
                        count++;
                    }
                }
                self.moreResultsInCurrentYear = count === self.itemsOnPage;

//                console.log('self.isAnotherYear:' + self.isAnotherYear + ' ' + self.currentUsedYear);

                if (self.isAnotherYear && count === 0) {
                    // seach next year
                    self.currentFromYear = null;
                    self.currentToYear = null;
                    self.originalSearchParameter.page = null;
//                    console.log(JSON.stringify(self.originalSearchParameter, null, 2));
                    self.fetchArchive(self.originalSearchParameter, options);
                } else {

                    options.success({
                        moreresults: self.moreResultsInCurrentYear || self.isAnotherYear,
                        currentUsedYear: self.currentUsedYear,
                        moreResultsInCurrentYear: self.moreResultsInCurrentYear ? self.currentUsedYear : null,
                        currentCount: count
                    });
                }
            });
            request.fail(function (jqXHR, textStatus, errorThrown) {

                console.warn("ApiArchvieSearch: POST fail textStatus is: " + textStatus + " jqXHR.status: " + jqXHR.status);
                if (window.hs_archive.isArchiveWeb && !window.hs_archive.sessiondata.preferences.isPrivateComputer) {
                    // reset session
                    options.resetSession();
                } else if (self.counter > 0) {
                    console.warn('ApiArchvieSearch: Time out error. RETRY ' + (6 - self.counter));
                    self.counter--;
                    // update token
                    var apiLoginService = new ApiLogin();
                    apiLoginService.checkStatusAndGetToken({
                        success: function (loginOptions) {
                            searchParameter.token = loginOptions.token;
                            console.log("ApiArchvieSearch: renew token to " + searchParameter.token);
                            setTimeout(function () {
                                self.postRequest(searchParameter, options);
                            }, sd.delayForNext);
                        },
                        error: function () {
                            console.error("ApiArchvieSearch: No token from server");
                            options.error(1);
                        }
                    });
                } else {
                    console.info('ApiArchvieSearch: Error after 5 attempts for ' + JSON.stringify(searchParameter));
                    options.error(1);
                }
            });
        }

    });
});