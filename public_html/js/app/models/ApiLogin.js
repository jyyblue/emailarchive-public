/*
 * TODO move credential management to a new module
 * 
 */
define(function (require) {

    "use strict";
    var Backbone = require('backbone');
    var $ = require('jquery');
    var _ = require('Underscore');
    var sd = require('app/models/StaticData');

    var CryptoJS = require("CryptoJS");
    // needed for CryptoJS
    require("cryptojs_base64");
    require("cryptojs_pbkdf2");
    require("cryptojs_aes");
    require("cryptojs_evpkdf");
    require("cryptojs_md5");
    require("cryptojs_cipher-core");
    require("cryptojs_sha1");
    require("cryptojs_hmac");

    return Backbone.Model.extend({
        keySize: 256,
        // TODO move to module Credentials
        localkey: 'ase_mobile_credentials_1_0',
        urlPost: sd.apiBaseUrl + '/Login',
        username: '',
        password: '',
        counter: sd.timeoutRetries,
        initialize: function (options) {
            this.options = options | {};
        },
        // TODO move to a new module Credentials
        writeCredentials: function (options) {
            var credentials = {};
            credentials.username = options.username;
            credentials.data = this.encrypt(options, credentials.username);
            if (window.hs_archive.isArchiveMobileDevice) {
                window.hs_archive.credentials.username = credentials.username;
                console.log("ApiLogin: Credentials to localstorage");
                localStorage.removeItem(this.localkey);
                localStorage.setItem(this.localkey, JSON.stringify(credentials));
            } else {
                console.log("ApiLogin: Credentials to memory");
                window.hs_archive.credentials = credentials;
            }

        },
        // TODO move to a new module Credentials
        readCredentials: function (options) {
            var credentialsJson = null;
            if (window.hs_archive.isArchiveMobileDevice) {
                console.log("ApiLogin: Credentials from localstorage");
                credentialsJson = localStorage.getItem(this.localkey);
                console.log("ApiLogin: Credentials HIDDEN");
//                 console.log(credentialsJson);
                if (credentialsJson) {
                    var credentialsEncrpt = JSON.parse(credentialsJson);
                    var credentials = {};
                    try {
                        credentials = this.decrypt(credentialsEncrpt.data, credentialsEncrpt.username);
                    } catch (e) {
                        console.log("catch decrypt Credentials");
                    }
                    options.success(credentials);
                } else {
                    var credentials = {};
                    credentials.username = '';
                    credentials.password = '';
                    options.success(credentials);
                }
            } else {
                console.log("ApiLogin: Credentials from memory");
                // console.log(window.hs_archive.credentials.data);
                // console.log(window.hs_archive.credentials.username);

                var credentials = this.decrypt(window.hs_archive.credentials.data, window.hs_archive.credentials.username);
                // console.log(credentials);
                options.success(credentials);
            }
        },
        checkStatusAndGetToken: function (options) {
            var self = this;
            this.readCredentials({
                success: function (credentials) {
                    if (!_.isEmpty(credentials.username) && !_.isEmpty(credentials.username)) {
                        self.fetch({
                            username: credentials.username,
                            password: credentials.password,
                            success: function (loginOptions) {
                                console.log("ApiLogin: Success - Token from Server");
                                options.success(loginOptions);
                            },
                            error: function (errorCode) {
                                console.warn("ApiLogin: NO token from Server");
                                options.error(errorCode);
                            }
                        });
                    } else {
                        console.warn("ApiLogin: No credentials from storage");
                        // check credentials error
                        options.error(0);
                    }
                }
            });
        },
        removeCredentials: function () {
            if (window.hs_archive.isArchiveMobileDevice) {
                localStorage.removeItem(this.localkey);
            } else {
                window.hs_archive.credentials = {};
            }
        },
        fetch: function (options) {

            console.log("api-uri: " + this.urlPost);
//            console.log(window.hs_archive.asemobile_footprint);
            var self = this;
            console.log("ApiLogin: Start AJAX call");
            // console.log("ApiLogin: footprint is " + JSON.stringify(window.hs_archive.asemobile_footprint));

            var request = $.ajax({
                url: this.urlPost,
                type: "POST",
                timeout: sd.loginTimeout,
                // crossDomain: true,
                // dataType: "json",
                data: {
                    "username": options.username,
                    "password": options.password,
                    "footprint": window.hs_archive.asemobile_footprint,
                    "response": 'json'
                }
            });

            //console.log('request');
//            console.log(request);

            request.done(function (data, textStatus) {
                if (data.LoginResponse.result.type === 1) {
                    console.log("ApiLogin: POST status is " + textStatus + " token: " + data.LoginResponse.result.token);
                    if (window.hs_archive_modules.router) {
                        window.hs_archive.sessiondata.token = data.LoginResponse.result.token;
                    }
                    self.writeCredentials({
                        username: options.username,
                        password: options.password,
                        token: data.LoginResponse.result.token
                    });
                    options.success({
                        token: data.LoginResponse.result.token,
                        type: data.LoginResponse.result.type
                    });
                } else {
                    // role/type not allowed
                    options.error(4);
                }
            });
            request.fail(function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401) {
                    console.warn('ApiLogin: Unauthorized. [401]');
                    options.error(0);
                } else if (textStatus === 'timeout') {
                    if (self.counter > 0) {
                        console.warn('ApiLogin: Time out error. RETRY ' + (6 - self.counter));
                        self.counter--;
                        setTimeout(function () {
                            self.fetch(options);
                        }, sd.delayForNext);
                    } else {
                        console.warn('ApiLogin: Time out error after 5 attempts.');
                        options.error(1);
                    }
                } else if (jqXHR.status === 0) {
                    console.warn('ApiLogin: No connection - verify network.');
                    options.error(2);
                } else if (jqXHR.status === 404) {
                    console.warn('ApiLogin: Requested page not found. [404]');
                    options.error(2);
                } else if (jqXHR.status === 500) {
                    console.warn('ApiLogin: Internal Server Error [500].');
                    options.error(1);
                } else if (textStatus === 'parsererror') {
                    console.warn('ApiLogin: Requested JSON parse failed.');
                    options.error(1);
                } else if (textStatus === 'abort') {
                    console.warn('ApiLogin: Ajax request aborted.');
                    options.error(1);
                } else {
                    console.warn('ApiLogin: Uncaught Error ' + jqXHR.responseText);
                    options.error(3);
                }
            });
        },
        // TODO move to Preferences.js
        encode: function (jsonObj) {
            var json = JSON.stringify(jsonObj);
            var dec1 = CryptoJS.enc.Utf8.parse(json);
            var dec2 = CryptoJS.enc.Base64.stringify(dec1);
            return dec2;
        },
        // TODO move to Preferences.js
        decode: function (base64String) {
            var dec2 = CryptoJS.enc.Base64.parse(base64String);
            var dec1 = CryptoJS.enc.Utf8.stringify(dec2);
            return JSON.parse(dec1);
        },
        // TODO move to a new module Credentials
        encrypt: function (jsonObj, passPhrase) {
            var json = JSON.stringify(jsonObj);
            var dec1 = CryptoJS.enc.Utf8.parse(json);
            var key = CryptoJS.PBKDF2(
                    CryptoJS.MD5(passPhrase).toString(),
                    CryptoJS.enc.Hex.parse('salt'), {
                keySize: this.keySize / 4
            });
            var encrypted = CryptoJS.AES.encrypt(dec1, key, {iv: key});
//            console.log(dec1);
//            console.log(key);
//            console.log(encrypted.ciphertext);

            var ciphertext = encrypted.ciphertext.toString(CryptoJS.enc.Base64);
//            console.log(ciphertext);
            return ciphertext;
        },
        // TODO move to a new module Credentials

        decrypt: function (cipherText, passPhrase) {
//            console.log('cipherText');
//            console.log(cipherText);
            var key = CryptoJS.PBKDF2(
                    CryptoJS.MD5(passPhrase).toString(),
                    CryptoJS.enc.Hex.parse('salt'), {
                keySize: this.keySize / 4
            });

            var cipherParams = CryptoJS.lib.CipherParams.create({
                ciphertext: CryptoJS.enc.Base64.parse(cipherText)
            });
            var decrypted = CryptoJS.AES.decrypt(
                    cipherParams,
                    key,
                    {iv: key});
//            console.log('decrypted');
//            console.log(decrypted);
            var dec1 = CryptoJS.enc.Utf8.stringify(decrypted);
//            console.log(dec1);
            return JSON.parse(dec1);
        }
    });


});