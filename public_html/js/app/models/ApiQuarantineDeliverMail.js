/*
 * Model for options in year-select
 * Rest-API: GetYear
 * 
 */
define(function (require) {

    "use strict";
    var Backbone = require('backbone');
    var $ = require('jquery');
    var _ = require('Underscore');
    var sd = require('app/models/StaticData');
    var ApiLogin = require('app/models/ApiLogin');

    var Model = Backbone.Model.extend({
        defaults: {
        }
    });
    return Backbone.Collection.extend({
        model: Model,
        counter: sd.timeoutRetries,
        urlPost: sd.apiBaseUrl + '/archive/QuarantineDeliverMail',
        initialize: function () {
        },
        fetch: function (options) {
            console.log("api-uri: " + this.urlPost);

            var searchParameter = {};
            searchParameter.id = options.mailid;
            searchParameter.year = options.year;
            searchParameter.token = window.hs_archive.sessiondata.token;
            searchParameter.response = "json";
            console.log('api-data: ' + JSON.stringify(searchParameter, null, 4));

            this.postRequest(searchParameter, options);
        },
        postRequest: function (searchParameter, options) {
            var self = this;

            var request = $.ajax({
                url: self.urlPost,
                type: "POST",
                data: searchParameter,
                timeout: sd.defaultTimeout
            });
            request.done(function (data, textStatus) {
                // console.log(JSON.stringify(data));
                console.log("ApiQuarantineDeliverMail: Request done, POST status is " + textStatus);
                options.success();
            });
            request.fail(function (jqXHR, textStatus, errorThrown) {
                console.warn("ApiQuarantineDeliverMail: POST fail textStatus is: " + textStatus + " jqXHR.status: " + jqXHR.status);
                if (window.hs_archive.isArchiveWeb && !window.hs_archive.sessiondata.preferences.isPrivateComputer) {
                    // reset session
                    window.hs_archive_modules.router.trigger('resetsession');
                } else if (self.counter > 0) {
                    console.warn('ApiQuarantineDeliverMail: Time out error. RETRY ' + (6 - self.counter));
                    self.counter--;
                    var apiLoginService = new ApiLogin();
                    apiLoginService.checkStatusAndGetToken({
                        success: function (loginOptions) {
                            searchParameter.token = loginOptions.token;
                            console.log("ApiQuarantineDeliverMail: renew token to " + searchParameter.token);
                            setTimeout(function () {
                                self.postRequest(searchParameter, options);
                            }, sd.delayForNext);
                        },
                        error: function () {
                            console.error("ApiQuarantineDeliverMail: No token from server");
                            options.error(1);
                        }
                    });
                } else {
                    console.info('ApiQuarantineDeliverMail: Time out error after 5 attempts for ' + JSON.stringify(searchParameter));
                    options.error(1);
                }
            });
        }
    });
});