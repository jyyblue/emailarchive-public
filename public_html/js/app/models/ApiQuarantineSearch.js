/*
 * 
 * Quarantaine development was stopped on 2015.0422
 * 
 * ApiQuarantineSearch 
 * 
 */
define(function (require) {
    "use strict";
    var Backbone = require('backbone');
    var $ = require('jquery');
    var _ = require('Underscore');
    var ApiLogin = require('app/models/ApiLogin');
    var sd = require('app/models/StaticData');
    var Model = Backbone.Model.extend({
        idAttribute: "_id"
    });
    return Backbone.Collection.extend({
        model: Model,
        itemsOnPage: sd.itemsOnSearchPage,
        moreResultsInCurrentYear: false,
        isAnotherYear: true,
        availableYears: null,
        availableYearsIndex: 0,
        availableYearsCount: 0,
        currentFromYear: null,
        currentToYear: null,
        apiPageParameter: 1,
        currentUsedYear: null,
        counter: sd.timeoutRetries,
        urlPost: sd.apiTestBaseUrl + '/mail/Search',
        initialize: function () {
        },
        readFirstYear: function (options) {
            console.error('ApiQuarantineSearch: Remove this call OR set default first search year');
            options.success(2000);
        },
        fetch: function (searchParameter, options) {
            console.log("api-uri: " + this.urlPost);
            if (searchParameter.page === 1) {
                this.currentFromYear = null;
                this.currentToYear = null;
                this.reset();
            }
            this.fetchQuarantine(searchParameter, options);
        },
        fetchQuarantine: function (searchParameter, options) {
            this.counter = sd.timeoutRetries;
            console.log('ApiQuarantineSearch: starttime: ' + searchParameter.starttime);
            console.log('ApiQuarantineSearch: endtime: ' + searchParameter.endtime);
            searchParameter.direction = sd.searchDirectionQuratine;
            searchParameter.page = this.apiPageParameter;
            searchParameter.limit = this.itemsOnPage;
            searchParameter.token = options.token;
            searchParameter.response = "json";
            this.originalSearchParameter = JSON.parse(JSON.stringify(searchParameter));
            // clear parameter not used for Api
            delete searchParameter.fromDate;
            delete searchParameter.fromDateMoment;
            delete searchParameter.toDate;
            delete searchParameter.toDateMoment;
            delete searchParameter.default;
            console.log('api-data: ' + JSON.stringify(searchParameter, null, 4));
            console.warn('TODO CHECK  & implement RestAPI.');
//            delete searchParameter.limit;
//            delete searchParameter.page;

            // required paramter
            searchParameter.endtime = '2014-01-03 10:09:01';
            searchParameter.starttime = '2015-01-03 01:09:01';
            searchParameter.filter = 1;
            searchParameter.direction = 2;

            this.postRequest(searchParameter, options);
        },
        postRequest: function (searchParameter, options) {
            var count = 0;
            var self = this;
            var request = $.ajax({
                type: "POST",
                url: self.urlPost,
                data: searchParameter,
                timeout: sd.defaultTimeout
//                beforeSend: function (jqXHR, settings) {
//                    console.log("Before send ... ");
//                    console.log(jqXHR);
//                    console.log(settings.data);
//                },
//                error: function (jqXHR, textStatus, errorThrown) {
//                    console.log("Error ... ");
//                    console.warn(jqXHR.getAllResponseHeaders());
//                    console.warn(jqXHR.getResponseHeader('Content-Type'));
//                },
//                success: function (data, textStatus, jqXHR) {
//                    console.log("Success ... ");
//                    console.warn(jqXHR);
//                    console.warn(jqXHR.getAllResponseHeaders());
//                    console.warn(textStatus);
//                    console.warn(data);
//                }
            });
            request.done(function (data, textStatus) {

                var MailHeader = data.SearchResponse.result.item;

//                console.log("ApiQuarantineSearch: MailHeader " + JSON.stringify(data.SearchResponse.result));
//                console.log(JSON.stringify(data.SearchResponse.result));
//                console.log(MailHeader);

                var convertItem = function (mail, count) {

                    // convert API data to client data

                    /* create my own id
                     * ID and NAME tokens must begin with a letter ([A-Za-z]) and may be followed 
                     * by any number of letters, digits ([0-9]), hyphens ("-"), underscores ("_"), 
                     * colons (":"), and periods (".").
                     * 
                     * jQuery has problems with ids that contain periods and colons
                     * 
                     * For example, if you limit yourself entirely to lower-case characters and always 
                     * separate words with either hyphens or underscores 
                     * (but not both, pick one and never use the other)
                     * 
                     * http://stackoverflow.com/questions/70579/what-are-valid-values-for-the-id-attribute-in-html
                     */
                    var idFromBackend = atob(mail.id);
                    var localId = idFromBackend.replace(/[^a-zA-Z0-9 -]/g, "_");
                    mail._id = localId;
//                        mail._id = mail.id;
//                        console.log(mail._id);

                    mail.From = mail.mail_from;
                    delete mail.mail_from;

                    mail.Subject = mail.mail_subject;
                    delete mail.mail_subject;

                    mail.Date = mail.mail_date;
                    delete mail.mail_date;

                    mail.To = mail.mail_to;
                    delete mail.mail_to;

                    mail.Size = mail.mail_size;
                    delete mail.mail_size;

                    mail.direction = mail.mail_direction === 0 ? 'IN' : 'OUT';
                    delete mail.mail_direction;

                    return mail;
                };

                // BUG FIX backend
                if (MailHeader instanceof Array) {
                    _.each(MailHeader, function (mail) {
                        var model = new Model(convertItem(mail, count));
                        self.add(model);
                        count++;
                    });
                } else {
                    // BUG FIX backend
                    var mail = MailHeader;
                    if (mail) {
                        var model = new Model(convertItem(mail, 0));
                        self.add(model);
                        count++;
                    }
                }

                self.moreResultsInCurrentYear = count === self.itemsOnPage;
//
//                if (self.isAnotherYear && count === 0) {
//                    // seach next year
//                    self.currentFromYear = null;
//                    self.currentToYear = null;
//                    self.originalSearchParameter.page = null;
//
//                    self.fetchArchive(self.originalSearchParameter, options);
//                } else {
//                    options.success({
//                        moreresults: self.moreResultsInCurrentYear || self.isAnotherYear,
//                        currentUsedYear: self.currentUsedYear,
//                        moreResultsInCurrentYear: self.moreResultsInCurrentYear ? self.currentUsedYear : null,
//                        currentCount: count
//                    });
//                }

                options.success({
                    moreresults: true,
                    currentUsedYear: '2000',
                    moreResultsInCurrentYear: false,
                    currentCount: 30
                });
            });
            request.fail(function (jqXHR, textStatus, errorThrown) {

//                console.log(jqXHR.getResponseHeader());

                console.log("TODO analyze fails");
                options.error(3);
                return;


                console.warn("ApiQuarantineSearch: POST fail textStatus is: " + textStatus + " jqXHR.status: " + jqXHR.status);
//                if (textStatus === 'timeout' || jqXHR.status === 401) {
                if (window.hs_archive.isArchiveWeb && !window.hs_archive.sessiondata.preferences.isPrivateComputer) {
                    // reset session
                    options.resetSession();
                } else if (self.counter > 0) {
                    console.warn('ApiQuarantineSearch: Time out error. RETRY ' + (6 - self.counter));
                    self.counter--;
                    // update token
                    var apiLoginService = new ApiLogin();
                    apiLoginService.checkStatusAndGetToken({
                        success: function (loginOptions) {
                            searchParameter.token = loginOptions.token;
                            console.log("ApiQuarantineSearch: renew token to " + searchParameter.token);
                            setTimeout(function () {
                                self.postRequest(searchParameter, options);
                            }, sd.delayForNext);
                        },
                        error: function () {
                            console.error("ApiQuarantineSearch: No token from server");
                        }
                    });
                } else {
                    console.info('ApiQuarantineSearch: Error after 5 attempts for ' + JSON.stringify(searchParameter));
                    options.error(1);
                }
//                } else {
//                    console.warn(
//                            "ApiQuarantineSearch: jqXHR.status: " + jqXHR.status + " textStatus: " + textStatus + " jqXHR.responseText: " + jqXHR.responseText);
//                    options.error(1);
//                }
            });
        }

    });
});