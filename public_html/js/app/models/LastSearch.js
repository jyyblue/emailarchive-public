/*
 * Local storage for last search
 * 
 */
define(function (require) {

    "use strict";
    var Backbone = require('backbone');
    return Backbone.Model.extend({
        localkey: 'ase_mobile_lastsearch_1_0',
        readLastSearchParameter: function (options) {
            var lastSearchParameter = {};
            
            // change this line to allow read last Search on privateComputer for WebClient
            if (window.hs_archive.isArchiveMobileDevice) {
                console.log("LastSearch: readLastSearchParameter");
                var lastSearchParameterJson = localStorage.getItem(this.localkey);
                if (lastSearchParameterJson) {
                    lastSearchParameter = JSON.parse(lastSearchParameterJson);
                }
            } else {
                lastSearchParameter = {};
                if (window.hs_archive.sessiondata.lastSearchParameter) {
                    lastSearchParameter = JSON.parse(window.hs_archive.sessiondata.lastSearchParameter);
                }
                console.log("LastSearch: Web read last search: " + JSON.stringify(lastSearchParameter));
            }
            options.callback(lastSearchParameter);
        },
        persistsLastSearchParameter: function (item) {
            
            // change this line to allow read last Search on privateComputer for WebClient
            if (window.hs_archive.isArchiveMobileDevice) {
                console.log("LastSearch: persistsLastSearchParameter");
                if (item) {
                    // persist last search 
                    localStorage.removeItem(this.localkey);
                    localStorage.setItem(this.localkey, JSON.stringify(item));
                } else {
                    // delete last search
                    localStorage.removeItem(this.localkey);
                    // localStorage.setItem(this.localkey, '{}');
                }
            } else {
                console.log("LastSearch: Web persits last search: " + JSON.stringify(item));
                window.hs_archive.sessiondata.lastSearchParameter = JSON.stringify(item);
            }
        }
    });
});