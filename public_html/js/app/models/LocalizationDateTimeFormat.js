/*
 * Date-time format for momentjs
 * 
 */

define(function () {
    "use strict";


    // moments
    var formats2digits = {
        "de": "DD.MM.YYYY HH:mm:ss",
        "en": "DD/MM/YYYY HH:mm:ss",
        "es": "DD/MM/YYYY HH:mm:ss"
    };

    function localeDateFormat() {
        var language = (navigator.language || navigator.userLanguage);
//        console.log("LocalizationDateTimeFormat: before: " + language);
        var separator = language.indexOf('-');
        if (separator !== -1) {
            language = language.substr(0, separator);
        }
//        console.log("LocalizationDateTimeFormat: after: " + language);
        return formats2digits[language] || formats2digits.en;
    }

    return localeDateFormat;

});