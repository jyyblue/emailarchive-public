/*
 * Local storage for preferences
 * 
 */
define(function (require) {

    "use strict";

    var Backbone = require('backbone');

    return Backbone.Model.extend({
        localkey: 'ase_mobile_preferences_1_0',
        read: function (options) {
            console.log("PreferencesStore: read");
            var prefencesJson = localStorage.getItem(this.localkey);
            if (prefencesJson) {
                var preferences = JSON.parse(prefencesJson);
                options.success(preferences);
            } else {
                // preferences does not exists -> default
                // persist default values and return
                options.success({});
            }
        },
        persists: function (preferences) {
            localStorage.removeItem(this.localkey);
            localStorage.setItem(this.localkey, JSON.stringify(preferences));
            console.log("PreferencesStore: preferences persited");
        },
        remove: function () {
            localStorage.removeItem(this.localkey);
            console.log("PreferencesStore: preferences removed");
        }
    });

});