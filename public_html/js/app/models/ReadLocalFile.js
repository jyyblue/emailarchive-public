/*
 * Read Data from url using ajax
 * used for demo mode attachments 
 * see jQuery.get(..)
 * 
 */
define(function (require) {

    "use strict";
    var Backbone = require('backbone');
    var $ = require('jquery');
    var _ = require('Underscore');
    var sd = require('app/models/StaticData');


    var model = Backbone.Model.extend({
    });

    return Backbone.Collection.extend({
        localkey: 'ase_mobile_favoritesearch',
        model: model,
        urlDemo: "data/attachments_base64/",
        initialize: function () {
            console.log('ReadLocalFile: Init');
        },
        fetch: function (options) {
            var file = this.urlDemo + options.filename;
            console.log('ReadLocalFile: Fetch');

            var request = $.ajax({
                type: "GET",
                url: file,
                timeout: sd.attachmentTimeout,
                dataType: 'text'
            });
            request.done(function (data, textStatus) {
                //console.log(data.length);
                var byteString = atob(data);
                var filename = options.filename;
                // console.log(filename.split('.').pop());
                var mimetype = sd.demoModeMimeTypes[filename.split('.').pop()];
                if (!_.isUndefined(mimetype)) {
                    if (typeof Blob !== 'undefined') {
                        var arrayBuffer = new ArrayBuffer(byteString.length);
                        var binArray = new Uint8Array(arrayBuffer);
                        for (var i = 0; i < byteString.length; i++) {
                            binArray[i] = byteString.charCodeAt(i);
                        }
                        var dataView = new DataView(arrayBuffer);
                        var blob = new Blob([dataView], {mimetype: mimetype});
                        options.success(blob, {
                            filename: filename,
                            mimetype: mimetype,
                            requestid: options.id
                        });
                    } else {
                        // use an older api
                        console.warn("ReadLocalFile: Use DEPRECATED WebKitBlobBuilder");
                        var bb = new window.WebKitBlobBuilder();
                        bb.append(arrayBuffer);
                        var blob = bb.getBlob(mimetype);

                        console.log("ReadLocalFile: Done.");
                        options.success(blob, {
                            filename: filename,
                            mimetype: mimetype,
                            requestid: options.requestid
                        });

                    }
                } else {
                    options.error({requestid: options.id, filename: options.filename, errorcode: 'Mimetype not exists'});
                }
            });
            request.fail(function (jqXHR, textStatus, errorThrown) {
                options.error({requestid: options.id, filename: options.filename, errorcode: textStatus});
            });
        },
    });
});