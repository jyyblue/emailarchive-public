/*
 * Constants
 * 
 */
define({
    apiBaseUrl: "https://control.hornetsecurity.com/api",
//    apiBaseUrl: "https://control.antispameurope.com/api",
    apiTestBaseUrl: "https://control.antispameurope.com/apitest",
    defaultDetailsContent: "",
    timeoutRetries: 5,
    delayForNext: 1000, //100
    loginTimeout: 3000, // 1000
    defaultTimeout: 5000, // 2000
    attachmentTimeout: 5000,
//    timeFormatString: 'mm/dd/yyyy'
    timeFormatStringMoments: 'DD.MM.YYYY',
    timeFormatStringDatePicker: 'dd.mm.yyyy',
    // see http://www.w3.org/TR/html-markup/input.date.html#input.date.attrs.value
    timeFormatHtmlInput: 'YYYY-MM-DD',
    timeFormatRestApi: 'YYYY-MM-DD HH:mm',
    timeFormatRestApiSearchResults: 'YYYY-MM-DD HH:mm:ss',
    inputTimeFormat:'ddd, DD MMM YYYY HH:mm:ss ZZ',
    logDateFormat: 'MMMM Do YYYY, HH:mm:ss',
    demoModeItemsOnSearchPage: 8,
    itemsOnSearchPage: 30,
    defaultLanguage: 'en',
    paddingTop: 60,
    sizeInPixelForHeaderAndFooter: 101,
    isMailBodyHtmlDefault: true,
    smartSearchSplitter: ':',
    logEmailAddress: 'support@hornetsecurity.com',
    searchDirection: 'both',
    searchDirectionQuratine: 2,
    archiveDefaultStartSearch: '{"page":1}',
    quarantineDefaultStartSearch: '{"direction":2,"page":1,"endtime":"2014-01-01 00:00:00","starttime":"2015-03-20 12:00:00","filter":1}',
    iosStatusbarBackground: '#4d4d4d',
    demoModeMimeTypes: {
        pdf: 'application/pdf',
        xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        pptx: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        docx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    },
});

