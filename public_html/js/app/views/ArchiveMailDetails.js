/*
 * renders mail details after a model is set
 * - manage event for openAttachment in mobeils's viewer (get attachment, if success and viewer registered view)
 * - manage event openMailinClient for mobile devices (get all attachments and open client)
 * - manage event for resendMail
 * 
 * - supports gesture for go back in TwoPageMode from details to list
 */
define(function (require) {

    "use strict";
    var $ = require('jquery');
    var Backbone = require('backbone');
    var _ = require('Underscore');
    require('hammerjs-jquery-backbone');

    var WaitForServerAction = require('app/views/generic/WaitForServerAction');

    var EmailService = require('app/views/cordova/Email');
    var FileService = require('app/views/web/File');
    var GetBody = require('app/models/ApiArchiveGetBody');
    var DeliverMail = require('app/models/ApiArchiveDeliverMail');
    var detailsAllTemplate = require('hbs!app/tpl/ArchiveMailDetails');
    var attachmentHeaderTemplate = require('hbs!app/tpl/AttachmentHeaderTemplate');
    var attachmentTemplate = require('hbs!app/tpl/AttachmentTemplate');
    var sd = require('app/models/StaticData');

    return Backbone.View.extend({
        isHtmlMailBody: sd.isMailBodyHtmlDefault,
        currentRequest: null,
        initialize: function (options) {
            this.options = options || {};
            _.bindAll(this, 'setModel', 'resendMail', 'eventBackToMaillist');
        },
        events: {
            "click #resendmail": "resendMail",
            "click #openmailclient": "openMailInClient",
            "click .attachment": "openAttachment"
        },
        hammerEvents: {
            'swiperight': 'eventBackToMaillist'
        },
        hammerOptions: {
        },
        eventBackToMaillist: function (event) {
            event.preventDefault();
            event.stopPropagation();
            var self = this;
            if (this.options.isMobileTwoPages) {
                $('#email-datails').fadeTo('fast', 0.33);
                self.options.renderList();
            }
        },
        setModel: function (model) {
//            console.log("Set model");
            //console.log(JSON.stringify(model.toJSON()));

            // FIX for REST-Backend  - convert Attachments-String to list
            var al = model.get("attachments");

            if (!_.isEmpty(al) && !_.isArray(al)) {
                // change to array - once als long as list is not reset
                console.log('ArchiveMailDetails: convert array');
                if (al.indexOf(',') === -1) {
                    var attachmentlist = [al];
                    model.set("attachments", attachmentlist);
                } else {
                    // split, 
                    // TODO ? should be an array in the future
                    var attachmentlist = al.split(',');
                    // simpe trim all names .. regex ?
                    var attachments = [];
                    for (var i = 0; i < attachmentlist.length; i++) {
                        var item = attachmentlist[i];
                        if (!_.isUndefined(item)) {
                            item = item.trim();
                            if (!_.isEmpty(item)) {
                                attachments.push(item);
                            }
                        }
                    }
                    model.set("attachments", attachments);
                }
            }

            if (window.hs_archive.sessiondata.preferences.isDemoMode) {
                this.model = model;
                this.render();
            } else if (!_.isUndefined(model.get('mailbody'))) {
                this.model = model;
                this.render();
            } else {
                // get body from Rest and store it in model for later use
                var self = this;
                var popup = new WaitForServerAction({
                    title: '__mail.loading_email',
                    actionTitle: '__base.cancel',
                    static: true,
                    action: function () {
                        console.warn("ArchiveMailDetails: canceled GetMailBody ");
                        self.currentRequest.abort();
                    }
                });

                var getBody = new GetBody();
                this.currentRequest = getBody.fetch({
                    year: model.get("year"),
                    mailid: model.get("id"),
                    success: function (mailbody) {
                        popup.hide();

                        model.set("mailbody", mailbody);
                        // simple check for html
                        var isHtmlMailBody = !(
                                mailbody.indexOf("<html>") === -1 &&
                                mailbody.indexOf("<body") === -1 &&
                                mailbody.indexOf("</td>") === -1);
//                        console.log("MailDetails: isHtmlMailBody is " + isHtmlMailBody);
                        model.set("isHtmlMailBody", isHtmlMailBody);

                        self.model = model;
                        self.render();
                    }, error: function (errorCode) {
                        console.log("MailDetails: No mailbody returned. errorCode " + errorCode);

                        popup.hide();

                        // create a copy to avoid conflicts during highlighting
                        self.model = model;

                        self.render();
                    }
                });

            }
        },
        openAttachment: function (event) {
            // no action for this link
            event.preventDefault();
            event.stopPropagation();
            // event.stopImmediatePropagation();

            if (window.hs_archive.isArchiveMobileDeviceDev) {
//            if (window.hs_archive.isArchiveMobileDevice && false) { // use temporaliy for development 
                alert("DEVINFO: MailDetails No mobile Device 1");
            } else
            if (window.hs_archive.isArchiveMobileDevice) {
                var self = this;
                var popup = new WaitForServerAction({
                    title: '__mail.loading_attachment',
                    actionTitle: '__base.cancel',
                    action: function () {
                        console.log("MailDetails: Attachment download canceled.");
                        self.currentRequest.abort();
                    }
                });
                this.currentRequest = new FileService({
                    attachmentlist: [event.target.dataset.attachment],
                    isDemoMode: window.hs_archive.sessiondata.preferences.isDemoMode,
                    year_id: this.model.get("year"),
                    email_id: this.model.get("id"),
                    success: function (pathToFiles, options) {
                        popup.hide();
                        if (pathToFiles.length > 0) {
                            var localFileMeta = pathToFiles[0];
                            // open Viewer
                            console.log(
                                    "MailDetails: open external viewer mimetype: " + localFileMeta.mimetype);
                            console.log(
                                    "MailDetails: path: " + localFileMeta.path);
                            console.log("MailDetails: MailDetails done.");
                            cordova.plugins.fileOpener2.open(
                                    localFileMeta.path,
                                    localFileMeta.mimetype, {
                                        success: function () {
                                            console.log('MailDetails: File opened successfully.');
                                        },
                                        error: function (errorObj) {
                                            window.hs_archive_modules.router.addModalView({
                                                title: '__base.info',
                                                description: '__mail.attachment_no_open',
                                                action: '__base.close'
                                            });
                                            console.warn('MailDetails: Error status: ' + errorObj.status
                                                    + ' - message: ' + errorObj.message);
                                        }
                                    }
                            );
                        } else if (!options.canceled) {
                            window.hs_archive_modules.router.addModalView({
                                title: '__base.info',
                                description: '__mail.attachment_notfound_msg',
                                action: '__base.close'
                            });
                            console.log('MailDetails: attachment list empty.');
                        }
                    },
                    error: function () {
                        console.error('MailDetails: TODO MSG? Error fileservice');
                        window.hs_archive_modules.router.addModalView({hide: 'true'});
                    }

                });

            } else {

                if (window.hs_archive.sessiondata.preferences.isPrivateComputer) {
                    var self = this;
                    var popup = new WaitForServerAction({
                        title: '__mail.loading_attachment',
                        actionTitle: '__base.cancel',
                        action: function () {
                            console.log("MailDetails: Attachment download canceled.");
                            self.currentRequest.abort();
                        }
                    });
                    this.currentRequest = new FileService({
                        attachmentlist: [event.target.dataset.attachment],
                        isDemoMode: window.hs_archive.sessiondata.preferences.isDemoMode,
                        year_id: this.model.get("year"),
                        email_id: this.model.get("id"),
                        loadFromRestOfferSave: !window.hs_archive.sessiondata.preferences.isDemoMode,
                        success: function () {
                            console.log('MailDetails: WEB Success fileservice');
                            popup.hide();
                        },
                        error: function () {
                            console.warn('MailDetails: No file from fileservice');
                            popup.hide();
                        }
                    });
                }
            }
        },
        openMailInClient: function (event) {
            // console.log('openMailInClient');
            event.preventDefault();
            event.stopPropagation();
            // console.log(this.model.get('attachmentlist'));
            // console.log(this.model.id);
            // download all attachments
            if (window.hs_archive.isArchiveWeb || window.hs_archive.isArchiveMobileDeviceDev) {
                alert("DEVINFO: No mobile Device");
            } else {
                var self = this;
                var osAndVersion = window.hs_archive_modules.router.checkOperatingSystem();
                // Q&D Hacks ---start---
                var mailBody = self.model.get('mailbody');

                if (!_.isUndefined(mailBody)) {
                    // mailbody was received from backend
                    switch (osAndVersion) {
                        case 'ios_gte_7':
                        case 'ios_lt_7':
                        case 'web':
                            // do nothing
                            break;
                        default:
                            // for any other
                            // TODO Clear issue: Dirty Hack: remove all html comments and style infos
                            mailBody = mailBody.replace(/<style[\s\S]*?style>/g, "").replace(/<!--[\s\S]*?-->/g, "");
                            // console.log(mailBody);
                            break
                    }
                }
                // Q&D Hacks ---end---

                if (self.model.get('attachments') && !_.isEmpty(self.model.get('attachments'))) {
                    var popup = new WaitForServerAction({
                        title: '__mail.loading_attachments',
                        actionTitle: '__base.cancel',
                        action: function () {
                            self.currentRequest.abort();
                        }
                    });
                    this.currentRequest = new FileService({
                        attachmentlist: self.model.get('attachments'),
                        isDemoMode: window.hs_archive.sessiondata.preferences.isDemoMode,
                        year_id: self.model.get("year"),
                        email_id: self.model.get("id"),
                        success: function (pathToFiles) {
                            var attachmentsPaths = [];
                            for (var i = 0; i < pathToFiles.length; i++) {
                                attachmentsPaths[i] = 'absolute://' + pathToFiles[i].path;
                            }
                            // open Mail Client
                            console.log("MailDetails: open mail client.");
                            console.info('MailDetails: Attachments: ' + JSON.stringify(attachmentsPaths));

                            var maildata = {
                                to: [self.model.get('To')],
                                subject: self.model.get('Subject'),
                                body: mailBody,
                                isHtml: self.model.get('isHtmlMailBody'),
                                attachments: attachmentsPaths
                            };
                            new EmailService({maildata: maildata});
                            popup.hide();
                        },
                        error: function () {
                            console.error('Router: TODO MSG? Error fileservice');
                            popup.hide();
                            alert('Testing: MSG-ID 4 Error fileservice');
                        }
                    });
                } else {
                    console.log("MailDetails: open mail client.");
                    var maildata = {
                        to: [self.model.get('To')],
                        subject: self.model.get('Subject'),
                        body: mailBody,
                        isHtml: self.model.get('isHtmlMailBody'),
                        attachments: []
                    };
                    // console.log(JSON.stringify(maildata));
                    new EmailService({maildata: maildata});
                }
            }
        },
        resendMail: function (event) {
            event.preventDefault();
            event.stopPropagation();
            var deliverMail = new DeliverMail();
            deliverMail.fetch({
                year: this.model.get("year"),
                mailid: this.model.get("id"),
                success: function (textStatus) {
                    window.hs_archive_modules.router.addModalView({
                        title: textStatus,
                        description: '__mail.mail_delivered',
                        action: '__base.close'
                    });
                }, error: function (errorCode) {
                    window.hs_archive_modules.router.addModalView({
                        title: '__base.error',
                        description: '__base.errorcode_' + errorCode,
                        action: '__base.close'
                    });
                }
            });
        },
        render: function () {

            // console.log('MailDetails: Current configuration cept in memory: ' + JSON.stringify(window.ase_archive_config));

//            console.log(window.hs_archive.sessiondata.search);
//            console.log(this.model.get('To_marked'));
//            console.log(this.model.get('From_marked'));

            if (!_.isUndefined(window.hs_archive.sessiondata.search.to) && _.isUndefined(this.model.get('To_marked'))) {
                var search = new RegExp(window.hs_archive.sessiondata.search.to, 'i');
                this.model.set('To_marked', this.model.get('To').replace(search, '<span class="highlighted">$&</span>'));
            }

//            console.log(!_.isUndefined(window.hs_archive.sessiondata.search.from) && _.isUndefined(this.model.get('From_marked')));

            if (!_.isUndefined(window.hs_archive.sessiondata.search.from) && _.isUndefined(this.model.get('From_marked'))) {
                var search = new RegExp(window.hs_archive.sessiondata.search.from, 'i');
                this.model.set('From_marked', this.model.get('From').replace(search, function (match) {
                    return '<span class="highlighted">' + match + '</span>';
                }));
            }

//            console.log('From: ' + this.model.get('From_marked'));
//            console.log('To: ' + this.model.get('To_marked'));
//            console.log('Subject: ' + this.model.get('Subject_marked'));


            this.$el.html(detailsAllTemplate({
//                isMobileTwoPages: this.options.isMobileTwoPages,
//                title: this.options.title,
                model: this.model.toJSON(),
                isResendAllowed: !window.hs_archive.sessiondata.preferences.isDemoMode,
                isLocalDevice: window.hs_archive.isArchiveMobileDevice
            }));

            // console.log("MailDetails: Attachments available? " + _.isObject(this.model.get('attachments')));
            if (this.model.get('attachments')) {
                var linkAndActions = window.hs_archive.isArchiveMobileDevice || window.hs_archive.sessiondata.preferences.isPrivateComputer;
                //  console.log("MailDetails: Link attachments? " + linkAndActions);
                var $attachment = $('#attachments');
                $attachment.append(attachmentHeaderTemplate());
                _.each(this.model.get('attachments'), function (item) {
                    // TODO supportedIcons ??? no mimetype here       
                    $attachment.append(attachmentTemplate({title: item, linked: linkAndActions, icon: 'default'}));
                });
            }

            if (this.model.get('mailbody')) {
                // add encoded mailbody to iframe
                var iframe = $('#mailbody')[0];
                var iframedoc = iframe.contentDocument || iframe.contentWindow.document;
                // details http://stackoverflow.com/questions/997986/write-elements-into-a-child-iframe-using-javascript-or-jquery
                iframedoc.open();
                iframedoc.close();

                console.log('ArchiveMailDetails: ' + this.model.get('isHtmlMailBody'));

                if (this.model.get('isHtmlMailBody')) {

//                    console.log('ArchiveMailDetails: window.hs_archive.sessiondata.preferences.showLinksInEmails '
//                            + window.hs_archive.sessiondata.preferences.showLinksInEmails);

                    // INFO could be undefined 
                    if (window.hs_archive.sessiondata.preferences.showLinksInEmails === true) {
                        iframedoc.body.innerHTML = this.model.get('mailbody');
                    } else {
                        iframedoc.body.innerHTML = this.removeExternalLinksToImages(this.model.get('mailbody'));
                    }
                    // sizing
                    if (iframedoc.body.scrollWidth < 950) {
                        iframe.style.width = (iframedoc.body.scrollWidth + 50) + 'px';
                    } else {
                        // TODO remove overflow & scrolling from HTML and add only here
                        iframe.style.width = '1000px';
                    }
                } else {
                    var preWidth = '1000px';
                    if (iframedoc.body.scrollWidth < 1000) {
                        preWidth = (iframedoc.body.scrollWidth) + 'px';
                    }
                    iframedoc.body.innerHTML = '<pre style="width:' + (preWidth - 10) + ';overflow-wrap:break-word;white-space: pre-wrap;white-space: -moz-pre-wrap;white-space: -pre-wrap;white-space: -o-pre-wrap;word-wrap: break-word;">' + this.model.get(
                            'mailbody') + "</pre>";

                    iframe.style.width = preWidth;
                }

                iframe.style.height = (iframedoc.body.scrollHeight + 80) + 'px';

                // filter all click events
                iframedoc.body.addEventListener("click", function (event) {
                    console.info('MailDetails: Linked content filtered');
                    event.preventDefault();
                    event.stopPropagation();
                    // TODO ? needed
                    event.stopImmediatePropagation();
                });
            }

            // TODO check if removeable
            // console.log(this.options.isMobileTwoPages);
            if (this.options.isMobileTwoPages) {
                window.scrollTo(0, 0);
            }

            return this;
        },
        removeExternalLinksToImages: function (text) {
            return text.replace(/(src=")(.*?)("\s?)/g, "$1$3");
        },
//        closeView: function () {
//            this.stopListening();
//            this.remove();
//        }
    });
});