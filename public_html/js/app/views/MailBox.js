/*
 * 
 * renders mailbox view for archive and quarantine
 * Quarantaine development was stopped on 2015.0422, functions was not removed
 * 
 * manages smart-search events and search-popup 
 * render results from collection using MailList
 * view details for email using detailsView provided as parameter from router
 * manage enhanced search using SearchForm
 * 
 * manage search result collection initialized in Router
 * 
 * 
 */

define(function (require) {

    "use strict";
    var $ = require('jquery');
    var Backbone = require('backbone');
    var _ = require('Underscore');

    var MailListView = require('app/views/MailList');

    var sd = require('app/models/StaticData');
    var SearchFormView = require('app/views/SearchForm');
    var WaitForServerAction = require('app/views/generic/WaitForServerAction');
    var mailboxFrame = require('hbs!app/tpl/Mailbox');
    var subTemplateNoemails = require('hbs!app/tpl/ArchiveNoEmails');
    var subTemplateMailboxOnePageWeb = require('hbs!app/tpl/MailboxInnerWeb');
    var subTemplateMailboxOnePage = require('hbs!app/tpl/MailboxInner');
    var subTemplatearchiveStartscreen = require('hbs!app/tpl/ArchiveStartScreen');
    var subTemplateQuarantineStartscreen = require('hbs!app/tpl/QuarantineStartScreen');

    return Backbone.View.extend({
        lastItemId: null,
        smartSearchFields: ['from', 'to', 'subject', 'attachment'],
        smartSearchFieldsAndBody: ['from', 'to', 'subject', 'attachment', 'body'],
        formerIds: [],
        isSimpleSearchVisible: false,
        formerId: null,
        formerItemIsNewItem: null,
        formerSelectedId: null,
        currentUsedYear: null,
        moreResultsInCurrentYear: null,
        moreresults: true,
        lastItemIdPreviousSearch: null,
        lastItemIdCurrentSearch: null,
        listPage: 1,
        $elSubView: null,
        $elListView: null,
        currentSearchParameter: {},
        searchResultCollection: null,
        detailsViewClass: '',
        currentView: '',
        searchFormView: null,
        detailsView: null,
        initialize: function (options) {

            this.options = options || {};
            this.mailListType = this.options.mailListType;
            this.searchResultCollection = this.options.apiCollection;
            this.detailsViewClass = this.options.detailsViewClass;

            _.bindAll(this, 'renderMailList', 'mailDetails', 'setSearchParameterFillcollectionAndView', 'fillMailCollectionAndView', 'loadnextpage');
        },
        events: {
            "keyup #simpleSearch": "eventKeyup",
            "keypress #simpleSearch": "eventKeypress",
            "click #clearSearch": "eventClearSearch",
            "click #startSearch": "eventStartSmartSearch",
            "click #from": "eventTriggerSingleParameterSearch",
            "click #to": "eventTriggerSingleParameterSearch",
            "click #subject": "eventTriggerSingleParameterSearch",
            "click #attachment": "eventTriggerSingleParameterSearch",
            "click #enhanced-search": "eventShowEnhancedSearch",
            "click #back-button": "eventBackButton"
        },
        showListCalledFromRouter: function (showHomeScreen) {
            console.log('MailBox > showListCalledFromRouter');
            if (showHomeScreen) {
                this.render().setSearchParameterFillcollectionAndView(JSON.parse(sd.archiveDefaultStartSearch));
            } else {
                this.render().renderMailList();
            }
        },
        eventBackButton: function (event) {
            window.hs_archive_modules.router.goBackInHistory();
        },
        eventShowEnhancedSearch: function (event) {
//            console.log('eventShowEnhancedSearch');
            event.preventDefault();

            this.clearSelectedSearchMenus();

            // transfer current input to enhanced search
            var smartSearchInputValue = $("#simpleSearch").val();

            var item = this.parseSmartSearch(smartSearchInputValue);
            window.hs_archive.sessiondata.search = JSON.parse(JSON.stringify(item));
            window.hs_archive_modules.router.currentView = 'searchForm';

            var self = this;
            // read first year from collection for default setting in SearchView
            this.searchResultCollection.readFirstYear({
                success: function (firstYear) {
                    self.renderSearchForm(firstYear);
                },
                error: function () {
                    self.renderSearchForm(null);
                }
            });
        },
        eventKeyup: function (event) {
//            console.log('>>>> event Keyup');
            var keyCode = null;
            if (event) {
                event.preventDefault();
                event.stopPropagation();
                keyCode = event.keyCode;
            }

            this.clearSelectedSearchMenus();
            // KEY: escape
            if (keyCode === 27) {
                this.eventClearSearch(null);
//                console.log("Shell: Reset search entries");
            } else {
                var smartSearchInputValue = $("#simpleSearch").val();
                var search = this.parseSmartSearchInputValue(smartSearchInputValue);
                for (var i = 0; i < this.smartSearchFields.length; i++) {
                    this.setSearchFieldValue(this.smartSearchFields[i], search[this.smartSearchFields[i]], search.defaultValue);
                }
                if (_.isEmpty(search) && (keyCode !== 13)) {
                    this.setMenuItemsState(false);
                } else if (!this.isSimpleSearchVisible) {
                    this.setMenuItemsState(true);
                }
            }
        },
        eventKeypress: function (event) {
            // monitor keys during input
            if (event.keyCode === 13) {
                this.hideSimpleSearch();
                $('#simpleSearch').blur();
                this.eventStartSmartSearch(event);
            }
        },
        eventTriggerSingleParameterSearch: function (event) {
            console.log('eventTriggerSingleParameterSearch');
            event.preventDefault();
            event.stopPropagation();
            // set during event keyUp
            var element = $(event.currentTarget);
            var searchValue = element[0].firstElementChild.dataset.search;
            if (_.isEmpty(searchValue)) {
                // no search pattern
                // TODO or change to if/else ?
                return;
            }

            var simpleSearchType = element.attr('id');
            this.clearSelectedSearchMenus();
            this.formerIds.push(simpleSearchType);
            $("#" + simpleSearchType).addClass('active');
            $('.dropdown').removeClass('open');
            var item = {};
            item.page = 1;
            item[simpleSearchType] = searchValue;
            window.hs_archive.sessiondata.search = JSON.parse(JSON.stringify(item));
            $("#simpleSearch").val(simpleSearchType + sd.smartSearchSplitter + searchValue);
            console.info("Shell: smart search parameter " + JSON.stringify(item, null, 4));
            this.setSearchParameterFillcollectionAndView(item);
        },
        eventStartSmartSearch: function (event) {
            console.log('eventStartSmartSearch');
            event.preventDefault();
            event.stopPropagation();

            // todo check this both action ?
            this.hideSimpleSearch();
            $('.dropdown').removeClass('open');


            var smartSearchInputValue = $("#simpleSearch").val();
//            console.log(smartSearchInputValue);
            var item = this.parseSmartSearch(smartSearchInputValue);
//            console.log(item);
            this.clearSelectedSearchMenus();
            // check userinput for menu selection
//            console.log(JSON.stringify(item, null, 2));
//            console.log(_.isUndefined(item.body));
            if (_.isUndefined(item.body) && !_.isEmpty(item)) {
                // set active menu - body is not in menu
                for (var i = 0; i < this.smartSearchFields.length; i++) {
                    var key = this.smartSearchFields[i];
                    console.log(item[key]);
                    if (!_.isUndefined(item[key])) {
                        $("#" + key).addClass('active');
                        this.formerIds.push(key);
                    }
                }
            }
            item.page = 1;

            // TODO parameters could be merge
            window.hs_archive.sessiondata.search = JSON.parse(JSON.stringify(item));

            this.setSearchParameterFillcollectionAndView(item);
        },
        eventClearSearch: function (event) {
            console.error('eventClearScreen');
            event ? event.preventDefault() : null;
            event ? event.stopPropagation() : null;
            for (var i = 0; i < this.smartSearchFields.length; i++) {
                this.setSearchFieldValue(this.smartSearchFields[i], '', null);
            }
            this.clearSelectedSearchMenus();
            $("#simpleSearch").val('');
            $("#simpleSearch").focus();
            this.setMenuItemsState(false);
            window.hs_archive.sessiondata.search = {};
        },
        parseSearchStringMultiple: function (s) {

            var stringArray = s.split(':');
            var item = {};
            var smartSearchFieldsLength = this.smartSearchFieldsAndBody.length;
            var lastKey = null;
            for (var j = 0; j < stringArray.length; j++) {
                var subStr = stringArray[j];
                // keys could exists in all items - but not in last one
                // find postion for a key - before a key MUST be an space to sparate it from a search pattern

                if (j === 0) {
                    // first one must be an key and space can be ignored
                    subStr = subStr.trim().toLowerCase();
                    var k = 0;
                    var key = null;
                    var keyIndex = -1;
                    do {
                        key = this.smartSearchFieldsAndBody[k];
                        k++;
                    } while (subStr !== key && k < smartSearchFieldsLength);
                    if (k < smartSearchFieldsLength) {
                        // a key was found
                        lastKey = key;
                        // console.info('key = ' + key);
                    } else {
                        lastKey = 'body';
                        // console.info('key = body (d)');
                    }
                    item[lastKey] = '';
                } else if ((j + 1) === stringArray.length) {
                    // last item must be a search pattern
                    // console.info('value = |' + subStr + "|");
                    if (subStr.trim().length > 0) {
                        // dont search for empty strings
                        item[lastKey] = subStr;
                        item.defaultValue = subStr;
                    } else {
                        delete item[lastKey];
                    }
                } else {
                    // substring 2..last-1
                    var keyIndex = -1;
                    var k = 0;
                    // console.info((j + 1) + ': ' + subStr);
                    do {
                        var key = this.smartSearchFieldsAndBody[k];
                        keyIndex = subStr.toLowerCase().indexOf(key);
                        // console.info(k + ' - ' + keyIndex)
                        k++;
                    } while (keyIndex === -1 && k < smartSearchFieldsLength);
                    if (subStr.trim().length === 0) {
                        // no value - no key
                        // dont search for empty strings
                        delete item[lastKey];
                        // console.info('key = body (d)');
                        lastKey = 'body';
                    } else if (keyIndex === -1) {
                        // no key found -> complete part is a pattern
                        // console.info((j + 1) + '. value = ' + subStr);
                        item[lastKey] = subStr;
                        item.defaultValue = subStr;
                        // console.info('key = body (d)');
                        lastKey = 'body';
                        item[lastKey] = '';
                    } else {
                        // check if only space after the key
                        // trim right
                        // console.info('before: |' + s2 + "|");
                        var s2trimmed = subStr.replace(/ +$/g, '').toLowerCase(); //$ marks the end of a string ~+$ means: all ~ characters at the end of a string
                        // console.log('after: |' + s2trimmed + "|");
                        // console.log(key);
                        // check endsWith and space is before key
                        if (s2trimmed.indexOf(' ' + key, s2trimmed.length - key.length - 1) !== -1) {
                            // key was found at the end of this substring 
                            // one space is used for split - dont use it as part of the searchpattern
                            var v = subStr.substring(0, keyIndex - 1);
                            // console.info('value = |' + v + '|');
                            if (v.trim().length > 0) {
                                // dont search for empty strings
                                item[lastKey] = v;
                                item.defaultValue = v;
                            } else {
                                delete item[lastKey];
                            }
                            // console.info('key = ' + key);
                            lastKey = key;
                        } else {
                            // no key was found - but a value
                            // console.info('value1 = |' + subStr + "|");
                            item[lastKey] = subStr;
                            item.defaultValue = subStr;
                            // console.info('key = body (d)');
                            lastKey = 'body';
                        }
                        item[lastKey] = '';
                    }
                }
                // console.info((j + 1) + JSON.stringify(item, null, 2));
            }
            return item;
        },
        parseSmartSearch: function (smartSearchInputValue) {
//            console.log('parseSmartSearch');
            var searchPattern = {};
            if (smartSearchInputValue.indexOf(sd.smartSearchSplitter) === -1) {
                // text only, search field not defined ->body
                if (!_.isEmpty(smartSearchInputValue)) {
                    searchPattern.body = smartSearchInputValue;
                }
            } else {
                /* contains a keyword f.e. subject:__search_string__
                 * Step 1: Split in key/value at ':'
                 */
                searchPattern = this.parseSearchStringMultiple(smartSearchInputValue);
            }
            return searchPattern;
        },
        clearSelectedSearchMenus: function () {
//            console.log('clearSelectedMenus');
            while (this.formerIds.length > 0) {
                var id = this.formerIds.pop();
                $("#" + id).removeClass('active');
            }
        },
        parseSmartSearchInputValue: function (smartSearchInputValue) {
            console.log('parseSmartSearchInputValue');
            var searchPattern = {};
            if (smartSearchInputValue) {
                if (smartSearchInputValue.indexOf(sd.smartSearchSplitter) === -1) {
                    // only String - no parsing
                    searchPattern.body = smartSearchInputValue;
                    searchPattern.defaultValue = smartSearchInputValue;
                } else {
                    searchPattern = this.parseSearchStringMultiple(smartSearchInputValue);
                }
            }
            return searchPattern;
        },
        addCurrentSearchParameterToMenu: function (searchParameter) {
//            console.log('addCurrentSearchParameterToMenu');
//            console.log("MailBox: add items to searchmenu " + JSON.stringify(searchParameter));
            var s = '';
            var bodySearch = null;
            if (searchParameter.body) {
                s += ' body:' + searchParameter.body;
                bodySearch = searchParameter.body;
            }

            for (var i = 0; i < this.smartSearchFields.length; i++) {
                var key = this.smartSearchFields[i];
                if (searchParameter[key]) {
                    this.formerIds.push(key);
                    $("#" + key).addClass('active');
                    s += ' ' + key + ':' + searchParameter[key];
                    this.setSearchFieldValue(key, searchParameter[key]);
                    if (_.isNull(bodySearch)) {
                        bodySearch = searchParameter[key];
                    }
                } else {
                    if (bodySearch) {
                        this.setSearchFieldValue(key, bodySearch);
                    } else {
                        this.setSearchFieldValue(key, '');
                    }
                }
            }

            // was at least menuentry added -> enable menu
            if (s.length > 0) {
                // at least one parameter was given -> set all emty values to this
                for (var i = 0; i < this.smartSearchFields.length; i++) {
                    var key = this.smartSearchFields[i];
                    if (!searchParameter[key]) {
                        this.setSearchFieldValue(key, bodySearch);
                    }
                }
                this.setMenuItemsState(true);
            } else {
                this.setMenuItemsState(false);
            }

            // TODO if $("#simpleSearch") was cleared and user clicked on "more results ... " at list menu entry is not selected
//            console.log(s);
            $("#simpleSearch").val(s.trim());
        },
        setSearchFieldValue: function (field, value, defaultValue) {
//            console.log('setSearchFieldValue');
            // console.log(field + ' ' + value + ' ' + defaultValue);
            if (!_.isNull(value) && !_.isUndefined(value)) {
                $('#' + field + 'value').text(value).html();
                $('#searchmenu-' + field).attr('data-search', value);
            } else if (!_.isNull(defaultValue)) {
                $('#' + field + 'value').text(defaultValue).html();
                $('#searchmenu-' + field).attr('data-search', defaultValue);
            }
        },
        setMenuItemsState: function (disabled) {
            // console.log("Shell: Will smartSearch now disabled? " + disabled);
            if (disabled) {
                for (var i = 0; i < this.smartSearchFields.length; i++) {
                    $('#' + this.smartSearchFields[i]).removeClass('disabled');
                }
            } else {
                for (var i = 0; i < this.smartSearchFields.length; i++) {
                    var key = this.smartSearchFields[i];
                    this.setSearchFieldValue(key, '');
                    $('#' + key).addClass('disabled');
                }
            }
        },
        hideSimpleSearch: function () {
            $('#searchgroup').removeClass('open');
            this.isSimpleSearchVisible = false;
        },
        // END event & helpers search menu
        mailList: function (additionalItemsLoaded, currentCount) {

            // print current configuration object
            // console.log(JSON.stringify(window.hs_archive, null, 4));

            console.log('Router: > mailList');
            if (this.searchResultCollection.length === 0) {
                this.renderMailList();
            } else {
//                console.log('Router: lastSelectedItemId: ' + this.selectedId);
//                console.log("Router: lastItemIdPreviousSearch:" + this.lastItemIdPreviousSearch);
//                console.log("Router: additionalItemsLoaded: " + additionalItemsLoaded);
                var self = this;

                this.scrollToItem = function (lastItemIdCurrentSearch) {
                    self.lastItemIdCurrentSearch = lastItemIdCurrentSearch;
                    if (self.selectedId && self.options.isMobileTwoPages) {
//                            console.log('Router: mailDetails - this.selectedId is ' + self.selectedId);
//                            console.log('Router: List offset/position ' + $('#tableview-item-list').offset().top + '/' + $('#tableview-item-list').position().top);
//                            console.log('Router: Item offset/position ' + $('#' + self.selectedId).offset().top + '/' + $('#' + self.selectedId).position().top);
//                            console.log('Router: window.innerHeight ' + window.innerHeight);
                        var itemTopPosition = $('#' + self.selectedId).offset();
                        $('body').animate({scrollTop: itemTopPosition.top - sd.paddingTop}, 'slow');
                    } else if (additionalItemsLoaded && self.lastItemIdPreviousSearch) {
                        var formerLastItem = $('#' + self.lastItemIdPreviousSearch);
                        var itemTopPosition = formerLastItem.offset();
                        if (self.options.isMobileTwoPages) {
                            $('body').animate({scrollTop: itemTopPosition.top - sd.paddingTop}, 'slow');
                        } else {
                            this.$elListView.animate({scrollTop: itemTopPosition.top - sd.paddingTop}, 'slow');
                        }
                    }
                };
//                console.log('Router: mailList - Page is ' + this.currentSearchParameter.page);
//                console.log('currentUsedYear ' + this.currentUsedYear);
//                console.log('moreResultsInCurrentYear ' + this.moreResultsInCurrentYear);
//                console.log('selectedId ' + this.selectedId);

                this.nextpage = this.moreresults ? this.listPage + 1 : null;
                this.formerLastItemId = this.lastItemIdPreviousSearch;
                this.currentCount = currentCount;

                this.renderMailList();


            }
        },
        scrollListToTop: function () {
            $('body').animate({scrollTop: 0}, 'fast', 'swing', function () {
                console.log('>>> scrolling finished');
            });
        },
        mailDetails: function (id, itemWasClicked) {
            console.log('MailBox > mailDetails');
            if (id !== null && _.isUndefined(this.searchResultCollection.get(id))) {
                window.hs_archive_modules.router.navigate('noPageInfo', {trigger: true});
            } else {
                if (this.options.isMobileTwoPages) {
                    window.hs_archive_modules.router.currentView = 'mailDetails';
                }
                $("#header").css('position', 'fixed');
                $("#footer").css('position', 'fixed');
                if (!itemWasClicked) {
                    // TODO check function scrollTop
                    this.$elListView.scrollTop(0);

                    var itemTopPosition = $('#' + id).offset();
                    this.$elListView.animate({scrollTop: itemTopPosition.top - this.$elListView.offset().top}, 'slow');
                }
                // do not change anything if new with AND former width lower than768px
                var keepView = this.lastViewWasTwoPages && this.options.isMobileTwoPages;
                this.lastViewWasTwoPages = this.options.isMobileTwoPages;
                console.info("Router: function maildetails (" + id + ")");
                console.log('Same id? ' + (this.selectedId === id));
                this.selectedId = id;
                if (this.searchResultCollection.length === 0) {
                    // should never happen ;-(
                    console.error('MailBox: this.searchResultCollection.length === 0');
                } else {
                    if (this.formerSelectedId) {
                        if (this.formerItemIsNewItem) {
                            $("#" + this.formerSelectedId).removeClass('new-email-item-selected');
                            $("#" + this.formerSelectedId).addClass('new-email-item');
                        } else {
                            $("#" + this.formerSelectedId).removeClass('email-item-selected');
                            $("#" + this.formerSelectedId).addClass('email-item');
                        }
                    }

                    this.formerSelectedId = id;
                    if ($("#" + id).hasClass('new-email-item')) {
                        this.formerItemIsNewItem = true;
                        $("#" + id).removeClass('new-email-item');
                        $("#" + id).addClass('new-email-item-selected');
                    } else {
                        this.formerItemIsNewItem = false;
                        $("#" + id).removeClass('email-item');
                        $("#" + id).addClass('email-item-selected');
                    }
                    var options = {keepView: keepView, id: id};

                    if (this.detailsView) {
                        // introduced because of Android 4.2 - check if needed
                        this.detailsView.undelegateEvents();
                    }
                    console.log("Router: Loading & Rendering MailDetails");
                    var model = this.searchResultCollection.get(options.id);

                    if (this.options.isMobileTwoPages) {
                        this.detailsView = new this.detailsViewClass({
                            isMobileTwoPages: true,
                            title: 'Email',
                            detailsall: true,
                            el: this.$elSubView,
                            renderList: this.renderMailList
                        });
                    } else {
                        var $elDetails = $("#tableview-item-details");
                        this.detailsView = new this.detailsViewClass({
                            isMobileTwoPages: false,
                            title: 'Email',
                            detailsall: true,
                            el: $elDetails
                        });
                    }
                    this.detailsView.setModel(model);
                }
            }
        },
        loadnextpage: function (apiPageNumber) {
            if (this.isStart || (window.hs_archive.isArchiveWeb
                    && _.isEmpty(window.hs_archive.credentials)
                    && !window.hs_archive.sessiondata.preferences.isDemoMode)) {
                // page reloaded
                this.apphome();
            } else {
                console.log("Router: loadnextpage");
                if (apiPageNumber) {
                    if (this.searchResultCollection.length === 0
                            || (window.hs_archive.sessiondata.token === null && !window.hs_archive.sessiondata.preferences.isDemoMode)) {
                        console.error('Testing: Empty collection, but page Number given? ot NO Token');
                    } else {
                        console.log('Router: page ' + apiPageNumber);
                        this.currentSearchParameter.page = parseInt(apiPageNumber, 10);
                        this.fillMailCollectionAndView({isNextListpage: true});
                    }
                } else {
                    console.error("Router: NO PARAMETER for paging");
                }
            }
        },
        clearMailCollection: function () {
            this.searchResultCollection.reset();
            this.resetMailCollectionIds();
        },
        resetMailCollectionIds: function () {
            this.selectedId = null;
            this.formerSelectedId = null;
            this.lastItemIdPreviousSearch = null;
            this.lastItemIdCurrentSearch = null;
        },
        setSearchParameterFillcollectionAndView: function (searchParameter) {
            this.currentSearchParameter = searchParameter;
            window.hs_archive.sessiondata.search = searchParameter;
            this.fillMailCollectionAndView({isNextListpage: false});
        },
        fillMailCollectionAndView: function (options) {

            // TODO remove this.currentSearchParameter & merge SearchForm & SmartSearch
            this.addCurrentSearchParameterToMenu(this.currentSearchParameter);

            var loadingSearchResultsFromServer = new WaitForServerAction({
                title: options.isNextListpage ? '__app.loading_next_page' : '__app.loading_search_results',
                static: true
            });

            if (!window.hs_archive.sessiondata.preferences.isDemoMode) {
                loadingSearchResultsFromServer.show();
            }

            console.log('MailBox: reset listPage: ' + !options.isNextListpage);
            if (options.isNextListpage) {
                this.listPage++;
            } else {
                this.listPage = 1;
            }

            console.log('MailBox: fillMailCollectionAndView - Page is ' + this.currentSearchParameter.page);
            var self = this;
            // load data from api to a backbone collection
            this.searchResultCollection.fetch(this.currentSearchParameter, {
                token: window.hs_archive.sessiondata.token,
                isDemoMode: window.hs_archive.sessiondata.preferences.isDemoMode,
                success: function (options) {

                    loadingSearchResultsFromServer.hide();

                    console.log('Router: Collection success');

                    self.moreresults = options.moreresults;
                    self.currentUsedYear = options.currentUsedYear;
                    self.moreResultsInCurrentYear = options.moreResultsInCurrentYear;

                    console.log('Router: self.currentSearchParameter.page: ' + self.currentSearchParameter.page);
                    console.log('Router: self.listPage: ' + self.listPage);

                    if (self.listPage > 1) {
                        console.log("Router: next page");
                        self.lastItemIdPreviousSearch = self.lastItemIdCurrentSearch;
                        self.selectedId = null;
                        self.formerSelectedId = null;
                        self.archiveMailDetailsView = null;
                        // call mailllistview
                        self.mailList(true, options.currentCount);
                    } else {
                        console.log("Router: reset result paging");
                        self.resetMailCollectionIds();
                        self.archiveMailDetailsView = null;
                        // call mailllistview and add route
                        self.mailList(false, options.currentCount);
                    }
                },
                error: function (errorcode) {

                    console.log('Router: Collection error');
                    loadingSearchResultsFromServer.hide();
                    // error msg 
                    window.hs_archive_modules.router.addModalView({
                        title: '__base.error',
                        description: '__base.errorcode_' + errorcode,
                        action: '__base.close'
                    });
                    console.warn("Router: mailCollection fetch error. No Data from Server");
                    self.renderMailList();
                },
                resetSession: function () {

                    console.log('Router: Collection resetSession');
                    console.warn("Router: mailCollection token refresh not allowed. Need credentials");
                    loadingSearchResultsFromServer.hide();
                    // event added in initialize
                    self.trigger('resetsession');
                }
            });
        },
        setViewMode: function (options) {
            console.log('MailBox > setViewMode - isMobileTwoPages is now ' + options.newModeIsTwoPages);
            this.options.isMobileTwoPages = options.newModeIsTwoPages;
        },
        setMailboxView: function (options) {
            if (this.options.isMobileTwoPages !== options.newModeIsTwoPages) {
                // set new viewMode for later use
                this.options.isMobileTwoPages = options.newModeIsTwoPages;
                // orentation change and new view mode
                if (options.newModeIsTwoPages) {
                    console.log('MailBox: now two pages');
                    // switch from one page to twopage
                    if (options.currentView === 'mailList' && this.selectedId === null) {
                        this.renderMailList();
                        this.resizeMailListAndScrollToSelectedItem(options.screenHeightInPixel);
                    } else {
                        this.mailDetails(this.selectedId);
                    }
                } else {
                    // switch from twopage to one page
                    console.log('MailBox: now single page');
                    this.renderMailList();
                    this.resizeView(options.screenHeightInPixel);
                }
            }
        },
        resizeView: function (height) {
            if (window.hs_archive.isArchiveMobileDevice) {
                $('.scrollable').height((height) + 'px');
                console.log('MailBox: resizeView -> scrollable height is ' + $('.scrollable').height());
            }
        }, // mail-detail-twopages
        resizeMailListAndScrollToSelectedItem: function (height) {
            if (window.hs_archive.isArchiveMobileDevice) {
                $('.scrollable').height((height) + 'px');
                console.log('MailBox: resizeMailListAndScrollToSelectedItem -> scrollable height is ' + $('.scrollable').height());
            }
            if (this.selectedId) {
                $('#tableview-item-list').scrollTop(0);
                var itemTopPosition = $('#' + this.selectedId).offset();
                $('#tableview-item-list').animate({scrollTop: itemTopPosition.top - $('#tableview-item-list').offset().top}, 'slow');
                $('.backglyph').hide();
            }
        }, // mail-detail-twopages
        resizeMailDetalsTo: function (height) {
            $('#mail-detail-twopages').height((height) + 'px');
            console.log('MailBox: scrollable height is ' + $('#mail-detail-twopages').height());
        },
        render: function () {
            this.$el.html(mailboxFrame({container: "container-fluid", isMobileTwoPages: this.options.isMobileTwoPages}));
            this.$elSubView = $('#mailbox-content');
            return this;
        },
        renderSearchForm: function (firstYear) {
            console.log('renderSearchForm');

            this.$elSubView.off();
            new SearchFormView({
                setSearchParameterFillcollectionAndView: this.setSearchParameterFillcollectionAndView,
                firstYear: firstYear,
                el: this.$elSubView
            });

        },
        renderMailList: function () {
            window.hs_archive_modules.router.currentView = 'mailList';

            if (this.searchResultCollection.length === 0) {
                this.$elSubView.html(subTemplateNoemails);
            } else {
                if (window.hs_archive.isArchiveMobileDevice) {
                    if (this.options.isMobileTwoPages) {
                        this.$elListView = this.$elSubView;
                    } else {
                        // TODO check append ??
                        this.$elSubView.html(subTemplateMailboxOnePage({
                            isMobileTwoPages: this.options.isMobileTwoPages,
                            container: "container-fluid",
                            col_xs_left: 'col-xs-12',
                            col_xs_right: null
                        }));
                        this.$elListView = $('#tableview-item-list');
                    }
                } else {
                    // TODO check append ??
                    this.$elSubView.html(subTemplateMailboxOnePageWeb({
                        // can be removed
                        isMobileTwoPages: this.options.isMobileTwoPages,
                        isWeb: window.hs_archive.isArchiveWeb,
                        container: "container-fluid",
                        col_xs_left: window.hs_archive.isArchiveMobileDevice ? 'col-xs-12' : 'col-xs-4',
                        col_xs_right: window.hs_archive.isArchiveMobileDevice ? null : 'col-xs-8'
                    }));
                    this.$elListView = $('#tableview-item-list');
                }

                // create list view
                var listView = new MailListView({
                    itemClickListener: this.mailDetails,
                    collection: this.searchResultCollection,
                    split: false,
                    nextpage: this.nextpage,
                    loadnextpage: this.loadnextpage,
                    currentUsedYear: this.currentUsedYear,
                    moreResultsInCurrentYear: this.moreResultsInCurrentYear,
                    selectedId: this.selectedId,
                    formerLastItemId: this.formerLastItemId,
                    currentCount: this.currentCount,
                    isMobileTwoPages: this.options.isMobileTwoPages
                });

                // add list
                var renderedView = listView.render();
                this.$elListView.html(renderedView.$el);

                // show start screen in details
                if (window.hs_archive.isQuarantineWeb) {
                    $('#tableview-item-details').html(subTemplateQuarantineStartscreen());
                } else {
                    $('#tableview-item-details').html(subTemplatearchiveStartscreen());
                }

                this.resizeView(window.hs_archive_modules.router.getScreenHeightInPixel());


                this.lastItemId = renderedView.lastItemId;
            }

            if (this.nextpage === null || this.nextpage > 1 && this.scrollToItem) {
                this.scrollToItem(this.lastItemId);
            }

            if (this.selectedId && !this.options.isMobileTwoPages) {
                // render details
                console.log('MailBox: render details after maillist');
                this.mailDetails(this.selectedId);
            }
            return this;

        }
    });
});
