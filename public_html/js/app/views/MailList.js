/*
 * MailList and RowView to render collection provided by ApiArchiveSearch
 * add new attributes to model contains html code for marker
 * creates footer for list
 * 
 */
define(function (require) {

    "use strict";

    var Backbone = require('backbone');
    var $ = require('jquery');
    var _ = require('Underscore');

    var rowTemplate = require('hbs!app/tpl/MailListItemClick');
    var rowHeaderTemplate = require('hbs!app/tpl/MailListItemHeader');
    var footerTemplate = require('hbs!app/tpl/MailListItemFooter');

    var moment = require('moment');
    require('moment_de');
    require('moment_es');

    var LocalizationDateTimeFormat = require('app/models/LocalizationDateTimeFormat');

    var sd = require('app/models/StaticData');

    var firstChar = "";

    var MailList = Backbone.View.extend({
        tagName: 'ul',
        lastItemId: null,
        newItem: false,
        deviceTimezone: null,
        dateTimeFormat: null,
        mostReasonYear: '',
        oldestYear: '',
        isFirstItem: true,
        className: 'mail-list',
        yearHistory: {},
        initialize: function (options) {
            this.options = options || {};
            _.bindAll(this, 'renderOne');
            this.options.sortAttributes = 'title';
            this.deviceTimezone = new Date().getTimezoneOffset();
            this.dateTimeFormat = LocalizationDateTimeFormat();
        },
        events: {
            "click #loadnextpage": "eventLoadNextPage"
        },
        setSortableAttributes: function (attributes) {
            this.options.sortAttributes = attributes;
        },
        eventLoadNextPage: function (event) {
            event.preventDefault();
            event.stopPropagation();
            this.options.loadnextpage(event.target.dataset.id);
        },
        sortArrayOfObjects: function (key, desc) {
            return function (a, b) {
                return desc ? ~~(a[key] < b[key]) : ~~(a[key] > b[key]);
            };
        },
        render: function () {
            // JavaScript has arrays (integer-indexed) and objects (string-indexed), 
            this.yearHistory = {};
            console.log("MailList: Render list");

            this.options.collection.each(this.renderOne);

            // JavaScript doesn't guarantee the order of keys in an object, using
            // a sorted array of objects for output instead
            var outputYearHistory = [];
            for (var key in this.yearHistory) {
                outputYearHistory.push({key: key, value: this.yearHistory[key]});
            }
            outputYearHistory.sort(this.sortArrayOfObjects('key', true));

            this.$el.append(footerTemplate({
                yearSring: this.mostReasonYear !== this.oldestYear ? this.oldestYear + '-' + this.mostReasonYear : this.mostReasonYear,
                yearHistory: outputYearHistory,
                count: this.collection.length,
                nextpage: this.options.nextpage,
                currentUsedYear: this.options.currentUsedYear,
                moreResultsInCurrentYear: this.options.moreResultsInCurrentYear,
                noEmailsInCurrentYear: (this.options.currentCount === 0)
            }));

            return this;
        },
        renderOne: function (model) {
            // check for first character
            var newFirstChar;
            if (this.options.split) {
                var tmpChar = model.get(this.options.sortAttributes.toString())[0].toUpperCase();
                if (tmpChar !== firstChar) {
                    firstChar = tmpChar;
                    newFirstChar = firstChar;
                }
            }
            if (newFirstChar) {
                this.$el.append(rowHeaderTemplate(newFirstChar));
            }
            var selected = model.id.toString() === this.options.selectedId;

            // convert HannoverTime (-120) to GMT and set device timezone
// 
            // *** START *** for moment.calender() - using relative dates
            var locale = (navigator.language) ? navigator.language : navigator.userLanguage;
            var separator = locale.indexOf('-');
            if (separator !== -1) {
                locale = locale.substr(0, separator);
            }
            if (locale !== 'en' && locale !== 'es' && locale !== 'de') {
                locale = 'en';
            }
            moment.locale(locale);
            model.set('DateLocal_list', moment(model.get("Date"), sd.inputTimeFormat).zone(-120).zone(this.deviceTimezone).format(sd.timeFormatRestApiSearchResults));
            // *** ELSE *** NOW AND - used in details
            model.set('DateLocal', moment(model.get("Date"), sd.inputTimeFormat).zone(-120).zone(this.deviceTimezone).format(sd.timeFormatRestApiSearchResults));
            // *** END ***


            // set dates for footer
//            console.log(moment(model.get('Date')).year());
            var d = moment(model.get('Date')).year();
            if (_.isUndefined(this.yearHistory[d])) {
                this.yearHistory[d] = 1;
            } else {
                this.yearHistory[d]++;
            }
//            console.log(this.yearHistory);

            if (this.isFirstItem) {
                this.mostReasonYear = d;
                this.isFirstItem = false;
            }
            this.oldestYear = d;
            var row = new RowView({
                model: model,
                itemClickListener: this.options.itemClickListener,
                selected: selected,
                newItem: this.newItem,
                isMobileTwoPages: this.options.isMobileTwoPages

            });
            if (model.id === this.options.formerLastItemId) {
                this.newItem = true;
            }
            this.lastItemId = model.id;
            this.$el.append(row.render(rowTemplate).$el);
            return this;
        }
    });

    var RowView = Backbone.View.extend({
        initialize: function (options) {
            this.options = options || {};
            _.bindAll(this, 'itemClickListener');
        },
        events: {
            "click": 'itemClickListener'
        },
        itemClickListener: function (event) {
//            console.log(this.model.get('_id'));
            this.options.itemClickListener(this.model.get('_id'), true);
        },
        render: function (currentRowTemplate) {
            var email = this.model.get('From');
            var incomingMail = true;
            if (this.model.get('direction') === 'OUT') {
                incomingMail = false;
                email = this.model.get('To');
            }
            email = email.length > 30 ? (email.substring(0, 28) + '..') : email;

//            console.log(window.hs_archive.sessiondata.search);

            // look for subject OR body
            if (!_.isEmpty(window.hs_archive.sessiondata.search)) {
                if (window.hs_archive.sessiondata.search.subject && _.isUndefined(this.model.get('Subject_marked'))) {
                    var search = new RegExp(window.hs_archive.sessiondata.search.subject, 'i');
                    this.model.set('Subject_marked', this.model.get('Subject').replace(search, '<span class="highlighted">$&</span>'));
                }
                // TODO ? body searchs also over subject ?
                if (window.hs_archive.sessiondata.search.body && _.isUndefined(this.model.get('Subject_marked'))) {
                    var search = new RegExp(window.hs_archive.sessiondata.search.body, 'i');
                    this.model.set('Subject_marked', this.model.get('Subject').replace(search, '<span class="highlighted">$&</span>'));
                }
                if (incomingMail && window.hs_archive.sessiondata.search.from) {
                    var search = new RegExp(window.hs_archive.sessiondata.search.from, 'i');
                    email = email.replace(search, '<span class="highlighted">$&</span>');
                } else if (!incomingMail && window.hs_archive.sessiondata.search.to) {
                    var search = new RegExp(window.hs_archive.sessiondata.search.to, 'i');
                    email = email.replace(search, '<span class="highlighted">$&</span>');
                }
            }

            if (this.options.newItem) {
                this.setElement($(currentRowTemplate({
                    email: email,
                    incomingMail: incomingMail,
                    model: this.model.toJSON(),
                    cssClassName: this.options.selected ? 'new-email-item-selected' : 'new-email-item',
                    arrowback: this.options.isMobileTwoPages ? 'arrowback' : null
                })));
            } else {
                this.setElement($(currentRowTemplate({
                    email: email,
                    incomingMail: incomingMail,
                    model: this.model.toJSON(),
                    cssClassName: this.options.selected ? 'email-item-selected' : 'email-item',
                    arrowback: this.options.isMobileTwoPages ? 'arrowback' : null
                })));
            }
            return this;
        }
    });

    return MailList;
});