/*
 * main view Preferences 
 * - login form 
 * - manage events for preferences and save preferences using PreferencesStore
 * - authenticate user and, if succes ful, navigate to apphome
 * - clear local credentials
 * - send eventlog
 * 
 */
define(function (require) {

    "use strict";

    var $ = require('jquery');
    var _ = require('Underscore');
    require('bootstrapSwitch');
    var Backbone = require('backbone');

    var template = require('hbs!app/tpl/Preferences');
    var templateWeb = require('hbs!app/tpl/PreferencesWeb');
    var WaitForServerAction = require('app/views/generic/WaitForServerAction');

    var ApiLogin = require('app/models/ApiLogin');
    var PreferencesStore = require('app/models/PreferencesStore');
    var EmailService = require('app/views/cordova/Email');
    var moment = require('moment');
    var FileService = require('app/views/web/File');

    var sd = require('app/models/StaticData');


    return Backbone.View.extend({
        loginService: null,
        preferencesStore: new PreferencesStore(),
        isDemoMode: true,
        isEnhancedLogging: false,
        events: {
            "click #sendlog": "eventSendlog",
            "click #authenticate": "authenticate",
            "click #clear": "clearUserCredentials",
            "click #logout": "logoutAndClear",
            
            "click #preferencs-button": "eventPrivateComputer",
            'switchChange.bootstrapSwitch #isdemomode': 'eventDemoMode',
            'switchChange.bootstrapSwitch #showlinksinemails': 'eventLoadImages',
            //'switchChange.bootstrapSwitch #isprivatecomputer': 'eventPrivateComputer',
            'switchChange.bootstrapSwitch #isenhancedlogging': 'eventEnhancedLogging',
            'submit form': 'submit'
        },
        initialize: function (options) {
            this.options = options || {};
            if (options.apiLoginService) {
                this.apiLoginService = options.apiLoginService;
            } else {
                this.apiLoginService = new ApiLogin();
            }
            this.render();
        },
        submit: function (event) {
            // noting - but reload page ... see Router.initialize
        },
        eventDemoMode: function (event) {
            event.preventDefault();
            event.stopPropagation();
            var isDemoMode = event.target.checked;

            $('#username_asemobile').prop('disabled', isDemoMode);
            $('#password_asemobile').prop('disabled', isDemoMode);
            $('#authenticate').prop('disabled', isDemoMode);
            window.hs_archive.sessiondata.preferences.isDemoMode = isDemoMode;
            if (isDemoMode) {
                window.hs_archive.credentials.username = null;
                window.hs_archive.credentials.data = null;
                window.hs_archive.sessiondata.token = null;
                window.hs_archive.sessiondata.years = null;
                $('#loggedinarea').remove();
            } else {
                // Demo mode was switched off
                window.hs_archive_modules.router.addModalView({
                    title: '__base.info',
                    description: '__login.demomodeleft_pleaselogin',
                    action: '__base.close'
                });

            }
            this.options.resetCollection();
            this.writePreferences();
        },
        eventLoadImages: function (event) {
            event.preventDefault();
            event.stopPropagation();
            var showLinksInEmails = event.target.checked;
            window.hs_archive.sessiondata.preferences.showLinksInEmails = showLinksInEmails;
            this.writePreferences();

        },
        eventPrivateComputer: function (event) {
            event.preventDefault();
            event.stopPropagation();
            var isPrivateComputer = event.target.checked;
            window.hs_archive.sessiondata.preferences.isPrivateComputer = isPrivateComputer;
            this.writePreferences();
        },
        eventEnhancedLogging: function (event) {
            event.preventDefault();
            event.stopPropagation();

            console.warn(window.hs_archive.logtext);

            var isEnhancedLogging = event.target.checked;
            window.hs_archive.isEnhanchedLogging = isEnhancedLogging;
            // enhaced loggin wil not be stored local
            // this.writePreferences();
        },
        writePreferences: function () {
            if (window.hs_archive.isArchiveMobileDevice || window.hs_archive.sessiondata.preferences.isPrivateComputer) {
                this.preferencesStore.persists(window.hs_archive.sessiondata.preferences);
            } else {
                this.preferencesStore.remove();
            }
        },
        authenticate: function (event) {
            event.preventDefault();
            event.stopPropagation();
            $('#authenticate').addClass('disabled');
            console.log("Preferences: authenticate");

            var username = $("#username_asemobile").val();
            var password = $("#password_asemobile").val();

            if (window.hs_archive.isArchiveMobileDevice) {

                // clear current data in memory, token, mailcollection, local files and 
                this.clearUserCredentials();
            }
            if (username && password) {
                var popup = new WaitForServerAction({
                    title: '__app.authorizing',
                    action: '__base.close',
                    static: true
                });

                var self = this;
                this.apiLoginService = new ApiLogin();
                this.apiLoginService.fetch({
                    username: username,
                    password: password,
                    success: function (options) {
                        console.log("Preferences: ApiLoginService success");

                        window.hs_archive.sessiondata.token = options.token;
                        popup.hide();

                        if (window.hs_archive.isArchiveMobileDevice) {
                            self.options.navigateToApphome();
                        } else {
                            // credentials are aleready written by fetch
                            if (window.hs_archive.sessiondata.preferences.isPrivateComputer) {

                                // simulate classic form-event to save credentials
                                var transferObject = {};
                                transferObject.token = options.token;
                                transferObject.username = username;
                                transferObject.password = password;
                                Session.set('transferObject', self.apiLoginService.encode(transferObject));

                                // activate store credentials in Browser and reload page
                                $('#loginForm').submit();
                            } else {
                                self.options.navigateToApphome();
                            }
                        }
                    },
                    error: function (errorcode) {
                        popup.hide();
                        console.warn("Preferences: ApiLoginService ERROR");
                        $('#authenticate').removeClass('disabled');
                        if (window.hs_archive.isArchiveMobileDevice) {
                            window.hs_archive.sessiondata.token = null;
                            self.options.navigateToApphome();

                            window.hs_archive_modules.router.addModalView({
                                title: '__base.error',
                                description: '__login.errorcode_' + errorcode,
                                action: '__base.close'
                            });
                        } else {
                            var transferObject = {};
                            transferObject.errorcode = errorcode;
                            Session.set('transferObject', self.apiLoginService.encode(transferObject));

                            $('#loginForm').submit();
                        }
                    }
                });
            } else {
                window.hs_archive_modules.router.addModalView({
                    title: '__base.error',
                    description: '__login.errorcode_0',
                    action: '__base.close'
                });
            }
        },
        logoutAndClear: function (event) {
            event.preventDefault();
            event.stopPropagation();
            this.initEnvironment();
            // activate store credentials in Browser and reload page
            $('#loginForm').submit();
        },
        initEnvironment: function () {
            window.hs_archive.sessiondata = {};
            window.hs_archive.sessiondata.preferences = {};
            window.hs_archive.sessiondata.search = {};
        },
        clearUserCredentials: function (event) {
            console.log("Preferences: clear credentials");

            var clearUserEnvironment = function (silent) {

                /*
                 * TODO ? should sourrounded by try/catch to ensure that all steps was succesfull
                 * if there was an error inform the user to restart the app
                 */

                // clear sessiondata
                self.initEnvironment();
                // remove from local storage
                self.apiLoginService.removeCredentials();
                // delete preferences after every new login
                self.preferencesStore.remove();

                // clear current data in memory, token, mailcollection
                window.hs_archive.sessiondata.token = null;

                window.hs_archive_modules.router.clearMailCollection();

                // INFO 2015.0317 disabled automatc switch to DemoMode after clear credentials
                // and save changed preferences

                // window.hs_archive.sessiondata.preferences.isDemoMode = true;
                // self.writePreferences();

                // clear tempory filesystem
                new FileService({deleteDirectory: 'root',
                    success: function (options) {
                        console.log("Preferences: clearUserCredentials "
                                + options.successCount + " Directories/Files deleted");
                        if (!silent) {
                            self.render();
                        }
                    },
                    error: function (options) {
                        console.log("Preferences: clearUserCredentials "
                                + options.successCount + " Directories/Files deleted, but " +
                                options.errorCount + " Error(s) reported.");
                        if (!silent) {
                            self.render();
                        }
                    }
                });
            };

            var silent = _.isUndefined(event);

            var self = this;

            if (silent) {
                clearUserEnvironment(silent);
            } else {
                event.preventDefault();
                event.stopPropagation();
                window.hs_archive_modules.router.addModalView({
                    title: '__preferences.remove_local_data',
                    yes: "__base.delete",
                    no: "__base.cancel",
                    description: '__preferences.remove_local_data_msg',
                    callback: clearUserEnvironment
                });
            }
        },
        eventSendlog: function () {
            if (window.hs_archive.isArchiveMobileDevice) {
                var text;
                text = window.hs_archive.logtext + "\n\n" + JSON.stringify(window.hs_archive.asemobile_footprint);
                console.log(text);
                new FileService({
                    filename: moment().format('YYYYMMDDHHmmss') + '.txt',
                    text: text,
                    success: function (pathToFiles) {
                        console.log("File written");
                        // reset log
                        window.hs_archive.logtext = 'Log started at ' + moment().format('MMMM Do YYYY, hh:mm:ss');
                        // send log
                        var attachmentsPaths = [];
                        for (var i = 0; i < pathToFiles.length; i++) {
                            attachmentsPaths[i] = 'absolute://' + pathToFiles[i].path;
                        }
                        var mailBody = window.navigator.appVersion + "\n\n" + navigator.userAgent;
                        var maildata = {
                            to: [sd.logEmailAddress],
                            subject: "LOG: " + window.hs_archive.asemobile_footprint.Os + " " + window.hs_archive.asemobile_footprint.OsVersion,
                            body: mailBody,
                            isHtml: false,
                            attachments: attachmentsPaths
                        };
                        new EmailService({
                            maildata: maildata
                        });

                    },
                    error: function () {
                        console.log("File NOT written");
                        alert('Testing: MSG-ID 5 - log wile not written.');
                    }
                });
            }
        },
        render: function () {
            console.log("Preferences: render");
            var self = this;
            if (window.hs_archive.isArchiveMobileDevice) {
                this.apiLoginService.readCredentials({
                    success: function (credentials) {
                        self.$el.html(template({
                            titleKey: '__product.a_name',
                            loggedin: !_.isNull(window.hs_archive.sessiondata.token),
                            username: credentials.username,
                            dummy: credentials.password,
                            mobiledevice: true,
                            build_id: window.hs_archive.build_id
                        }));
                        
                        $('#isenhancedlogging').bootstrapSwitch('state', window.hs_archive.isEnhanchedLogging, false);
                        
                        self.preferencesStore.read({
                            success: function (preferences) {
                                window.hs_archive.sessiondata.preferences = preferences;

                                $('#isdemomode').bootstrapSwitch('state', window.hs_archive.sessiondata.preferences.isDemoMode, false);
                                if (window.hs_archive.sessiondata.preferences.isDemoMode) {
                                    $('#username_asemobile').prop('disabled', true);
                                    $('#password_asemobile').prop('disabled', true);
                                    $('#authenticate').prop('disabled', true);
                                }
                                $('#isprivatecomputer').bootstrapSwitch('state', window.hs_archive.sessiondata.preferences.isPrivateComputer, false);
                                $('#showlinksinemails').bootstrapSwitch('state', window.hs_archive.sessiondata.preferences.showLinksInEmails, false);
                            }
                        });
                    }
                });
            } else {
                // use form for submit and save
                var username = "";
                if (!_.isEmpty(window.hs_archive.credentials)) {
                    username = window.hs_archive.credentials.username;
                }

                self.$el.html(templateWeb({
                    titleKey: window.hs_archive.isQuarantineWeb ? '__product.q_name' : '__product.a_name',
                    isArchive: !window.hs_archive.isQuarantineWeb,
                    isNotWebOnMobileDevice: !window.hs_archive_modules.router.isWebOnIOS(),
                    loggedinOrDemomode: username || window.hs_archive.sessiondata.preferences.isDemoMode,
                    loggedin: username || window.hs_archive.sessiondata.preferences.isDemoMode,
                    isdemomode: window.hs_archive.sessiondata.preferences.isDemoMode,
                    username: username,
                    dummy: '',
                    build_id: window.hs_archive.build_id
                }));
                $('#username_asemobile').focus();

                $('#isprivatecomputer').bootstrapSwitch('state', window.hs_archive.sessiondata.preferences.isPrivateComputer, false);
                if (!window.hs_archive.isQuarantineWeb) {
                    $('#isdemomode').bootstrapSwitch('state', window.hs_archive.sessiondata.preferences.isDemoMode, false);
                    $('#showlinksinemails').bootstrapSwitch('state', window.hs_archive.sessiondata.preferences.showLinksInEmails, false);
                }
                if (window.hs_archive.sessiondata.preferences.isDemoMode) {
                    $('#username_asemobile').prop('disabled', true);
                    $('#password_asemobile').prop('disabled', true);
                    $('#authenticate').prop('disabled', true);
                }
            }
            var language = (navigator.language) ? navigator.language : navigator.userLanguage;
            var separator = language.indexOf('-');
            if (separator !== -1) {
                language = language.substr(0, separator);
            }
            return this;
        }
    });

});