/*
 * Quarantaine development was stopped on 2015.0422
 * 
 * Copy from Archive - simple json output
 * 
 */

define(function (require) {

    "use strict";
    var $ = require('jquery');
    var Backbone = require('backbone');
    var _ = require('Underscore');
//    var GetBody = require('app/models/ApiArchiveGetBody');
//    var DeliverMail = require('app/models/ApiArchiveDeliverMail');
    var JsonOutput = require('hbs!app/tpl/dev/JsonOutput');
    var sd = require('app/models/StaticData');
    return Backbone.View.extend({
        isHtmlMailBody: sd.isMailBodyHtmlDefault,
        currentRequest: null,
        initialize: function (options) {
            this.options = options || {};
            _.bindAll(this, 'setModel', 'resendMail');
        },
        events: {
//            "click #resendmail": "resendMail",
//            "click #openmailclient": "openMailInClient",
//            "click .attachment": "openAttachment"
//            "click #mailbody a": 'nolinksallowed'
        },
//        nolinksallowed: function (event) {
//            console.warn('MailDetails: Linked content not allowed');
//            // after testing on android 4.2
//            event.preventDefault();
//            event.stopPropagation();
//            event.stopImmediatePropagation();
//        },
        setModel: function (model) {
//            console.log("Set model");
            //console.log(JSON.stringify(model.toJSON()));
            this.model = model;
            this.render();
            // TODO ? convert Attachments-String to list in Backend
//            var al = this.model.get("attachments");
//            if (al) {
//                if (!(al instanceof Array)) {
//                    if (al.indexOf(',') === -1) {
//                        var attachmentlist = [al];
//                        this.model.set("attachments", attachmentlist);
//                    } else {
//                        // split, 
//                        // TODO ? should be an array in the future
//                        var attachmentlist = al.split(',');
//                        // simpe trim all names .. regex ?
//                        for (var i = 0; i < attachmentlist.length; i++) {
//                            attachmentlist[i] = attachmentlist[i].trim();
//                        }
//                        this.model.set("attachments", attachmentlist);
//                    }
//                } else {
//                    // no action
//                }
//            }
//            if (window.hs_archive.sessiondata.preferences.isDemoMode) {
//                this.render();
//            } else {
////                this.$el.html(WaitTemplate({
////                    title: '__mail.loading_email'
////                }));
//                var self = this;
//                var modalViewLoadingMailbody = new WaitForResponseDialog({
//                    title: '__mail.loading_email',
//                    actionTitle: '__base.cancel',
//                    action: function () {
//                        self.currentRequest.abort();
//                    }
//                });
//                modalViewLoadingMailbody.show();
//
//                var mailDetail = new GetBody();
//                this.currentRequest = mailDetail.fetch({
//                    year: model.get("year"),
//                    mailid: model.get("id"),
//                    success: function (mailbody) {
//                        self.model.set("mailbody", mailbody);
//                        self.isHtmlMailBody = !(
//                                mailbody.indexOf("<html>") === -1 &&
//                                mailbody.indexOf("<body") === -1 &&
//                                mailbody.indexOf("</td>") === -1);
//                        console.info("MailDetails: isHtmlMailBody is " + self.isHtmlMailBody);
//                        modalViewLoadingMailbody.hide();
//                        self.render();
//                    }, error: function (errorCode) {
//                        console.log("MailDetails: No mailbody returned. errorCode " + errorCode);
//                        modalViewLoadingMailbody.hide();
//                        self.render();
//                    }
//                });
//            }
        },
//        resendMail: function (event) {
//            event.preventDefault();
//            event.stopPropagation();
//            var deliverMail = new DeliverMail();
//            deliverMail.fetch({
//                year: this.model.get("year"),
//                mailid: this.model.get("id"),
//                success: function () {
//                    window.hs_archive_modules.router.addModalView({
//                        title: '__base.success',
//                        description: '__mail.mail_delivered',
//                        action: '__base.close'
//                    });
//                    infoPopup.show();
//                    // waitTemplate.hide();
//                }, error: function (errorCode) {
//                    window.hs_archive_modules.router.addModalView({
//                        title: '__base.error',
//                        description: '__base.errorcode_' + errorCode,
//                        action: '__base.close'
//                    });
//                }
//            });
//        },
        renderAfterRotation: function (options) {
            this.options.isMobileTwoPages = options.isMobileTwoPages;
            this.$el = options.el;
            this.render();
        },
        render: function () {

            // console.log('MailDetails: Current configuration cept in memory: ' + JSON.stringify(window.ase_archive_config));

            this.$el.html(JsonOutput({
                data: JSON.stringify(this.model.toJSON(), null, 2)
            }));

            // TODO check: only for isTwoPageView
            window.scrollTo(0, 0);
//            if (this.model.get('mailbody')) {
//                // add encoded mailbody to iframe
//
//                var iframe = $('#mailbody')[0];
//                var iframedoc = iframe.contentDocument || iframe.contentWindow.document;
//                // details http://stackoverflow.com/questions/997986/write-elements-into-a-child-iframe-using-javascript-or-jquery
//                iframedoc.open();
//                iframedoc.close();
//
//                if (window.hs_archive.sessiondata.search.body) {
//                    var search = new RegExp(window.hs_archive.sessiondata.search.body, 'i');
//                    
//                    // INFO inside iframe no css from parent - direct modification
//                    this.model.set('mailbody', this.model.get('mailbody').replace(search, '<span class="highlighted">$&</span>'));
//                }
//
//                if (this.isHtmlMailBody) {
//                    //iframedoc.body.innerHTML = this.model.get('mailbody').replace(/http/gi, "xttp");
//                    if (window.hs_archive.sessiondata.preferences.showLinksInEmails) {
//                        iframedoc.body.innerHTML = this.model.get('mailbody');
//                    } else {
//                        iframedoc.body.innerHTML = this.removeExternalLinksToImages(this.model.get('mailbody'));
//                    }
//                } else {
//                    iframedoc.body.innerHTML = '<pre>' + this.model.get('mailbody') + "</pre>";
//                }
//
//                // filter all click events
//                iframedoc.body.addEventListener("click", function (event) {
//                    console.info('MailDetails: Linked content filtered');
//                    event.preventDefault();
//                    event.stopPropagation();
//                    event.stopImmediatePropagation();
//                });
//                iframe.style.height = (iframedoc.body.scrollHeight + 80) + 'px';
//                iframe.style.width = (iframedoc.body.scrollWidth + 80) + 'px';
//                // iframe.sandbox = 'allow-forms allow-scripts allow-same-origin';
//            }

            return this;
        },
        removeExternalLinksToImages: function (text) {
            return text.replace(/(src=")(.*?)("\s?)/g, "$1$3");
        },
        closeView: function () {
            this.stopListening();
            this.remove();
        }
    });
});