/*
 * renders enhanced search form
 * - existing parameters comes from SmartSearch  or, if null, from last search (loacalstorage)
 * - adjust min/manx date for search if user set date outside the period
 * - clear form
 * 
 */
define(function (require) {

    "use strict";
    var $ = require('jquery');
    var _ = require('Underscore');
    var Backbone = require('backbone');
    var moment = require('moment');
    var localizationDateFormat = require('app/models/LocalizationDateFormat');
    require('boostrap_daterangepicker');
    var sd = require('app/models/StaticData');
    var l10n = require('tools/hbshelpers/l10n');
    var formSearchTemplate = require('hbs!app/tpl/ArchiveSearchForm');
    var LastSearch = require('app/models/LastSearch');
    return Backbone.View.extend({
        lastsearchService: new LastSearch(),
        dateRange: null,
        lastSearchParameter: {},
        locale: null,
        currentFromDate: null,
        currentToDate: null,
        timeFormat: null,
        minDate: null,
        maxDate: null,
        useNativeDatePicker: false,
        initialize: function (options) {
            console.log('SearchForm: init');
            this.options = options || {};

            // set timeformat returns from native inputs or daterangepicker for converting a moment object
            var osAndVersion = window.hs_archive_modules.router.checkOperatingSystem();
            switch (osAndVersion) {
                case 'ios_gte_7':
                case 'ios_lt_7':
                case 'android_gte_5':
                    // default - works also if timeFormat is null - on at least ios6
                    this.useNativeDatePicker = true;
                    this.timeFormat = sd.timeFormatHtmlInput;
                    break;
                default:
                    // for datepicker
                    this.useNativeDatePicker = false;
                    this.timeFormat = localizationDateFormat().toUpperCase();
                    break
            }

            if (options.firstYear) {
                this.minDate = moment(new Date(options.firstYear, 0, 1));
            } else {
                console.warn('No first year in local store for this user. Server? ');
                this.minDate = moment(new Date());
            }
            this.maxDate = moment(new Date());

            // copy object
            this.lastSearchParameter = JSON.parse(JSON.stringify(window.hs_archive.sessiondata.search));

            var self = this;
            
            // TODO call to  this.lastsearchService.readLastSearchParameter can be moved in else 
            this.lastsearchService.readLastSearchParameter({callback: function (lastSearchParameter) {
                    if (_.isEmpty(self.lastSearchParameter)) {
                        for (var item in lastSearchParameter) {
                            if (!_.isUndefined(self.lastSearchParameter)) {
                                self.lastSearchParameter[item] = lastSearchParameter[item];
                            }
                        }
                    } else {
                        self.lastSearchParameter.starttime = lastSearchParameter.starttime;
                        self.lastSearchParameter.fromDate = lastSearchParameter.fromDate;
                        self.lastSearchParameter.fromDateMoment = lastSearchParameter.fromDateMoment;

                        self.lastSearchParameter.endtime = lastSearchParameter.endtime;
                        self.lastSearchParameter.toDate = lastSearchParameter.toDate;
                        self.lastSearchParameter.toDateMoment = lastSearchParameter.toDateMoment;
                    }
                    self.render();
                }
            });
        },
        events: {
            "click #go": "processForm",
            "click #back": "showMailList",
            "click #reset": "clearLastSearch",
            "click #autofill-from": "eventAutofillFrom",
            "click #autofill-to": "eventAutofillTo"

        },
        eventAutofillFrom: function () {
            $('#mailfrom').val(window.hs_archive.credentials.username);
        },
        eventAutofillTo: function () {
            $('#mailto').val(window.hs_archive.credentials.username);
        },
        processForm: function (event) {
            event.preventDefault();
            event.stopPropagation();
            var item = {};
            item.page = 1;
            if (this.currentFromDate) {
                
//                console.log("SearchForm: " + JSON.stringify(this.currentFromDate));
                item.fromDate = null;
                item.fromDateMoment = this.currentFromDate;
                item.starttime = new Date(this.currentFromDate).getTime()/1000;
                
            } else if ($('#fromDate').val()) {
               
//                console.log("$('#fromDate').val()");
//                console.log($('#fromDate').val());
//                console.log(this.timeFormat);
                item.fromDate = null;
                item.fromDateMoment = moment($('#fromDate').val(), this.timeFormat);
                item.starttime = moment($('#fromDate').val(), this.timeFormat).unix();
                //item.starttime = item.fromDateMoment.format(sd.timeFormatRestApi);
            } else {
               
                // default
                
                item.starttime = this.minDate.format(sd.timeFormatRestApi);
            }

            if (this.currentToDate) {
                item.toDate = null;
                item.toDateMoment = this.currentToDate;
                item.endtime = new Date(this.currentToDate).getTime()/1000;
            } else if ($('#toDate').val()) {
//                console.log("$('#toDate').val()");
//                console.log($('#toDate').val());
//                item.toDate = $('#toDate').val();
                item.toDate = null;
                item.toDateMoment = moment($('#toDate').val(), this.timeFormat);
                item.endtime = item.toDateMoment.add('d', 1).format(sd.timeFormatRestApi);
            } else {
                // default
                item.endtime = this.maxDate.add('d', 1).format(sd.timeFormatRestApi);
            }

            // other values
            if ($('#body').val()) {
                item.body = $('#body').val();
            }
            if ($('#mailto').val()) {
                item.to = $('#mailto').val();
            }
            if ($('#mailfrom').val()) {
                item.from = $('#mailfrom').val();
            }
            if ($('#mailsubject').val()) {
                item.subject = $('#mailsubject').val();
            }
            if ($('#mailattachment').val()) {
                item.attachment = $('#mailattachment').val();
            }

            // check date ranges
           
            if (item.fromDateMoment.isBefore(this.minDate)) {
                item.fromDateMoment = this.minDate;
                item.starttime = new Date(this.minDate).getTime()/1000;
            }
            if (item.toDateMoment.isAfter(this.maxDate)) {
                item.toDateMoment = this.maxDate;
                item.endtime = new Date(this.maxDate).getTime()/1000;
            }

            // INFO - from/toDateMoment and from/toDate will be not used anywhere else, 
            // but they contain a date-oject, and not a string for (?) later use

            // persist last search 
            this.lastsearchService.persistsLastSearchParameter(item);

            this.options.setSearchParameterFillcollectionAndView(item);
        },
        clearLastSearch: function (event) {
            event.preventDefault();
            event.stopPropagation();
            this.lastSearchParameter = {};
            this.lastsearchService.persistsLastSearchParameter();
            this.render();
        },
        showMailList: function (event) {
            event.preventDefault();
            event.stopPropagation();
            window.hs_archive_modules.router.navigate('home', {trigger: true});
        },
        render: function () {
//            console.log("SearchForm: render");

            // dirty hack
            if (!_.isObject(this.lastSearchParameter.fromDate)) {
                this.lastSearchParameter.fromDate = this.minDate;
            }
            if (!_.isObject(this.lastSearchParameter.toDate)) {
                this.lastSearchParameter.toDate = this.maxDate;
            }

//            console.warn(this.lastSearchParameter.fromDateMoment);
//            console.warn(this.minDate);
            var cFromDate = this.lastSearchParameter.fromDateMoment ? moment(this.lastSearchParameter.fromDateMoment) : this.minDate;
            var cToDate = this.lastSearchParameter.toDateMoment ? moment(this.lastSearchParameter.toDateMoment) : this.maxDate;

//            console.log(JSON.stringify(cFromDate));
//            console.log(JSON.stringify(cToDate));


            // dirty hack
            if (!_.isObject(cFromDate)) {
                cFromDate = this.minDate;
            }
            if (!_.isObject(cToDate)) {
                cToDate = this.maxDate;
            }


            var self = this;
            console.log("SearchForm: lastSearchParameter: " + JSON.stringify(self.lastSearchParameter.fromDateMoment));

            if (this.useNativeDatePicker) {
                // use native select
                console.log("SearchForm: fromDate: " + self.lastSearchParameter.fromDate);
                self.lastSearchParameter.fromDate = cFromDate.format(sd.timeFormatHtmlInput);
                console.log("SearchForm: fromDate: " + self.lastSearchParameter.fromDate);
                console.log("SearchForm: toDate: " + self.lastSearchParameter.toDate);
                self.lastSearchParameter.toDate = cToDate.format(sd.timeFormatHtmlInput);
                console.log("SearchForm: toDate: " + self.lastSearchParameter.toDate);

                self.$el.html(formSearchTemplate({
                    title: self.options.title,
                    model: self.lastSearchParameter,
                    daterangepicker: false
                }));
            } else {
                self.currentFromDate = cFromDate;
                self.currentToDate = cToDate;
                var dateString = self.currentFromDate.format(self.timeFormat) + ' - '
                        + self.currentToDate.format(self.timeFormat);

                console.log("SearchForm: dateString: " + dateString);

                self.$el.html(formSearchTemplate({
                    title: self.options.title,
                    model: self.lastSearchParameter,
                    daterangepicker: true,
                    searchrangevalue: dateString
                }));
                // Daterangepicker
                var ranges = {};
//                            ranges[l10n('__calendar.today')] = [moment(), moment()];
                ranges[l10n('__calendar.yesterday')] = [moment().subtract('days', 1), moment().subtract(
                            'days', 1)];
                ranges[l10n('__calendar.last_7_days')] = [moment().subtract('days', 6), moment()];
                ranges[l10n('__calendar.last_30_days')] = [moment().subtract('days', 29), moment()];
                ranges[l10n('__calendar.last_120_days')] = [moment().subtract('days', 119), moment()];
//                            ranges[l10n('__calendar.last_month')] = [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')];
                ranges[l10n('__calendar.last_year')] = [moment().subtract('year', 1).startOf('year'), moment().subtract('year', 1).endOf('year')];
                ranges[l10n('__calendar.full')] = [self.minDate, self.maxDate];

                $('#searchrange').daterangepicker({
                    startDate: self.currentFromDate,
                    endDate: self.currentToDate,
                    minDate: self.minDate,
                    maxDate: self.maxDate,
                    ranges: ranges,
                    format: self.timeFormat,
                    locale: {
                        applyLabel: l10n('__base.apply'),
                        cancelLabel: l10n('__base.close'),
                        fromLabel: l10n('__calendar.from'),
                        toLabel: l10n('__calendar.to'),
                        customRangeLabel: l10n('__calendar.custom'),
                        daysOfWeek: l10n('__calendar.daysOfWeek'),
                        monthNames: l10n('__calendar.monthNames'),
                        firstDay: 1
                    }
                }, function (start, end) {
                    self.currentFromDate = start;
                    self.currentToDate = end;
                });
            }
            return this;
        }
    });
});