/*
 * add html for views and footer
 * provide html element for views
 * manage footer action
 * switch view state visible/hidden using css
 * 
 */
define(function (require) {

    "use strict";
    var Backbone = require('backbone');
    var $ = require('jquery');
    var template = require('hbs!app/tpl/Shell');
    var l10n = require('tools/hbshelpers/l10n');
    return Backbone.View.extend({
        menuData: {},
        template: template,
        currentFooter: null,
        initialize: function (options) {
            console.log('Shell: init');
            // set html page title
            $(document).prop('title',
                    window.hs_archive.isQuarantineWeb ? l10n('__product.html_head_title_quarantine')
                    : l10n('__product.html_head_title_archive'));
            this.options = options || {};
        },
        events: {
            "click #content": "eventFixIOS",
            "click #mailbox": "eventFixIOS",
            "click #footermenu": "eventFooterMenu"
        },
        eventFixIOS: function () {
            // TODO check if needed in future
//            if (window.hs_archive_modules.router.ioskeyboard) {
//                $("#header").css('position', 'fixed');
            $("#footer").css('position', 'fixed');
//            }
        },
        eventFooterMenu: function (event) {
            event.preventDefault();
            event.stopPropagation();
            var targetId = event.target.id;

            console.log("Shell: footerMenu event " + targetId);

            if (targetId === 'mailbox-button' || targetId === 'mailbox-icon') {
                var elementVisible = $('#mailbox', this.$el);
                var elementHidden = $('#content', this.$el);
                elementVisible.removeClass('hidden');
                elementHidden.addClass('hidden');

                if (window.hs_archive.isQuarantineWeb) {
                    window.hs_archive_modules.router.navigate('quarantine', {trigger: true});
                } else {
                    window.hs_archive_modules.router.navigate('archive', {trigger: true});
                }
            } else if (targetId === 'preferences-button' || targetId === 'preferences-icon') {
                var elementVisible = $('#content', this.$el);
                var elementHidden = $('#mailbox', this.$el);
                elementVisible.removeClass('hidden');
                elementHidden.addClass('hidden');

                window.hs_archive_modules.router.navigate('preferences', {trigger: true});
            } else if (targetId === 'help-button' || targetId === 'help-icon') {
                var elementVisible = $('#content', this.$el);
                var elementHidden = $('#mailbox', this.$el);
                elementVisible.removeClass('hidden');
                elementHidden.addClass('hidden');

                window.hs_archive_modules.router.navigate("help", {trigger: true});
            }
        },
        enableFooterMenuButtonState: function (enable) {
            console.log('Shell enableFooterMenuButtonState is ' + enable);
            if (enable) {
                $('#mailbox-button').removeClass('hidden');
                $('#help-button').removeClass('hidden');
            } else {
                $('#mailbox-button').addClass('hidden');
                $('#help-button').addClass('hidden');
            }
        },
        setCurrentFooterButton: function (id) {

            console.log('Shell > setCurrentFooterButton to ' + id);

            if (this.currentFooter) {
                $('#' + this.currentFooter).removeClass('btn-primary');
                $('#' + this.currentFooter).addClass('btn-default');
                var elementHidden = $('#' + this.currentFooter.substr(0, this.currentFooter.indexOf('-')), this.$el);
                elementHidden.addClass('hidden');
            }
            // TODO - read jquery doku
            if (id.indexOf('icon') > -1) {
                this.currentFooter = id.replace('icon', 'button');
            } else {
                this.currentFooter = id + '-button';
            }

            $('#' + this.currentFooter).addClass('btn-primary');
            $('#' + this.currentFooter).removeClass('btn-default');

            var elementVisible = $('#' + this.currentFooter.substr(0, this.currentFooter.indexOf('-')), this.$el);
            elementVisible.removeClass('hidden');

        },
        getContentElement: function (elementId) {
            var elementVisible = $('#' + elementId, this.$el);

            if (elementId !== 'mailbox') {
                // The .off() method removes event handlers that were attached with .on(). 
                elementVisible.off();
            }
            return elementVisible;
        },
        render: function () {
            console.log("Shell: render ");
            this.$el.html(this.template({
                title: this.options.title,
                bbsearchwidth: "",
                showFooter: true,
                footerbuttonwidth: 4,
                isArchive: !window.hs_archive.isQuarantineWeb

            }));
            return this;
        }
    });
});

