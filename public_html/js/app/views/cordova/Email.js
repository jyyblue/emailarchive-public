/*
 * open email client in mobile device
 * 
 */
define(function (require) {

    "use strict";
    var Backbone = require('backbone');

    return Backbone.View.extend({
        details: false,
        initialize: function (options) {
            this.options = options || {};
            this.options.maildata = options.maildata;
            this.render();
        },
        render: function () {
            var self = this;
            var errorWindow = function () {
                window.hs_archive_modules.router.addModalView({
                    title: '__base.error',
                    description: '__mail.no_email_service',
                    action: '__base.close'
                });
            };
            if (window.plugin && window.plugin.email) {
                window.plugin.email.isServiceAvailable(
                        function (isAvailable) {
                            console.log('typeof self.options.maildata.body');
                            console.log(typeof self.options.maildata.body);
                            //console.log(isAvailable);
                            if (isAvailable) {
                                console.log("Email: will send email, is data available? " + _.isObject(
                                        self.options.maildata));
                                console.log("Email: self.options.maildata.isHtml is " + self.options.maildata.isHtml);
                                window.plugin.email.open({
                                    to: self.options.maildata.to,
                                    cc: self.options.maildata.cc, // contains all the email addresses for CC field
                                    bcc: self.options.maildata.bcc, // contains all the email addresses for BCC field
                                    attachments: self.options.maildata.attachments, // contains all full paths to the files you want to attach
                                    subject: self.options.maildata.subject, // represents the subject of the email
                                    body: self.options.maildata.body, // represents the email body (could be HTML code, in this case set isHtml to true)
                                    isHtml: self.options.maildata.isHtml // indicats if the body is HTML or plain text
                                });
                            } else {
                                errorWindow();
                            }
                        }
                );
            }
            else {
                errorWindow();
            }
            return this;
        }
    });
});