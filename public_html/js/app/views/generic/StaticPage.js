/*
 * Render a template provided in options
 * 
 */

define(function(require) {

    "use strict";

    var Backbone = require('backbone');

    return Backbone.View.extend({
        initialize: function(options) {
            this.options = options || {};
            this.render();
        },
        render: function() {
            console.log("Static: render");
            if (this.options.template) {
                if (this.options.msg) {
                    this.$el.html(this.options.template(this.options.msg));
                } else {
                    this.$el.html(this.options.template());                    
                }
            }
            return this;
        }
    });

});