/*
 * Add and remove a (static) bootstrap modal popup to screen
 * if options.actionTitle is provided user can cancel this action and an 
 * optional provided options.action will be called
 * 
 */
define(function (require) {

    "use strict";
    var Backbone = require('backbone');
    var template = require('hbs!app/tpl/WaitForServerAction');

    return Backbone.View.extend({
//        id: 'base-modal',
        className: 'modal fade',
        attributes: {
            role: 'dialog'
        },
        events: {
            'click #action': 'cancel',
            'click': 'noaction',
            'hidden.bs.modal': 'teardown'
        },
        initialize: function (options) {
            console.log("WaitForServerAction: init ");
            this.options = options || {};

            console.log('WaitForServerAction: static is ' + this.options.static);
            this.render();
        },
        noaction: function (event) {
            event.stopImmediatePropagation();
        },
        show: function () {
            this.$el.modal('show');
        },
        cancel: function () {
            if (this.options.action) {
                this.options.action();
            }
            this.$el.modal('hide');
        },
        hide: function () {
            this.$el.modal('hide');
        },
        teardown: function () {
            this.$el.data('modal', null);
            this.remove();
        },
        render: function () {
            console.log("WaitForServerAction: render");

            this.$el.html(template({
                title: this.options.title,
                description: this.options.description,
                actionTitle: this.options.actionTitle
            }));

            if (this.options.static || this.options.action) {
                this.$el.modal({show: false, backdrop: 'static', keyboard: false});
            } else {
                this.$el.modal({show: false});
            }

            this.show();

            return this;
        }
    });
});