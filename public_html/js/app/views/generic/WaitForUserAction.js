/*
 * Add and remove a (static) bootstrap modal popup to screen
 * and user must confirm the messeage
 * 
 */
define(function (require) {

    "use strict";
    var Backbone = require('backbone');
    var $ = require('jquery');
    var template = require('hbs!app/tpl/WaitForUserAction');

    return Backbone.View.extend({
        className: 'modal',
        attributes: {
            role: 'dialog'
        },
        events: {
            'hidden.bs.modal': 'teardown',
            'click #btnYes': 'action'
        },
        initialize: function (options) {
            console.log("WaitForUserAction: init");
            this.options = options || {};
            this.render();
        },
        show: function () {
            this.$el.modal('show');
        },
        teardown: function () {
            this.$el.data('modal', null);
            this.remove();
        },
        hide: function () {
            this.$el.modal('hide');
        },
        action: function () {
            this.options.callback();
        },
        render: function () {
            console.log("WaitForUserAction: Render message");
            this.$el.html(template({
                title: this.options.title,
                description: this.options.description,
                action: this.options.action,
                yes: this.options.yes,
                no: this.options.no
            }));

            if (this.options.static || this.options.callback) {
                this.$el.modal({show: false, backdrop: 'static', keyboard: false});
            } else {
                this.$el.modal({show: false});
            }
            return this;
        }
    });
});