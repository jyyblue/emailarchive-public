/*
 * File management
 * 
 * Load files from REST using ApiArchiveGetAttachment and save in local filesystem (mobile) or offer save (web)
 * Load files from URL using ReadLocalFile and save in local filesystem (mobile) or offer save (web)
 * write string in a file
 * delete all files from temp app directory
 */
define(function (require) {

    "use strict";

    var Backbone = require('backbone');
    var $ = require('jquery');
    var _ = require('Underscore');
    var MailAttachment = require('app/models/ApiArchiveGetAttachment');
    var ReadLocalFile = require('app/models/ReadLocalFile');
    require('filesaver');

    // do not call from QuarantaineApp !
    return Backbone.View.extend({
        counter: 0,
        currentRequests: [],
        fs: null,
        initialize: function (options) {
            this.options = options || {};
            console.log('File: ' + JSON.stringify(options));

            if (window.hs_archive.isArchiveWeb) {
                if (options.loadFromRestOfferSave) {
                    this.loadFromRestOfferSave();
                } else {
                    // Demo mode
                    console.log("File: download from url");
                    this.loadLocalOfferSave();
                }
            } else {
                _.bindAll(this, 'errorHandler');

                var self = this;

                // 2015.0513 - does not work in firefox for development - use chrome/webkit for development 
                window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
                window.requestFileSystem(window.TEMPORARY, 32 * 1024 * 1024, function (filesystem) {
                    self.fs = filesystem;
                    if (options.text) {
                        // used for enhanced Logging
                        self.writeFile();
                    } else if (options.isDemoMode) {
                        console.log("File: download from url");
                        self.loadLocal();
                    } else if (options.deleteDirectory) {
                        self.removeRecursively();
                    } else {
                        // load from antispam api
                        console.log("File: load base64 from rest");
                        self.loadFromRest();
                    }
                }, this.errorHandler);
            }
        },
        removeRecursivelyAsync: function () {
            var errorCount = 0;
            var successCount = 0;
            var directoryReader = this.fs.root.createReader();

            var dfd = new $.Deferred();

            directoryReader.readEntries(function (entries) {

                var count = entries.length;
                if (count === 0) {
                    dfd.resolve({successCount: successCount, errorCount: errorCount});
                }
                _.each(entries, function (entry) {
                    if (entry.isDirectory) {
                        entry.removeRecursively(
                                function () {
                                    console.log("things are going well");
                                    successCount++;
                                    if (successCount + errorCount === count) {
                                        dfd.resolve({successCount: successCount, errorCount: errorCount});
                                    }
                                },
                                function () {
                                    console.log("you fail this time");
                                    errorCount++;
                                    if (successCount + errorCount === count) {
                                        dfd.resolve({successCount: successCount, errorCount: errorCount});
                                    }
                                }
                        );
                    } else {
                        entry.remove(
                                function () {
                                    console.log("things are going well");
                                    successCount++;
                                    if (successCount + errorCount === count) {
                                        dfd.resolve({successCount: successCount, errorCount: errorCount});
                                    }
                                },
                                function () {
                                    console.log("you fail this time");
                                    errorCount++;
                                    if (successCount + errorCount === count) {
                                        dfd.resolve({successCount: successCount, errorCount: errorCount});
                                    }
                                }
                        );
                    }
                });
            });
            return dfd.promise();

        },
        removeRecursively: function () {
            var self = this;
            $.when(this.removeRecursivelyAsync()).then(
                    function (options) {
                        if (options.errorCount === 0) {
                            self.options.success({successCount: options.successCount});
                        } else {
                            self.options.error({successCount: options.successCount, errorCount: options.errorCount});
                        }
                    }
            );
        },
        writeFile: function () {
            console.log("File: Write file");

            var self = this;
            // create maildirectory
            this.fs.root.getDirectory('logs/', {create: true},
            function (dirEntry) {
                // console.log('File: DirEntry: ' + JSON.stringify(dirEntry));
                var pathToFiles = new Array();
                self.fs.root.getFile('logs/' + self.options.filename, {create: true},
                function (fileEntry) {
                    // console.log('File: FileEntry: ' + JSON.stringify(fileEntry));
                    fileEntry.createWriter(function (fileWriter) {

                        fileWriter.onwriteend = function (e) {
                            console.log('File: writeFile completed.');
                            self.options.success(pathToFiles);
                        };

                        fileWriter.onerror = function (e) {
                            console.log('File: writeFile failed: ' + e.toString());
                            self.options.error();
                        };

                        // console.log('File: FileWriter: ' + JSON.stringify(fileWriter));
                        var furl = fileEntry.toURL().replace('file://', '');
                        var furlarr = furl.split('/');
                        var fpath = '';
                        for (var i = 0; i < furlarr.length - 1; i++) {
                            fpath = fpath + '/' + furlarr[i];
                        }
                        fpath = fpath + '/' + self.options.filename;

                        var localFileMeta = {};
                        localFileMeta.path = fpath;
                        localFileMeta.mimetype = 'text/plain';
                        pathToFiles.push(localFileMeta);

                        var osAndVersion = window.hs_archive_modules.router.checkOperatingSystem()
                        switch (osAndVersion) {
                            case 'ios_gte_7':
                            case 'ios_lt_7':
                            case 'android_gte_44':
                            case 'android_gte_5':
                            case 'web':
                                // Create a new Blob and write it to log.txt.
                                var blob = new Blob([self.options.text], {type: 'text/plain'});
                                fileWriter.write(blob);
                                break;
                            default:
                                console.warn("File: Use DEPRECATED WebKitBlobBuilder");
                                var bb = new window.WebKitBlobBuilder();
                                bb.append([self.options.text]);
                                var blob = bb.getBlob('text/plain');
                                fileWriter.write(blob);

                                break;
                        }
                    }, self.errorHandler);

                }, self.errorHandler);
            });
        },
        loadFromRestOfferSave: function () {
            console.log('File: loadFromRestOfferSave');

            var self = this;
            var attachmentlistLength = this.options.attachmentlist.length;
            var mailAttachment = new MailAttachment();
            for (var i = 0; i < attachmentlistLength; i++) {
                var request = mailAttachment.fetch({
                    year: self.options.year_id,
                    mailid: self.options.email_id,
                    filename: self.options.attachmentlist[i],
                    requestid: i,
                    success: function (attachment, options) {
                        self.currentRequests[options.requestid] = null;
//                        console.log(options.mimetype);
//                        console.log(options.filename);
//                        console.log(attachment);
                        saveAs(new Blob([attachment], {type: options.mimetype}), options.filename);
                        self.options.success();
                    }, error: function (options) {
                        console.warn("File: skip file: " + options.filename + " errorcode: " + options.errorcode);
                        self.currentRequests[options.requestid] = null;
                        self.options.error();
                    }
                });

                this.currentRequests[i] = request;
                console.log("File: current running request: " + this.currentRequests.length);
            }
        },
        loadFromRest: function () {
            if (this.options.attachmentlist) {

                // attacements exists

                //console.log(this.options.attachmentlist);
                var mailAttachment = new MailAttachment();
                var attachmentlistLength = this.options.attachmentlist.length;
                // console.log("Length: " + arrayLength);

                var fileNames = new Array();
                // preare filenames
                for (var i = 0; i < attachmentlistLength; i++) {
                    var fileMeta = {};
//                console.log("url ... " + url);
                    // var extension = '.' + url.replace(/^.*\./, "");
                    fileMeta.targetFileName = this.options.attachmentlist[i];
                    fileMeta.targetDirectoryName = this.options.year_id + '_' + this.options.email_id + '/';
                    fileNames.push(fileMeta);
                }

                var self = this;
                // create maildirectory
                this.fs.root.getDirectory(this.options.year_id + '_' + this.options.email_id, {create: true},
                function (dirEntry) {

                    // directory exists

//                    console.log("Directory created, start download ... ");
                    var counter = fileNames.length;
                    var pathToFiles = new Array();

                    for (var i = 0; i < attachmentlistLength; i++) {
                        var fileMeta = fileNames[i];


                        /*
                         *  prepared to check, if file already exists in local filesystem 
                         *  
                         *  not implemented, because mimetype is not stored local
                         */
//                        var localFileName = fileMeta.targetDirectoryName + fileMeta.targetFileName;
//                        self.fileAlreadyExists({
//                            pathToFile: localFileName,
//                            success: function () {
//                                console.warn('File: Requested file already exists.')
//                            },
//                            error: function (e) {
//                                // acts as errorhandler
//                                if (e.name === "NotFoundError" || e.code === FileError.NOT_FOUND_ERR) {
//                                    console.warn("File: File not in filesystem, will be downloaded.");
//                                }
//                            }
//                        });


                        var request = mailAttachment.fetch({
                            year: self.options.year_id,
                            mailid: self.options.email_id,
                            filename: fileMeta.targetFileName,
                            requestid: i,
                            success: function (attachment, options) {
                                self.currentRequests[options.requestid] = null;

                                console.log("File: attachment loaded, will stored local as " + fileMeta.targetDirectoryName + options.filename);
                                fileMeta.mimetype = options.mimetype;
                                fileMeta.filename = options.filename;

                                // write data file on disk
                                self.fs.root.getFile(fileMeta.targetDirectoryName + options.filename, {create: true},
                                function (fileEntry) {

                                    //console.log("FILESYSTEM: " + fileEntry.filesystem.name);
                                    //console.log("FULLPATH: " + fileEntry.fullPath);
                                    //console.log("toURL: " + fileEntry.toURL());

                                    fileEntry.createWriter(function (fileWriter) {

                                        // console.log('File: FileWriter: ' + JSON.stringify(fileWriter));
                                        fileWriter.write(attachment);
                                        // console.log('>>> fileEntry.toURL()');
                                        // console.log(fileEntry.toURL());
                                        // TODO Check updated Plugins
                                        var furl = fileEntry.toURL().replace('file://', '');
                                        var furlarr = furl.split('/');
                                        var fpath = '';
                                        for (var i = 0; i < furlarr.length - 1; i++) {
                                            fpath = fpath + '/' + furlarr[i];
                                        }
                                        fpath = fpath + '/' + options.filename;
                                        console.log('File: local filesystem path: ' + fpath);
                                        var localFileMeta = {};
                                        localFileMeta.path = fpath;
                                        localFileMeta.mimetype = fileMeta.mimetype;
                                        pathToFiles.push(localFileMeta);

                                        fileWriter.onwriteend = function () {
                                            console.log("File: File succesfull written.");

                                            counter--;

                                            console.log("Counter: " + counter);

                                            if (counter === 0) {
                                                console.log("File: All files done.");
//                                                self.writeMimeTypes(fileNames, pathToFiles);
                                                self.options.success(pathToFiles);
                                            } else {
                                                console.log('counter is ' + counter);
                                            }
                                        };
                                    }, self.errorHandler);
                                }, self.errorHandler);
                            }, error: function (options) {
                                self.currentRequests[options.requestid] = null;
                                console.warn("File: skip file: " + options.filename + " errorcode: " + options.errorcode);
                                counter--;
                                if (counter === 0) {
                                    self.options.success(pathToFiles, {canceled: true});
                                }
                            }
                        });
                        self.currentRequests[i] = request;
                        console.log("File: current running request: " + self.currentRequests.length);
                    }
                }
                , self.errorHandler);
            }
        },
        /*
         * not implemented because of missing mimetype for local files
         */
//        fileAlreadyExists: function (options) {
//            console.log('this.options.pathToFile');
//            console.log(options.pathToFile);
//            this.fs.root.getFile(options.pathToFile, {}, function (fileEntry) {
//                fileEntry.file(function (file) {
//                    var reader = new FileReader();
//                    reader.onloadend = function () {
//                        options.success();
//                    };
//                    reader.readAsText(file);
//                }, options.error);
//            }, options.error);
//        },
        loadLocalOfferSave: function () {
            console.log('File: TODO load Attachments ');
            var self = this;
            var attachmentlistLength = this.options.attachmentlist.length;
            var readLocalFile = new ReadLocalFile();
            for (var i = 0; i < attachmentlistLength; i++) {

                var request = readLocalFile.fetch({
                    filename: self.options.attachmentlist[i],
                    requestid: i,
                    success: function (attachment, options) {
                        self.currentRequests[options.requestid] = null;
//                        console.log(options.mimetype);
//                        console.log(options.filename);
//                        console.log(attachment);
                        saveAs(new Blob([attachment], {type: options.mimetype}), options.filename);
                        self.options.success();
                    }, error: function (options) {
                        console.warn("File: skip file: " + options.filename + " errorcode: " + options.errorcode);
                        self.currentRequests[options.requestid] = null;
                        self.options.error();
                    }
                });

                this.currentRequests[i] = request;
                console.log("File: current running request: " + this.currentRequests.length);
            }
        },
        loadLocal: function () {
            //console.log(this.options.attachmentlist);
            var attachmentlistLength = this.options.attachmentlist.length;
            // console.log("Length: " + arrayLength);
            var fileNames = new Array();

            // preare filenames
            for (var i = 0; i < attachmentlistLength; i++) {
                var fileMeta = {};
//                console.log("url ... " + url);
                fileMeta.targetFileName = this.options.attachmentlist[i];
                fileMeta.targetDirectoryName = 'example_' + this.options.email_id + '/';
                fileNames.push(fileMeta);
            }

            var self = this;

            // get directory
            this.fs.root.getDirectory('example_' + this.options.email_id, {create: true}, function (dirEntry) {
                var counter = fileNames.length;
                var pathToFiles = new Array();

                var readLocalFile = new ReadLocalFile();

                for (var i = 0; i < fileNames.length; i++) {

                    var request = readLocalFile.fetch({
                        filename: self.options.attachmentlist[i],
                        requestid: i,
                        success: function (attachment, options) {
                            self.currentRequests[options.requestid] = null;
                            console.log("File: attachment loaded, will stored local as " + fileMeta.targetDirectoryName + options.filename);
                            fileMeta.mimetype = options.mimetype;
                            fileMeta.filename = options.filename;

                            // write data file on disk
                            self.fs.root.getFile(fileMeta.targetDirectoryName + options.filename, {create: true},
                            function (fileEntry) {

                                //console.log("FILESYSTEM: " + fileEntry.filesystem.name);
                                //console.log("FULLPATH: " + fileEntry.fullPath);
                                //console.log("toURL: " + fileEntry.toURL());

                                fileEntry.createWriter(function (fileWriter) {

                                    // console.log('File: FileWriter: ' + JSON.stringify(fileWriter));
                                    fileWriter.write(attachment);
                                    // console.log('>>> fileEntry.toURL()');
                                    // console.log(fileEntry.toURL());
                                    // TODO Check updated Plugins
                                    var furl = fileEntry.toURL().replace('file://', '');
                                    var furlarr = furl.split('/');
                                    var fpath = '';
                                    for (var i = 0; i < furlarr.length - 1; i++) {
                                        fpath = fpath + '/' + furlarr[i];
                                    }
                                    fpath = fpath + '/' + options.filename;
                                    console.log('File: local filesystem path: ' + fpath);
                                    var localFileMeta = {};
                                    localFileMeta.path = fpath;
                                    localFileMeta.mimetype = fileMeta.mimetype;
                                    pathToFiles.push(localFileMeta);

                                    // TODO fileWriter.onwriteend ??
                                    fileWriter.onwriteend = function () {
                                        console.log("File: Local file succesfull written.");
                                        counter--;
                                        if (counter === 0) {
                                            console.log("File: All files done.");
//                                                self.writeMimeTypes(fileNames, pathToFiles);
                                            self.options.success(pathToFiles);
                                        } else {
                                            // console.log('counter is ' + counter);
                                        }
                                    };
                                }, self.errorHandler);
                            }, self.errorHandler);
                        }, error: function (options) {
                            self.currentRequests[options.requestid] = null;
                            console.warn("File: skip file: " + options.filename + " errorcode: " + options.errorcode);
                            counter--;
                            if (counter === 0) {
                                self.options.success(pathToFiles, {canceled: true});
                            }
                        }
                    });

                    self.currentRequests[i] = request;
                    console.log("File: current running request: " + self.currentRequests.length);
                }
            });
        },
        errorHandler: function (e) {

            var msg = '';
            // console.log(JSON.stringify(e));
            if (e.name) {
                console.log('File: ' + e.name + ": " + e.message);
            } else {
                switch (e.code) {
                    case FileError.QUOTA_EXCEEDED_ERR:
                        msg = 'QUOTA_EXCEEDED_ERR';
                        break;
                    case FileError.NOT_FOUND_ERR:
                        msg = 'NOT_FOUND_ERR';
                        break;
                    case FileError.SECURITY_ERR:
                        msg = 'SECURITY_ERR';
                        break;
                    case FileError.INVALID_MODIFICATION_ERR:
                        msg = 'INVALID_MODIFICATION_ERR';
                        break;
                    case FileError.INVALID_STATE_ERR:
                        msg = 'INVALID_STATE_ERR';
                        break;
                    default:
                        msg = 'Unknown ErrorCode,  ' + e.message;
                        break;
                }
                console.warn("File: Error from code: " + msg);
            }
            // TODO ? transfer message to caller ?
            // this.options.error();
        },
        render: function () {
            return this;
        },
        abort: function () {
            for (var i = 0; i < this.currentRequests.length; i++) {
                console.log(JSON.stringify(this.currentRequests, null, 4));
                if (this.currentRequests[i]) {
                    this.currentRequests[i].abort();
                }
            }
        }
    });

});