/**
 * RequireJS configuration
 * define global Session 
 * start app
 *  - target configuration
 *  - configure logging
 *  - get credentials from locaalstorage (for mobile)
 *  - start Router & Backbon history
 */
require.config({
    baseUrl: 'libs',
    paths: {
        app: '../js/app',
        tools: '../js/tools',
        models: '../js/app/models',
        // core libs
        jquery: 'jquery/dist/jquery.min',
        backbone: 'backbone/backbone',
        bootstrap: "bootstrap/dist/js/bootstrap",
        // template
        Underscore: 'underscore/underscore',
        hbs: 'require-handlebars-plugin/hbs',
        moment: 'momentjs/min/moment.min',
        moment_de: 'momentjs/locale/de',
        moment_es: 'momentjs/locale/es',
        'CryptoJS': "cryptojslib/components/core",
        'cryptojs_base64': "cryptojslib/components/enc-base64",
        'cryptojs_md5': "cryptojslib/components/md5",
        'cryptojs_evpkdf': "cryptojslib/components/evpkdf",
        'cryptojs_pbkdf2': "cryptojslib/components/pbkdf2",
        'cryptojs_cipher-core': "cryptojslib/components/cipher-core",
        'cryptojs_aes': "cryptojslib/components/aes",
        'cryptojs_sha1': "cryptojslib/components/sha1",
        'cryptojs_hmac': "cryptojslib/components/hmac",
        bootstrapSwitch: 'bootstrap-switch/dist/js/bootstrap-switch.min',
        'boostrap_daterangepicker': 'bootstrap-daterangepicker/daterangepicker',
        filesaver: "FileSaver/FileSaver",
        hammerjs: "hammerjs/hammer",
        'jquery-hammerjs': "jquery-hammerjs/jquery.hammer",
        'hammerjs-jquery-backbone': "backbone.hammer/backbone.hammer"

    },
    shim: {
        'jQuery': {
            exports: '$'
        },
        'backbone': {
            deps: ['Underscore', 'jquery'],
            exports: 'Backbone'
        },
        'Underscore': {
            exports: '_'
        },
        'bootstrap': {
            deps: ['jquery'],
            exports: 'bootstrap'
        },
        'bootstrapSwitch': {
            deps: ['jquery'],
            exports: 'bootstrapSwitch'
        },
        'CryptoJS': {
            exports: "CryptoJS"
        },
        'cryptojs_base64': {
            deps: ['CryptoJS'],
            exports: "CryptoJS"
        },
        'cryptojs_pbkdf2': {
            deps: ['CryptoJS', 'cryptojs_sha1', 'cryptojs_hmac'],
            exports: "CryptoJS"
        },
        'cryptojs_sha1': {
            deps: ['CryptoJS'],
            exports: "CryptoJS"
        },
        'cryptojs_hmac': {
            deps: ['CryptoJS'],
            exports: "CryptoJS"
        },
        'cryptojs_md5': {
            deps: ['CryptoJS'],
            exports: "CryptoJS"
        },
        'cryptojs_evpkdf': {
            deps: ['CryptoJS', 'cryptojs_md5'],
            exports: "CryptoJS"
        },
        'cryptojs_cipher-core': {
            deps: ['CryptoJS', 'cryptojs_evpkdf', 'cryptojs_base64'],
            exports: "CryptoJS"
        },
        'cryptojs_aes': {
            deps: ['CryptoJS', 'cryptojs_base64', 'cryptojs_md5', 'cryptojs_evpkdf', 'cryptojs_cipher-core'],
            exports: "CryptoJS"
        },
        'boostrap_daterangepicker': {
            deps: ['jquery', 'moment'],
            exports: 'Daterangepicker'
        }
    },
    hbs: {
        templateExtension: 'html',
        helpers: true,
        i18n: false,
        helperPathCallback: function (name) {
            return '../../tools/hbshelpers/' + name;
        }
    },
    noGlobal: true
});


/**
 * from http://blogs.sitepointstatic.com/examples/tech/js-session/session.js
 * 
 * Used to store encrypted credentails 
 * user get username/password 
 * .. submit button and action in form for storing password 
 * .. page reload 
 * .. new init
 * .. get values
 * .. delete data in session
 * 
 * TODO define/require
 * TODO a workaround was made for iOS, because storage was always EMPTY after reload
 * 
 */


// TODO msg for not Supported Browsers ... typeof ..
if (JSON && JSON.stringify && JSON.parse)
    var Session = Session || (function () {

        // window object
        var win = window.top || window;

        // console.log(win.name);

        // session store
        var store = (win.name ? JSON.parse(win.name) : {});

        // save store on page unload
        function Save() {
            win.name = JSON.stringify(store);
        }

        // page unload event
        if (window.addEventListener) {
            window.addEventListener("unload", Save, false);
        } else if (window.attachEvent) {
            window.attachEvent("onunload", Save);
        } else {
            window.onunload = Save;
        }

        // public methods
        return {
            // set a session variable
            set: function (name, value) {
                store[name] = value;
                // console.log("SET: " + name + ": " + store['transferObject']);
            },
            // get a session value
            get: function (name) {
                // console.log("GET: " + name + ": " + store['transferObject']);
                return (store[name] ? store[name] : undefined);
            },
            // clear session
            clear: function () {
                store = {};
                win.name = '';
            },
            // dump session data
            dump: function () {
                return JSON.stringify(store);
            }

        };

    })();

define(function (require) {

    "use strict";
    var Backbone = require('backbone');
    var $ = require('jquery');
    var _ = require('Underscore');
    var moment = require('moment');
    var Router = require('app/Router');
    // var RouterQuarantine = require('app/routerQuarantine');
    var Bootstrap = require('bootstrap');
    var ApiLogin = require('app/models/ApiLogin');
    var apiLoginService = new ApiLogin();
    var PreferencesStore = require('app/models/PreferencesStore');
    var Year = require('app/models/ApiArchiveGetYears');
    var sd = require('app/models/StaticData');
    var WaitForResponseDialog = require('app/views/generic/WaitForServerAction');
    var BaseModalView = require('app/views/generic/WaitForUserAction');
    var Footprint = require('tools/Footprint');

    // IE 9 issues
    // http://stackoverflow.com/questions/20624476/xdomainrequest-cors-for-xml-causing-access-is-denied-error-in-ie8-ie9
    // http://stackoverflow.com/questions/12592209/jquery-ajax-not-working-in-ie9
    // http://blogs.msdn.com/b/ieinternals/archive/2010/05/13/xdomainrequest-restrictions-limitations-and-workarounds.aspx
    // if (typeof console === "undefined" || typeof console.log === "undefined") {
    // alert("Console will be added");
    // console = {};
    // }
    // console.log(requirejs.s.contexts._.config.baseUrl);
    // 

    // INFO introduced on 2015.0326
    var pageRoot = "/ASEPrototype/";

//    var devLogFilter = '';

    window.hs_archive = {};

    // Globals for build and logging - see grunt.js
    // **START** do not touch - will be replaced during build
    var useCustomizedLogOrFilter = false;
    // true: no output to console
    var hideAllLogging = false;
    window.hs_archive.build_id = "edge";
    // deprecated -> set always to false
    window.hs_archive.isQuarantineWeb = false;
    window.hs_archive.isArchiveWeb = false;
    window.hs_archive.isArchiveMobileDevice = true;
    window.hs_archive.isArchiveMobileDeviceDev = true;

    // following actions only available if useCustomizedLogOrFilter=true (will be set to "true" during build process)
    // true: write console.info/warn/error in log file 
    // false: write console.warn/error in log file 
    window.hs_archive.isEnhanchedLogging = false;
    // true: write ALSO console.log to file and console and console.info to console (will be set to "false" during build process)
    window.hs_archive.isDeveloperLog = false;
    // **END** do not touch - will be replaced during build

    // access the router and global object/function from all modules - will be init later
    window.hs_archive_modules = {};
    window.hs_archive_modules.router = null;

    // keep all data for a session in this object
    window.hs_archive.asemobile_footprint = null;
    window.hs_archive.sessiondata = {};
    window.hs_archive.sessiondata.search = {};
    window.hs_archive.credentials = {};

    /*
     *  used to send more detailed debugging information - preferences -> send log file
     *  
     *  for testing: useCustomizedLogOrFilter must be set to true
     */
    moment.locale('en');
    window.hs_archive.logtext = 'Log started at: ' + moment().format(sd.logDateFormat);

    console.warn('Main: useCustomizedLogOrFilter is: ' + useCustomizedLogOrFilter);
    console.warn('Main: hideAllLogging is: ' + hideAllLogging);
    if (hideAllLogging) {
        // hijack console to suppress output
        var oldLog = console.log;
        console.log = function (message) {
            // silent
//            arguments[0] = 'Log suppressed';
//            oldLog.apply(console, arguments);
        };
        var oldInfo = console.info;
        console.info = function (message) {
//            arguments[0] = 'Info suppressed';
//            oldInfo.apply(console, arguments);
        };
        var oldWarning = console.warn;
        console.warn = function (message) {
//            arguments[0] = 'Warning suppressed';
//            oldWarning.apply(console, arguments);
        };
    } else if (useCustomizedLogOrFilter) {

        // TODO add 
        if (window.hs_archive.isDeveloperLog) {
            var oldLog = console.log;
            console.log = function (message) {
                if (_.isString(message)) {
                    window.hs_archive.logtext = window.hs_archive.logtext + "\n" + moment().format(sd.logDateFormat) + ": log: " + message;
//                    if (message.slice(0, devLogFilter.length) === devLogFilter) {
                    oldLog.apply(console, arguments);
//                    }
                } else {
                    oldLog.apply(console, arguments);
                }
            };
        } else {
            // No log output
            var oldLog = console.log;
            console.log = function (message) {
                // silent
                // arguments[0] = 'Log suppressed';
                // oldLog.apply(console, arguments);
            };
        }

        var oldInfo = console.info;

        //  console.log(oldInfo);
        console.info = function (message) {
            if (_.isString(message)) {
                if (window.hs_archive.isEnhanchedLogging || window.hs_archive.isDeveloperLog) {
                    // add output to log file
                    window.hs_archive.logtext = window.hs_archive.logtext + "\n" + moment().format(sd.logDateFormat) + ": info: " + message;
                    if (window.hs_archive.isDeveloperLog) {
                        oldInfo.apply(console, arguments);
                    }
                }
            }
        };

        var oldWarning = console.warn;
        console.log(oldWarning);
        console.warn = function (message) {
            if (_.isString(message)) {
                window.hs_archive.logtext = window.hs_archive.logtext + "\n" + moment().format(
                        sd.logDateFormat) + ": Warning: " + message;
            }
            //alert(logtext);
            oldWarning.apply(console, arguments);
        };

        var oldError = console.error;
        console.log(oldError);
        console.error = function (message) {
            if (_.isString(message)) {
                window.hs_archive.logtext = window.hs_archive.logtext + "\n" + moment().format(
                        sd.logDateFormat) + ": ERROR: " + message;
            }
            //alert(logtext);
            oldError.apply(console, arguments);
        };
    } else {
        window.hs_archive.logtext = window.hs_archive.logtext + "\nwindow.hs_archive.isArchiveMobileDevice " + window.hs_archive.isArchiveMobileDevice;
    }
    console.log(
            'Main: current mode {isMobileDevice:' + window.hs_archive.isArchiveMobileDevice + ', isArchiveWeb:' + window.hs_archive.isArchiveWeb + ', isQuarantine:' + window.hs_archive.isQuarantineWeb + '}');

    function readPreferencesTokenAndYears() {
        // only called for Archive Web AND Mobile
        function readPreferences() {
            // f.e isDemoMode
            var d = $.Deferred();
            var preferencesService = new PreferencesStore();
            preferencesService.read({success: function (p) {
                    d.resolve(p);
                }
            });
            return d.promise();
        }

        function readToken() {
            var d = $.Deferred();
            apiLoginService.checkStatusAndGetToken({
                success: function (options) {
                    d.resolve(options.token);
                },
                error: function (errorcode) {
                    console.warn("Main: No connection to server " + errorcode);
                    var modalView = new BaseModalView({
                        title: '__base.error',
                        description: '__login.errorcode_' + errorcode,
                        action: '__base.close'
                    });
                    modalView.show();

                    // alert('Testing: MSG-ID 4 No connection to server.');
                    d.resolve();
                }
            });
            return d.promise();
        }
//        if (window.hs_archive.isArchiveMobileDevice) {
        // only show on mobile device
        var modalView = new WaitForResponseDialog({
            title: '__app.authorizing',
            action: '__base.close',
            static: true
        });
        modalView.show();
//        }

        // create footprint for client before first ajax-call
        window.hs_archive.asemobile_footprint = new Footprint();
        var d = $.Deferred();
        $.when(readPreferences()).done(function (preferences) {
            if (window.hs_archive.isArchiveWeb || preferences.isDemoMode) {
                d.resolve(preferences, null, null);
                modalView.hide();
            } else {
                // read token
                var yearService = new Year();
                $.when(readToken()).done(function (token) {
                    if (token) {
                        yearService.fetch({
                            token: token,
                            success: function () {
                                modalView.hide();
                                d.resolve(preferences, token, yearService);
                            }, error: function () {
                                modalView.hide();
                                d.resolve(preferences, token, null);
                            }
                        });
                    } else {
                        modalView.hide();
                        d.resolve(preferences, null, null);
                    }
                });
            }
        });
        return d.promise();
    }

    Backbone.history.handlers.push({
        route: /(.*)/,
        callback: function (fragment) {
            if (fragment === '') {
                console.info('Main: default route');
            } else {
                console.info('Main: route ' + fragment);
            }
        }
    });

//    if (window.hs_archive.isArchiveMobileDevice || window.hs_archive.isArchiveMobileDeviceDev) {
    console.log('Main: Initialize app');

//        $.when(readTokenAndYears(), readPreferences()).done(function(tokenYearArray, preferences) {
    if (window.hs_archive.isArchiveMobileDevice && !window.hs_archive.isArchiveMobileDeviceDev) {
        document.addEventListener("deviceready", function () {
            StatusBar.overlaysWebView(false);

            $.when(readPreferencesTokenAndYears()).done(function (preferences, token, years) {
                console.log('Main: Mobile cordova environment');

                window.hs_archive.sessiondata.preferences = preferences;
                window.hs_archive.sessiondata.token = token;
                window.hs_archive.sessiondata.years = years;

                window.hs_archive_modules.router = new Router();
                Backbone.history.start({root: pageRoot});
            });
        }, false);
    } else if (window.hs_archive.isArchiveWeb || window.hs_archive.isArchiveMobileDeviceDev) {
        $.when(readPreferencesTokenAndYears()).done(function (preferences, token, years) {
            // only local preferences on private computers will be loaded
            console.log('Main: Web environment');

            window.hs_archive.sessiondata.preferences = preferences;
            // Always reset Demomode to false after reload
            if (window.hs_archive.isArchiveWeb) {
                window.hs_archive.sessiondata.preferences.isDemoMode = false;
            }

            window.hs_archive.sessiondata.token = token;
            window.hs_archive.sessiondata.years = years;
            window.hs_archive_modules.router = new Router();
            Backbone.history.start({root: pageRoot});
        });
    } else {

        console.log('Main: Quarantine environment');
        window.hs_archive.asemobile_footprint = new Footprint();

        window.hs_archive.sessiondata.preferences = {};
        window.hs_archive.sessiondata.preferences.isDemoMode = false;
        window.hs_archive_modules.router = new Router();
        Backbone.history.start({root: pageRoot});
    }
});

