/*
 * Client footprint for Backenend evaluation
 * 
 */
define(function () {
    "use strict";
    
    // TODO add enhanced information 
    var enhanced = false;
    function Footprint() {
        var localConfig = {};
        localConfig["Build"] = window.hs_archive.build_id;
        localConfig["Resolution_X"] = window.screen.availWidth;
        localConfig["Resolution_Y"] = window.screen.availHeight;
        localConfig["ColorDepth"] = screen.colorDepth;
        localConfig["UserAgent"] = navigator.userAgent;
        localConfig["Timezone"] = (new Date()).getTimezoneOffset();
        localConfig["Language"] = (navigator.language || navigator.userLanguage);
        if (enhanced) {
            document.cookie = "sFP";
            if (typeof navigator.cookieEnabled !== "undefined"
                    && navigator.cookieEnabled === true
                    && document.cookie.indexOf("sFP") !== -1) {
                localConfig["Cookies"] = true;
            } else {
                localConfig["Cookies"] = false;
            }
            localConfig ["Plugins"] = jQuery.map(navigator.plugins, function (oElement) {
                return oElement.name + "-" + oElement.filename;
            });
        }
        // if (window.hs_archive.isArchiveMobileDevice && !window.hs_archive.isArchiveMobileDeviceDev) {
        // console.log(JSON.stringify(window.device));
        if (window.device) {
            localConfig["Os"] = window.device.platform;
            localConfig["OsVersion"] = window.device.version;
        } else {
            localConfig["Os"] = 'web';
            localConfig["OsVersion"] = '';
        }
        // console.log(JSON.stringify(localConfig));
        return localConfig;
    }

    return Footprint;
});