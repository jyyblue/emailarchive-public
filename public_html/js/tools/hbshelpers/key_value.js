/*
 * Handlebar helper for a key value output
 * 
 */
define(function (require) {
    var Handlebars = require('hbs/handlebars');
// // HELPER: #key_value
//
// Usage: {{#key_value obj}} Key{{key}}: Value: {{value}} {{/key_value}}
//
// Iterate over an object, setting 'key' and 'value' for each property in
// the object.

    var key_value = function (obj, hb) {
        var buffer = "";

        if (obj) {
            for (var i = 0; i < obj.length; i++) {
                buffer += hb.fn({key: obj[i]['key'], value: obj[i]['value']});
            }
        }
        return buffer;
    };

    Handlebars.registerHelper("key_value", key_value);
    return(key_value());
});