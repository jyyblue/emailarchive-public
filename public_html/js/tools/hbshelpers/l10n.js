/*
 * Handlebar helper tanslation
 * 
 */

define(function (require) {
    var localization = require('models/Localization');
    var Handlebars = require('hbs/handlebars');
    var sd = require('app/models/StaticData');

    function l10n(keyword) {
        // exit now if there's no data
        if (!keyword) {
            console.warn("l10n: missing keyword!");
            return '__missing__';
        }
//        var locale = 'en';
        // Get language for user
        var locale = (navigator.language) ? navigator.language : navigator.userLanguage;
        var separator = locale.indexOf('-');
        if (separator !== -1) {
            locale = locale.substr(0, separator);
        }

        // pick the right dictionary
        var translations = localization.locale[locale] || localization.locale[sd.defaultLanguage];
        // pick the right dictionary (if only one available assume it's the right one..
        // var locale = properties.locale[lang] || properties.locale[defaultLanguage] || properties.locale || false;

        // exit now if there's no data
        if (!translations) {
            return;
        }
        var target = translations;
        // console.log("L10N: keyword: " + keyword);
        var key = keyword.split(".");
        // loop through all the key hierarchy (if any)
        for (var i in key) {
            if (!target) {
                break;
            } else {
                target = target[key[i]];
            }
        }

        if (!target) {
            // fallback defaultLanguage
            translations = localization.locale[sd.defaultLanguage];
            target = translations;
            // loop through all the key hierarchy (if any)
            for (var i in key) {
                if (!target) {
                    break;
                } else {
                    target = target[key[i]];
                }
            }
        }
        // fallback to the original string if nothing found
        target = target || keyword;
        
        //output
        return target;
    }
    Handlebars.registerHelper('l10n', l10n);

    return l10n;
});