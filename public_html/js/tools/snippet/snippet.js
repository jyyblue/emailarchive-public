require.config({
    baseUrl: 'libs',
    paths: {
        Underscore: 'underscore/underscore'
    },
    shim: {
        'Underscore': {
            exports: '_'
        }
    }
});

define(function (require) {

    var _ = require('Underscore');

    console.log("SNIPPET");
    // var s = window.hs_archive.asemobile_footprint.Os.toLowerCase();
    var s = 'android 6.1';
    var osAndVersion = 'web';
    if (/iphone|ipad|ipod|ios/i.test(s)) {
        if (/7./i.test(s)) {
            osAndVersion = 'ios_gte_7';
        } else {
            osAndVersion = 'ios_lt_7';
        }
    } else if (/android/i.test(s)) {
        // convert android version to a number

        var match = s.match(/android\s([0-9\.]*)/);
        if (_.isEmpty(match[1])) {
            // number conversion not successfull
            console.warn('Router: Android version not feasible. Update sourcecode.');
            if (/4.4|5./i.test(s)) {
                osAndVersion = 'android_gte_44';
            } else {
                osAndVersion = 'android_lt_44';
            }
        } else {
            if (parseFloat(match[1]) < 4.4) {
                osAndVersion = 'android_lt_44';
            } else {
                osAndVersion = 'android_gte_44';
            }
        }
    }
    console.log(osAndVersion);


});