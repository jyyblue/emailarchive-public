#!/usr/bin/env node

var srcPathWin = "E:\\test\\demo\\name.txt E:\\ssss\\sssss\\sss";
var srcPathLinux = "/home/test/demo/name.txt";

console.log(srcPathWin);
console.log(srcPathLinux);

console.log("/[^\s]*/.exec");
console.log(/[^\s]*/.exec(srcPathWin)[0]);

console.log("\nmatch(/^.*[\\/]/)[0]");
console.log(srcPathWin.match(/^.*[\\/]/)[0]);
console.log(srcPathLinux.match(/^.*[\\/]/)[0]);

console.log("\nmatch(/[^\\/]+$/)[0]);");
console.log(srcPathWin.match(/[^\\/]+$/)[0]);
console.log(srcPathLinux.match(/[^\\/]+$/)[0]);

console.log("\n/([^\\/]+)$/.exec");
console.log(/([^\\/]+)$/.exec(srcPathWin)[1]);
console.log(/([^\\/]+)$/.exec(srcPathLinux)[1]);





